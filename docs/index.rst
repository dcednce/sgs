.. Shared Genomic Segments documentation master file, created by
   sphinx-quickstart on Tue Sep 15 18:21:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shared Genomic Segments's documentation!
===================================================

.. toctree::

  sgs
  sgsOptions
  
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
