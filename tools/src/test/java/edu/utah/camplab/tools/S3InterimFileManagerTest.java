package edu.utah.camplab.tools;

import java.io.File;

public class S3InterimFileManagerTest {

  public static void main(String[] args) {
    String bucketChoise = args.length > 1 ? args[0] : "interim";
    
    S3InterimFileManager manager = new S3InterimFileManager(bucketChoise);
    manager.write(new File(args[args.length - 1]));
  }
}

  
