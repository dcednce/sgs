package edu.utah.camplab.tools;

import java.util.Map;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionDescriptor;

public class SGSHelpFormatter extends BuiltinHelpFormatter {

    private StringBuffer preamble;

    public SGSHelpFormatter(String verbage) {
        super(120,4);
        preamble = new StringBuffer(verbage);
    }
    
    public String getPreamble() {
        return preamble.toString();
    }

    public void extendPreample(String helpText) {
        preamble.append(helpText);
    }

    @Override
    public String format(Map<String, ? extends OptionDescriptor> options) {
        StringBuffer sb = new StringBuffer(super.format(options));
        int starti = 0;
        int endi = preamble.length();
        while (endi - starti > 120) {
            int breakpoint = preamble.substring(starti, 120).lastIndexOf(' ');
            if (breakpoint < 0) {
                breakpoint += 120;
            }
            sb.append(preamble.substring(starti, starti + breakpoint));
            starti += breakpoint;
        }
        sb.append(preamble.substring(starti));          
        return sb.toString();
    }
}
