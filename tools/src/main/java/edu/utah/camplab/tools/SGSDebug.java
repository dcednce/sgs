package edu.utah.camplab.tools;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

public class SGSDebug {
  public static long getRuntimeDebug() {
    return runtimeDebug;
  }

  public static void setRuntimeDebug(long runtimeDebug) {
    SGSDebug.runtimeDebug = runtimeDebug;
  }

  private static long runtimeDebug = 0L;  // To be reset by arg parser

  public static boolean isDebug(SGSDebugEnum dben) {
    return (runtimeDebug > 0 && (dben.getMaskValue() & runtimeDebug) > 0);
  }
  public static void addFlags(String uhCSV) {
    String[] uhs = uhCSV.split(",");
    for (String h : uhs) {
      Optional<SGSDebugEnum> maskOptional = (Arrays.stream(SGSDebugEnum.values()).filter((SGSDebugEnum dn) -> dn.getUserHandle().equals(h.trim())).findFirst());
      if (maskOptional.isPresent() ) {
        runtimeDebug |= maskOptional.get().getMaskValue();
      }
    }
  }
}
