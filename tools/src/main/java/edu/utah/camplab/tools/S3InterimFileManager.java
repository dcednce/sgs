package edu.utah.camplab.tools;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class S3InterimFileManager implements InterimFileManagerInterface {

  public static final Logger logger = LoggerFactory.getLogger(S3InterimFileManager.class);

  private AmazonS3 s3Client = null;
  private Regions clientRegion = Regions.DEFAULT_REGION;
  private String bucketName = null;
  
  public S3InterimFileManager(String bucketKey) {
    s3Client = AmazonS3ClientBuilder.standard().withRegion(clientRegion).build();
    StringBuilder sb = new StringBuilder("known buckets: ");
    String comma = "";
    try {
      for (Bucket bucket : s3Client.listBuckets()) {
	sb.append(bucket).append(comma);
	comma = ",";
	if (bucket.getName().contains(bucketKey)) {
	  bucketName = bucket.getName();
	  break;
	}
      }
    }
    catch (SdkClientException sdke) {
      sdke.printStackTrace();
    }
    if (bucketName == null) {
      logger.error("Could not find bucket: " + bucketKey);
      throw new SdkClientException("no bucket found for %s, try one of these ".format(bucketKey, sb.toString()));
    }
  }

  public void write(File jsonFile) {
    String objectName = jsonFile.getName().replaceAll("_", "/");
    PutObjectRequest poReq = new PutObjectRequest(bucketName, objectName, jsonFile);
    ObjectMetadata metadata = new ObjectMetadata();
    String finalMessage = "";
    int loops = 0;
    while (loops < 3) {
      try {
	if (jsonFile.getName().endsWith("gz")) {
	  metadata.setContentType("application/gzip");
	}
	else if (jsonFile.getName().endsWith("json")) {
	  metadata.setContentType("application/json");
	}
	else {
	  metadata.setContentType("plain/text");
	}
	metadata.addUserMetadata("title", jsonFile.getName());
	poReq.setMetadata(metadata);
	s3Client.putObject(poReq);
	break;
      }
      catch (SdkClientException s3x) {
	finalMessage = s3x.getMessage();
	logger.error("Could not write interim file {} loop {}", jsonFile.getName(), loops+1);
	loops++;
	try {
	  Thread.sleep(2000);
	}
	catch (InterruptedException ie) {
	  finalMessage = ie.getMessage();
	  logger.error("Sleep interupted: {}", finalMessage, ie);
	}
      }
      finally {
	if (loops == 3) {
	  logger.error("S3 write failed for {}: {} ", jsonFile.getName(), finalMessage);
	}
      }
    }
  }
}
