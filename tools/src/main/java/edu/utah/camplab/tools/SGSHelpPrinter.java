package edu.utah.camplab.tools;

import joptsimple.OptionParser;

import java.io.IOException;

public class SGSHelpPrinter {
    private OptionParser optionParser;
    private boolean doExit = true;
    private int width = 100;
    private String description = "";

    public SGSHelpPrinter(OptionParser op) {
        optionParser = op;
    }

    public SGSHelpPrinter(OptionParser op, boolean exit) {
        optionParser = op;
        doExit = exit;
    }

    public void printHelp() throws IOException {
        optionParser.printHelpOn(System.err);
        System.err.print(justifyHelp());
        if (doExit) System.exit(0);
    }

    public void setWidth(int w) {
        width = w;
    }

    public void setDescription(String d) {
        description = d;
    }

    public String getDescription() {
        if (description == null)
            description = "";
        return description;
    }

    private String justifyHelp() {
        int position = 0;
        int endpoint = 0;
        StringBuilder sb = new StringBuilder();
        while (position + width < getDescription().length() - 1) {
            endpoint += width;
            // Looking for last word break
            while (description.charAt(endpoint) != ' ') {
                endpoint--;
            }
            String chunk = getDescription().substring(position, endpoint);
            int nlAt = chunk.indexOf(System.getProperty("line.separator"));
            if (nlAt > -1) {
                //found EOLN, end chunk after it
                sb.append(chunk.substring(0, nlAt+1));
                position += nlAt + 1;
                endpoint = position;
            }
            else  {
                position =  ++endpoint;
                sb.append(chunk + "\n");
            }
        }
        sb.append(description.substring(position)+"\n");
        return sb.toString();
    }
}
