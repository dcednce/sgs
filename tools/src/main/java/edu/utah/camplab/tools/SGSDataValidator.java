package edu.utah.camplab.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import jpsgcs.linkage.LinkageInterface;

public class SGSDataValidator {
    public static void validateLocusCount(LinkageInterface li, File pedfile) {
        int nloci = li.nLoci();
		
        List<String> lines = PedTrimmer.getLines(pedfile);
        assert lines.isEmpty() == false;
		
        if (nloci != numMarkers(lines.get(0))) {
            throw new RuntimeException("LD model and pedfile do not agree on the number of loci.\n");
        }
    }

    public static void validateLinkageInterfaceAndPedfile(LinkageInterface li, File pedfile) {
        int nloci = li.nLoci();
		
        List<String> lines = PedTrimmer.getLines(pedfile);
        assert lines.isEmpty() == false;
		
        if (nloci != numMarkers(lines.get(0))) {
            throw new RuntimeException("LD model and pedfile do not agree on the number of loci.\n");
        }
    }
	
    private static int numMarkers(String line) {
        String[] parts = line.split("\\s+");
        return (parts.length - 9) / 2;
    }

    public static void validateGenotypes(LinkageInterface linkageInterface) {
        for (int j = 0; j < linkageInterface.nIndividuals(); ++j) {
            if (linkageInterface.proband(j) != 0) {
                int failCounts = 0;
				
                for (int i = 0; i < 1000; ++i) {
                    if (linkageInterface.getAllele(i, j, 0) < 0) {
                        failCounts++;
                    }
                    if (linkageInterface.getAllele(i, j, 1) < 0) {
                        failCounts++;
                    }
                }
				
                if (failCounts >= 1600) {
                    throw new RuntimeException(String.format("Insufficient genotype data for individual %s.\n", linkageInterface.individualName(j)));
                }
            }
        }
    }

    public static Set<String>  validateProbandSubsetsFile(File subsetsFile, File pedFile) {
        Set<String> subsetProbands = null;

        if (subsetsFile == null) {
            return null;
        }
		
        try {
            subsetProbands = ProbandManager.getUniqueProbandsFromSubsetsFile(subsetsFile);
            Set<String> pedProbands = ProbandManager.getProbandIdsFromPedFile(pedFile);
            for (String pid : subsetProbands) {
                if (pedProbands.contains(pid) == false) {
                    throw new RuntimeException(String.format("Invalid proband %s found in subsets file.\n", pid));
                }
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return subsetProbands;
    }
}
