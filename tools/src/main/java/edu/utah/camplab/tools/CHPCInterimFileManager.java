package edu.utah.camplab.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CHPCInterimFileManager implements InterimFileManagerInterface {
    /* notice we add trailing slash */
    public static final String CHPC_PE_ROOT_TEMPLATE = "/uufs/chpc.utah.edu/common/HIPAA/%s/sgsWork/%s";

    private Path irbDir;

    /*
     * we expect that clients to have access to the IRB partition
     */
    public CHPCInterimFileManager(String IRB, String operation) {
        irbDir = Paths.get(CHPCInterimFileManager.CHPC_PE_ROOT_TEMPLATE.formatted(IRB, operation));
        try {
            if (! Files.exists(irbDir)) {
                irbDir = Files.createDirectories(irbDir);
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException("Trying " + irbDir, ioe);
        }
    }
    public void write(File jsonFile) {
        try {
            Files.copy(jsonFile.toPath(), irbDir.resolve(jsonFile.toPath().getFileName()));
        }
        catch (IOException ioe) {
            throw new RuntimeException("did not write " + jsonFile.getAbsolutePath());
        }
    }

}
