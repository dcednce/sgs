package edu.utah.camplab.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import jpsgcs.linkage.LinkageId;

/**
 * Utility class for basic proband file and subset operations.
 * 
 * @author David Brown
 *
 */
public class ProbandManager {
	
	/**
	 * Gets the full set of unique probands present within a proband subsets file.
	 * 
	 * @param subsetsFile
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Set<String> getUniqueProbandsFromSubsetsFile(File subsetsFile) throws FileNotFoundException, IOException {
		Set<String> probands = new HashSet<String>();

		BufferedReader reader = new BufferedReader(new FileReader(subsetsFile));
		String inline = reader.readLine();
		while (inline != null) {
			LinkageIdSet idset = new LinkageIdSet(inline);
			for (LinkageId<?> id : idset.asList()) {
				probands.add(id.stringGetId());
			}
			inline = reader.readLine();
		}
		reader.close();

		return probands;
	}
	
	/**
	 * Gets the set of unique probands from a file with one proband id per line.
	 * 
	 * @param probandListFile
	 * @return
	 * @throws IOException 
	 */
    public static Set<String> getProbandIdsFromListFile(File probandListFile) throws IOException {
		Set<String> probandIds = new HashSet<>();

		BufferedReader reader = new BufferedReader(new FileReader(probandListFile));
		String line;
		while ((line = reader.readLine()) != null) {
			String id = line.replaceAll("\\s+", "");
			if (!id.equals("")) {
				probandIds.add(id);
			}
		}
		reader.close();

		return probandIds;
    }
    
    public static List<String> getPedFileColumns(String s, int ncols) {
        Scanner scanner = new Scanner(s);
        List<String> cols = new ArrayList<>();
        for (int i = 0; i < ncols; ++i) {
            String tok = scanner.next();
            tok = tok.replaceAll("\\s+", "");
            cols.add(tok);
        }
        scanner.close();
        return cols;
    }
    
    public static Set<String> getProbandIdsFromPedFile(File pedfile) throws IOException {
        Set<String> affecteds = new HashSet<>();
		BufferedReader reader = new BufferedReader(new FileReader(pedfile));

		String line;
		while ((line = reader.readLine()) != null) {
            List<String> parts = getPedFileColumns(line, 9);
            String affected = parts.get(8);
            String id = parts.get(1);
			
            if (affected.equals("0") == false) {
                affecteds.add(id);
            }
		}
		reader.close();
		return affecteds;
    }
}
