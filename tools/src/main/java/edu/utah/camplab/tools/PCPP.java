package edu.utah.camplab.tools;

/*
 * Just a data struct of segment loading
 */

public class PCPP {
    public String peopleName = null;
    public int chromosome = 0;
    public String pedfileName = null;
    public String parfileName = null;
    public String assignedType = null;

    public PCPP(String dataline) {
        String[] parts = dataline.split("\\s");
        chromosome = Integer.parseInt(parts[0]);
        peopleName = parts[1];
        pedfileName = parts[2];
        parfileName = parts[3];
	assignedType = "";
    }

    public PCPP() {
        ;
    }
}
    
