package edu.utah.camplab.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.postgresql.core.BaseConnection;
import org.postgresql.copy.CopyManager;

public class ConnectingThreadPoolExecutor extends ThreadPoolExecutor {
    private String templateURL;

    public ConnectingThreadPoolExecutor(int corePool, int maxCores, long keepAlive, TimeUnit tu, ArrayBlockingQueue<Runnable> q) {
        super(corePool, maxCores, keepAlive, tu, q);
    }

    public void setTemplateURL (String url) {
        templateURL = url;
    }
    public String getURL() {
        return templateURL;
    }

    protected void beforeExecute(Thread eThread, Runnable runner) {
        // We'll presume we know whence we came
        ProbandsetWriterThread pwt = (ProbandsetWriterThread)runner;
        try {
            prepWriterThreadConnection(pwt);
        }
        catch(SQLException sqle) {
            throw new RuntimeException("beforeExection prep failure", sqle);
        }
        super.beforeExecute(eThread, runner);
    }

    private void prepWriterThreadConnection( ProbandsetWriterThread pwt) throws SQLException {
        Connection threadConn = reconnect();
        CopyManager copyManager = new CopyManager((BaseConnection)threadConn);
        pwt.setConnection(threadConn);
        pwt.setCopyIn(copyManager);
    }

    private Connection reconnect() throws SQLException {
        String prop = System.getProperty("upass");
        String[] parts = null;
        if ( prop == null ) {
            System.err.println("No \"upass\" property available");
            System.exit(2);
        }
        else {
            parts = prop.split(":");
        }
        return DriverManager.getConnection(templateURL, parts[0], parts[1]);
    }
}
