package edu.utah.camplab.tools;

import java.io.File;

public interface InterimFileManagerInterface {
    public void write(File jsonFile);
}
