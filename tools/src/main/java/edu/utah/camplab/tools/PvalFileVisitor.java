package edu.utah.camplab.tools;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.Files;
import java.nio.file.FileVisitResult;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class PvalFileVisitor extends SimpleFileVisitor<Path> {

    private BufferedReader reader = null;
    private HashMap<String, List<SegmentSearchCriterion>> searchValues = new HashMap<>();
    String currentLine = null;
    Long simsThisFile = null;
    
    public PvalFileVisitor (List<SegmentSearchCriterion> searchers) {
        for (SegmentSearchCriterion ssc : searchers) {
            //Don't need ssc lookup??
            String id = String.format("%d:%d", ssc.chromosome, ssc.basePosition);
            List<SegmentSearchCriterion> searcher = searchValues.get(id);
            if (searcher != null) {
                searcher.add(ssc);
            }
            else {
                ArrayList<SegmentSearchCriterion> searchList = new ArrayList<>();
                searchList.add(ssc);
                searchValues.put(id, searchList);
            }
        }
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return FileVisitResult.CONTINUE;
    }
    
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
    	if (attrs.isDirectory()) {
            return FileVisitResult.CONTINUE;
    	}
        if ( ! file.toString().contains(".pval")) {
            return FileVisitResult.CONTINUE;
    	}

        List<String> foundIds = getFilenameIdList(file);
        if (file.toString().endsWith(".gz")) {
            GZIPInputStream zstream = new GZIPInputStream(new FileInputStream(file.toString()));
            InputStreamReader rstream = new InputStreamReader(zstream);
            reader = new BufferedReader(rstream);
        }
        else {
            reader = Files.newBufferedReader(file);
        }
                
        if (foundIds == null) {
            foundIds = getListedProbands(file);
        }
        if (foundIds != null) {
            examineFile(file, foundIds);
        }
        reader.close();
        return FileVisitResult.CONTINUE;
    }
    
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    private void examineFile(Path file, List<String> foundIds) throws IOException {
        List<SegmentSearchCriterion> matchedCriteria = new ArrayList<>();
        for (List<SegmentSearchCriterion> criterionList : searchValues.values()) {
            for (SegmentSearchCriterion criterion: criterionList) {
                boolean idsAreGood = false;
                if (foundIds.containsAll(criterion.requiredIds)) {
                    idsAreGood = true;
                    for (String noncarrier : criterion.restrictedIds) {
                        if (foundIds.contains(noncarrier)) {
                            idsAreGood = false;
                            break;
                        }
                    }
                }
                if (idsAreGood) {
                    matchedCriteria.add(criterion);
                }
            }
        }
        if (matchedCriteria.size() > 0) {
            findConsumingSegment(file, foundIds, matchedCriteria);
        }
        reader.close();
    }
    
    private List<String> getFilenameIdList(Path path) {
        String fname = path.getFileName().toString();
        String[]parts = fname.split(".");
        int partCount = parts.length;
        if (partCount  > 2) {
            for (int i = 0; i < partCount - 1; i++) {
                String[] fileNameIds = parts[0].split("[,_-]");
                if (fileNameIds.length > 1) {
                    List<String> sortable = Arrays.asList(fileNameIds);
                    Collections.sort(sortable);
                    return sortable;
                }
            }
        }
        return null;
    }

    private List<String> getListedProbands(Path path) throws IOException {
        ArrayList<String> foundList = new ArrayList<>();
        currentLine = reader.readLine();
        //Requires first line to begin with #
        while ( currentLine != null && currentLine.startsWith("#") ) {
            String[] parts = currentLine.split("\\s");
            if (currentLine.contains("Proband")) {
                String csv = parts[parts.length - 1];
                foundList.addAll(Arrays.asList(csv.split(",")));
                Collections.sort(foundList);
                // we don't break/return here to go past headers
            }
            else if (currentLine.toLowerCase().contains("simulation")) {
                simsThisFile = Long.valueOf(parts[parts.length - 1]);
            }
            currentLine = reader.readLine(); 
        }
        return foundList;
    }
    
    private void findConsumingSegment(Path currentFile,
                                      List<String> idList,
                                      List<SegmentSearchCriterion> criteriaSubset) throws IOException {
        if (simsThisFile == null) {
            currentLine  = reader.readLine();
            while (currentLine.startsWith("#")) {
                if (currentLine.toLowerCase().contains("simulation")) {
                    String[] parts = currentLine.split("\\s");
                    simsThisFile = Long.valueOf(parts[parts.length - 1]);
                }
            }
        }
        //currentLine now has fist data line, read as part of reading headers
        ColumnDef columnDef = new ColumnDef(currentLine.split("\\s"));
        while (currentLine != null) {
            String[] parts = currentLine.split("\\s");
            int startBase = Integer.valueOf(parts[columnDef.startbp]);
            int endBase = Integer.valueOf(parts[columnDef.endbp]);
            ProtoSegment foundSegment = new ProtoSegment(currentFile, startBase, idList, simsThisFile);
            //TODO: add to constructor
            foundSegment.endbase = endBase; 
            //foundSegment.markers = Integer.valueOf(parts[columnDef.runcol]);
            foundSegment.lt = Integer.valueOf(parts[columnDef.ltcol]);
            foundSegment.eq = Integer.valueOf(parts[columnDef.eqcol]);
            foundSegment.gt = Integer.valueOf(parts[columnDef.gtcol]);
            
            ArrayList<SegmentSearchCriterion> culls = new ArrayList<>();
            for(SegmentSearchCriterion critter : criteriaSubset) {
                if (! (critter.chromosome == Integer.valueOf(parts[0]))) {
                    //TODOremove critter, also if current base > critter base
                    culls.add(critter);
                    continue;
                }
                if (foundSegment.startbase <= critter.basePosition && foundSegment.endbase >= critter.basePosition) {
                    critter.maybeUpdateSegment(foundSegment);
                }
            }
            criteriaSubset.removeAll(culls);
            if (criteriaSubset.size() == 0) {
                return;
            }
            currentLine = reader.readLine();
        }
    }

    private class ColumnDef {
        public int ltcol = -1;
        public int eqcol = -1;
        public int gtcol = -1;
        public int runcol = -1;
        public int startbp = -1;
        public int endbp = -1;

        public ColumnDef(String[] linedata) {
            switch (linedata.length) {
            case 9:
                runcol = 3;
                ltcol = 6;
                eqcol = 7;
                gtcol = 8;
                startbp = 4;
                endbp = 5;
                break;
            default:
                throw new RuntimeException(".pval structure unsupported");
            }
        }
    }
}
