package edu.utah.camplab.tools;

public enum SGSDebugEnum {
  DROPEVAL(1L, "deval"),
  WRITELINKAGE(1L << 1, "linkage"),
  WRITEGDROP(1L << 2, "drop"),
  WRITEPEEL(1L<<3, "peel"),
  TIMEDROP(1L<<4, "tdrop"),
  TIMEEVAL(1L<<5, "teval");

  private long maskValue;

  public long getMaskValue() {
    return maskValue;
  }

  public void setMaskValue(long maskValue) {
    this.maskValue = maskValue;
  }

  public String getUserHandle() {
    return userHandle;
  }

  public void setUserHandle(String userHandle) {
    this.userHandle = userHandle;
  }

  private String userHandle;

  SGSDebugEnum(long mv, String uh) {
    maskValue = mv;
    userHandle = uh;
  }

  @Override
  public String toString() {
    return getUserHandle();
  }

}


