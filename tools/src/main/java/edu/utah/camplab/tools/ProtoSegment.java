package edu.utah.camplab.tools;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;

import java.util.List;
import java.util.UUID;

import edu.utah.camplab.tools.ProtoSegment;

public class ProtoSegment {
    public List<String> definingIds = null;
    public String filename = null;

    public UUID dbid =null;
    public int startbase = -1;
    public int endbase = -1;
    public int startmarker;
    public int endmarker;
    public long lt = 0;
    public long eq = 0;
    public long gt = 0;
    public long sims = 0;

    public ProtoSegment() {}
    
    public ProtoSegment(Path path, int zerobase, List<String> probandList, long simn) {
        definingIds = probandList;
        filename = path.toString();
        startbase = zerobase + 1;
        endbase = zerobase + 2;
        lt = 0;
        eq = 0;
        gt = 0;
        sims = simn;
    }

    public void write(Writer writer) throws IOException {
        writer.append(String.format("\tSegment probands: %s\n", definingIds));
        writer.append("\tFile: " + filename + "\n");
        writer.append("\tSimulations: " + sims + "\n");
        writer.append(String.format("\tstart: %d, end: %d, number of markers: %d, LT: %d, EQ: %d, GT: %d, putative p-value: %5.3E\n",
                                    startbase, endbase, getMarkerRun(), lt, eq, gt, getPvalue()));
    }

    public boolean betterThan(ProtoSegment other) {
        return getPvalue() < other.getPvalue();
    }

    public float getPvalue() {
        float pv = 1.0f;
        //assert all > 1
        if (lt == -1 || eq == -1) {
            pv = (1.0f * gt) / sims;
        }
        else {
            pv = (eq + gt) / (1.0f * (lt + eq + gt));
        }
        return pv;
    }

    public int getMarkerRun() {
        return endmarker - startmarker + 1;
    }
    
    public long hits() {
        long h = -1;
        if (eq == -1) {
            h = gt;
        }
        else {
            h = gt + eq;
        }
        return h;
    }
}
