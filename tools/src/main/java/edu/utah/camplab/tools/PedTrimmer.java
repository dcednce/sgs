package edu.utah.camplab.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class PedTrimmer {
  public static String delimiter = "\\s+";
  public static int columns = 9;
  public static boolean usingProbandList = false;

  
  private Map<String, Pedigree> allPedigrees = new HashMap<>();
  private Map<String, Person> allPersons = new HashMap<>();

  public Map<String, Pedigree> getAllPedigrees() {
    return allPedigrees;
  }

  public Map<String, Person> getAllPersons() {
    return allPersons;
  }

  public static void main(String[] args) {
    try {
      OptionParser op = new OptionParser();
      op.formatHelpWith(new BuiltinHelpFormatter(120, 4));
      op.accepts("ped", "input ped file to be trimmed").withRequiredArg().ofType(File.class).required();
      op.accepts("trimmed", "output trimmed file").withRequiredArg().ofType(File.class).required();
      op.accepts("probands", "csv of proband ids (file or in-line)").withRequiredArg().ofType(String.class);
      op.accepts("separator", "ped file delimiter").withRequiredArg().ofType(String.class).defaultsTo("\\s+");
      op.accepts("columns", "pre-genotype columns in ped file (6 or 9)").withRequiredArg().ofType(Integer.class).defaultsTo(9);
      op.accepts("help", "list the options");

      OptionSet opSet = op.parse(args);
      if (opSet.has("help")) {
	op.printHelpOn(System.out);
	System.exit(0);
      }
      File inFile = (File) opSet.valueOf("ped");
      if (!inFile.exists()) {
	System.err.println("Error: The specified input file does not exist.");
	System.exit(0);
      }
      File outFile = (File) opSet.valueOf("trimmed");
      if (outFile.exists()) {
	System.err.println("Error: The specified output file already exists. Please remove it or use a different filename.");
	System.exit(0);
      }

      Set<String>  probandSet = new HashSet<>();
      if (opSet.has("probands")) {
        PedTrimmer.usingProbandList = true;
	probandSet = PedTrimmer.parseProbands((String) opSet.valueOf("probands"));
      }
      
      if (opSet.has("separator")) {
	PedTrimmer.delimiter = (String) opSet.valueOf("separator");
	System.out.println("Set delimiter to " + PedTrimmer.delimiter);
      }

      if (opSet.has("columns")) {
	columns = (Integer) opSet.valueOf("columns");
      }
      PedTrimmer trimmer = new PedTrimmer();
      trimmer.parseInput(inFile, probandSet);
      trimmer.trimAndWrite(outFile, probandSet);
    }
    catch (Exception exc) {
      System.out.println("Whoa! " + exc.getMessage());
      exc.printStackTrace();
    }
  }

  private void parseInput(File ifile, Set<String> pbSet) {
    try (BufferedReader br = new BufferedReader(new FileReader(ifile))) {
      String line;
      while ((line = br.readLine()) != null) {
        String[] lineData = line.split(PedTrimmer.delimiter, PedTrimmer.columns+1);
        String ped = lineData[0];
        Person person = new Person(Arrays.copyOfRange(lineData, 1, lineData.length));
	int pbIndexer = lineData.length == PedTrimmer.columns ? 1 : 2;
        if (! pbSet.contains(person.id())) {
          person.setAffected("0");
        }
        else if (lineData[lineData.length - pbIndexer].equals("1")){
          System.out.println("Keeping proband " + person.id());
        }
        allPersons.put(person.id(), person);  // Possbile more than once; overwrite
        allPedigrees.putIfAbsent(ped, new Pedigree(ped));
        allPedigrees.get(ped).addPerson(person);  // person (maybe) once per ped
      }
      System.out.println(String.format("Starting with %d people (unique) in %d pedigrees.", allPersons.size(), allPedigrees.size()));
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
      throw new RuntimeException("failed to open trim file: " + ifile.getAbsolutePath(), ioe);
    }
  }

  private Set<String> readPedLines(List<String> lines, Collection<Person> people) {
    Set<String> originalProbands = new HashSet<>();
    //        int missing = 0;
    for (String line : lines) {
      Person p = new Person(line);
      if (p.isAffected()) {
	originalProbands.add(p.id());
      }
      people.add(p);
    }
    return originalProbands;
  }

  public void trimAndWrite(File outfile, Set<String> probandSet) {
    try ( BufferedWriter w = new BufferedWriter(new FileWriter(outfile))) {
      for(Pedigree pedigree : getAllPedigrees().values()) {
	pedigree.tidy(true);
	Pedigree trimmed = pedigree.trimPedigree(probandSet);
	if (trimmed == null) {
	  continue;
        }
	for (Person p : trimmed.people()) {
	  w.write(pedigree.getPedigreeId());
	  //get the original record
	  p.writeLine(w, allPersons.get(p.id()).getGenotypes());  // because we don't know this person isn't faked
	}
      }
    }
    catch(IOException ioe) {
      System.err.println("Trouble writing trimmed peds: " + ioe.getMessage());
      ioe.printStackTrace();
    }
  }

  public static Set<String> getProbandIdsFromListFile(File probandListFile) {
    Set<String> probandIds = new HashSet<>();

    try (BufferedReader br = new BufferedReader(new FileReader(probandListFile))) {
      String line;
      while ((line = br.readLine()) != null) {
	line.replaceAll("\\s+", "");
	if (line.contains(",")) {
	  if ( line.startsWith("#")) {
	    //"# Probands:
	    line = line.substring(line.indexOf(':')+1);
	  }
	  String[] ids = line.split(",");
	  for (String anId : ids) {
	    probandIds.add(anId);
	  }
	  break;
	}
	if (! line.equals("")) {
	  probandIds.add(line);
	}
      }
    }
    catch (FileNotFoundException e) {
      throw new RuntimeException("could not find probands file. )" +  probandListFile.getAbsolutePath());
    }
    catch (IOException e) {
      throw new RuntimeException("trouble reading probands file. )" + probandListFile.getAbsolutePath());
    }
        
    return probandIds;
  }
    
  public static List<String> getLines(File f) {
    List<String> lines = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new FileReader(f))) {
      String line;
      while ((line = br.readLine()) != null) {
	lines.add(line);
      }
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
      throw new RuntimeException("failed to open trim file: " + f, ioe);
    }
    return lines;
  }
    

  private static Set<String> parseProbands(String pbcsv) {
    if (pbcsv == null) {
      return (Set<String>)null;
    }
    Set<String> probandSet = new HashSet<String>();
    if (pbcsv.contains(",")) {
      String[] ids = pbcsv.split(",");
      probandSet.addAll(new HashSet<String>(Arrays.asList(ids)));
    }
    else {
      File pbFile = new File(pbcsv);
      if (! pbFile.exists()) {
	throw new RuntimeException("Could not find probands file: " + pbFile.getAbsolutePath());
      }
      try (BufferedReader br = new BufferedReader(new FileReader(pbFile))) {
	String line;
	while ((line = br.readLine()) != null) {
	  String[] ids = line.split(",");
	  probandSet.addAll(new HashSet<String>(Arrays.asList(ids)));
	}
      }
      catch (IOException ioe) {
	ioe.printStackTrace();
	throw new RuntimeException("failed to open probands file: " + pbFile, ioe);
      }
    }
    return probandSet;
  }
}

class Person implements Comparable<Person> {
  // ped id has been stripped
  // ego pa ma [nps nms ns] sex affected GTs
  // sex index = length-3
  // affected index = length-2
  // GT index = length-1
  private String[] pedfields;

  public Person(String[] pedline) {
    pedfields = pedline;
    if (pedfields.length < PedTrimmer.columns - 1)
      System.out.println("bad person: " + pedline);
    if (pedfields[1].length() == 0)
      pedfields[1] = "0";
    if (pedfields[2].length() == 0)
      pedfields[2] = "0";
  }

  public Person(String rline) {
    this(Arrays.copyOfRange(rline.split(PedTrimmer.delimiter, PedTrimmer.columns + 1),1, PedTrimmer.columns));
  }

  public Person(Person o) {
    pedfields = o.copy();
  }
  public String id() {
    return pedfields[0];
  }
    
  public String fid() {
    return pedfields[1];
  }
    
  public String[] copy() {
    return Arrays.copyOf(pedfields, pedfields.length);
  }
  public void setFid(String id) {
    pedfields[1] = id;
  }
  public String mid() {
    return pedfields[2];
  }
  public void setMid(String id) {
    pedfields[2] = id;
  }

  public boolean isMale() {
    return pedfields[pedfields.length - 3].equals("1");
  }
    
  public boolean isAffected() {
    return pedfields[pedfields.length - 2].equals("1");
  }

  public void setAffected(String val) {
    pedfields[pedfields.length - 2] = val;
  }
  
  public boolean hasParents() {
    return hasFather() && hasMother();
  }
    
  public boolean hasMother() {
    return !pedfields[2].equals("0");
  }
    
  public boolean hasFather() {
    return !pedfields[1].equals("0");
  }
    
  public void removeMother() {
    pedfields[2] = "0";
  }
    
  public void removeFather() {
    pedfields[1] = "0";
  }
    
  public String getGenotypes() {
    // we removed column 1 of input line, GTs at last index
    if (pedfields.length == PedTrimmer.columns)
      return pedfields[pedfields.length -  1];
    else
      return "";
  }
    
  public void writeLine(BufferedWriter w, String gts) throws IOException {
    for (int i = 0; i < pedfields.length -1 ; i++) {
      w.write(PedTrimmer.delimiter);
      w.write(pedfields[i]);
    }
    w.write(PedTrimmer.delimiter);
    w.write(gts);
    w.newLine();
  }
    
  @Override
  public String toString() {
    return ""+id();
  }
    
  @Override
  public int hashCode() {
    return id().hashCode();
  }

  @Override
  public int compareTo(Person p) {
    return id().compareTo(p.id());
  }
    
  @Override
  public boolean equals(Object p) {
    if (p instanceof Person) {
      return compareTo((Person) p) == 0;
    }
    return false;
  }
}

class Pedigree {
  private String pedigreeId;

  private Map<String, Person> idPersonMap;
  private Map<String, Set<Person>> parentOf;
  private Set<Person> mothers;
  private Set<Person> fathers;
    
  //  public Pedigree(Collection<Person> people) {
  public Pedigree(String id) {
    pedigreeId = id;
    idPersonMap = new HashMap<String, Person>();
    parentOf = new HashMap<String, Set<Person>>();
    mothers = new HashSet<>();
    fathers = new HashSet<>();
  }

  public void addPerson(Person p) {
    idPersonMap.put(p.id(), p);
  }

  public String getPedigreeId() {
    return pedigreeId;
  }
  
  public Map<String, Person> getIdPersonMap() {
    return idPersonMap;
  }

  public void tidy(boolean first) {
    //First time through we've just read in original records, nothing trimmed.
    //Subsequently we're trimming off ancestors from duplicate of original person
    HashMap<String, HashSet<Person>> missingMothers = new HashMap<>();
    HashMap<String, HashSet<Person>> missingFathers = new HashMap<>();
    for (Person ego : idPersonMap.values()) {
      //maybeNukeParents(ego);
      if (ego.hasMother()) {
	if (idPersonMap.containsKey(ego.mid())) {
	  Person mom = idPersonMap.get(ego.mid());
	  mothers.add(mom);
	  addToParents(mom, ego);
	}
	else {
	  if ( ! missingMothers.containsKey(ego.mid())) {
	    HashSet<Person> orphans = new HashSet<>();
	    orphans.add(ego);
	    missingMothers.put(ego.mid(), orphans);
	  }
	  else {
	    missingMothers.get(ego.mid()).add(ego);
	  }
	}
      }
      if (ego.hasFather()) {
	if (idPersonMap.containsKey(ego.fid())) {
	  Person dad = idPersonMap.get(ego.fid()); 
	  fathers.add(dad);
	  addToParents(dad, ego);
	}
	else {
	  if ( ! missingFathers.containsKey(ego.fid())) {
	    HashSet<Person> orphans = new HashSet<>();
	    orphans.add(ego);
	    missingFathers.put(ego.fid(), orphans);
	  }
	  else {
	    missingFathers.get(ego.fid()).add(ego);
	  }
	}
      }
    }

    if ( first ) {
      return;
    }
    // Now we have to find the egoless parents to see if they have more than one child.
    // If so re-instate as ego.
    for (Map.Entry<String, HashSet<Person>> me: missingMothers.entrySet()) {
      if (me.getValue().size() > 1) {
	augmentIdMap(me.getKey(), 2);
      }
      else {
	me.getValue().iterator().next().removeMother();
      }
    }
    for (Map.Entry<String, HashSet<Person>> me : missingFathers.entrySet()) {
      if (me.getValue().size() > 1) {
	augmentIdMap(me.getKey(), 1);
      }
      else {
	me.getValue().iterator().next().removeFather();
      }
    }
  }

  private void addToParents(Person parent, Person child) {
    if (! parentOf.containsKey(parent.id())) {
      HashSet<Person> kids = new HashSet<>();
      kids.add(child);
      parentOf.put(parent.id(), kids);
    }
    else {	
      parentOf.get(parent.id()).add(child);
    }
  }    
  private void warnMissing(String egoless) {
    System.out.println("Looks like " + egoless + " is missing an ego record in pedigree " + pedigreeId) ;
  }

  // // Here we take of degenerate input
  // augmentIdMap(mothers, pedigreeId, 2);
  // augmentIdMap(fathers, pedigreeId, 1);
  public void maybeNukeParents(Person child){
    if (child.hasMother()) {
      if (! idPersonMap.containsKey(child.id()))
        child.setMid("0");
    }
    if (child.hasFather()) {
      if (! idPersonMap.containsKey(child.id())) {
        child.setFid("0");
      }
    }
  }
  public Collection<Person> people() {
    return idPersonMap.values();
  }
    
  public Collection<Person> children(Person p) {
    return parentOf.get(p.id());
  }
    
  public Set<Person> addProbands(Set<String> probands) {  //todo: rename to getAffected
    Set<Person> affected = new HashSet<Person>();
    ArrayList<Person> proband_list = null;
    if (probands != null) {
      proband_list = new ArrayList<Person>();
      for (String proband : probands) {
	Person p = idPersonMap.get(proband);
	if (p ==  null) {
	  //System.out.println("missing proband: " + proband + " in pedigree " + pedigreeId);
	  continue;
	}
	if (p.isAffected()) {
	  proband_list.add(p);
	}
	else {
	  //throw new RuntimeException(proband + " is not a designated proband");
          System.out.println(proband + " no longer a proband in pedigree " + pedigreeId);
	}
      }
    }
    else {
      proband_list = new ArrayList<Person>();
      for (Person p : idPersonMap.values()) {
	if (p.isAffected()) {
	  proband_list.add(p);
	}
      }
    }
    for(Person person : proband_list){
      Person dup = new Person(person);
      affected.add(dup);
    }
    return affected;
  }
    
  public Pedigree trimPedigree( Set<String> probands) {
    Pedigree trimmed = new Pedigree(pedigreeId+"-trim");
    Set<Person> affected = addProbands(probands);  // start with the probands of this ped
    if (affected.size() < 2) {
      System.out.println(String.format("Pedigree %s has been trimmed out of existance", pedigreeId));
      return null;
    }
    getCommonAncestors(affected, trimmed);

//    for (Person p : people()) {  // All the original people?
//      if (! trimmed.idPersonMap.containsKey(p.id())) {
//        Person trimParent = getIdPersonMap().get(p.id());
//        Collection<Person> pchildren = trimmed.children(trimParent);
//	if (pchildren != null) {
//	  for (Person c : pchildren) {
//	    if (p.id().equals(c.fid())) {
//	      c.removeFather();
//	    }
//	    if (p.id().equals(c.mid())) {
//	      c.removeMother();
//	    }
//	  }
//	}
//      }
//    }
    trimmed.tidy(false);
    trimmed.clean(idPersonMap);
    return trimmed;
  }

  private void clean(Map<String, Person> source) {
    for (Person p : people()) {
      if (p.hasFather() && !idPersonMap.containsKey(p.fid())) {
        p.removeFather();
      }
      if (p.hasMother() && !idPersonMap.containsKey(p.mid())) {
        p.removeMother();
      }
    }
  }

  private void getCommonAncestors(Collection<Person> affected, Pedigree trimmed) {
    for (Person ego : affected) {
      trimmed.idPersonMap.put(ego.id(), ego);
    }

    Person[] arr = affected.toArray(new Person[0]);
    for (int i = 0; i < arr.length - 1; i++) {
      Person pi = arr[i];
      for (int j = i + 1; j < arr.length; j++) {
	Person pj = arr[j];
	Set<Person> ancestors = getCommonAncestors(pi, pj);
	if (!ancestors.isEmpty()) {
	  for(Person offs : getDecendants(ancestors, pi))
	    trimmed.idPersonMap.put(offs.id(), new Person(offs));
	  for(Person offs : getDecendants(ancestors, pj))
            trimmed.idPersonMap.put(offs.id(), new Person(offs));
	}
      }
    }
  }

  public Set<Person> getCommonAncestors(Person p1, Person p2) {
    Set<Person> anc = getAncestors(p1);
    anc.retainAll(getAncestors(p2));
    if (anc.size() < 2) {
      return anc;
    }
    for (Person p : anc.toArray(new Person[0])) {
      Person father = idPersonMap.get(p.fid());
      Person mother = idPersonMap.get(p.mid());
      if (p.hasFather() && anc.contains(father)) {
	anc.remove(father);
      }
      if (p.hasMother() && anc.contains(mother)) {
	anc.remove(mother);
      }
    }
    return anc;
  }
    
  private Set<Person> getAncestors(Person p) {
    Set<Person> anc = new HashSet<Person>();
    Person base = new Person(p);
    anc.add(base);
    if (base.hasFather()) {
      Person father = idPersonMap.get(p.fid());
      if (father != null)
        anc.addAll(getAncestors(father));
      else
	base.setFid("0");
    }
    if (base.hasMother()) {
      Person mother = idPersonMap.get(p.mid());
      if (mother != null)
        anc.addAll(getAncestors(mother));
      else
	base.setMid("0");
    }
    return anc;
  }

  public Set<Person> getDecendants(Set<Person> ancestors, Person p) {
    Set<Person> decendants = new HashSet<Person>();
    if (ancestors != null) {
      for (Person ancestor : ancestors) {
	decendants.addAll(getDecendants(ancestor, p));
      }
    }
    return decendants;
  }

  private Set<Person> getDecendants(Person ancestor, Person p) {
    Set<Person> decendants = new HashSet<Person>();
    if (ancestor.equals(p)) { 
      decendants.add(ancestor);
      return decendants;
    }
    Set<Person> kids = parentOf.get(ancestor.id());
    decendants.addAll(getDecendants(kids, p));
    if (!decendants.isEmpty()) {
      for (Person kid : kids) {
	if (decendants.contains(kid)) {
	  if (kid.hasFather()) {
	    decendants.add(idPersonMap.get(kid.fid()));
	  }
	  if (kid.hasMother()) {
	    decendants.add(idPersonMap.get(kid.mid()));
	  }
	}
      }
    }
    return decendants;
  }

  private void augmentIdMap(String lostId, int morf) {
    // this is just a place holder person
    String fake = String.format("%s 0 0 %d 0 0 0 0 0", lostId, "0 0", morf);
    String[] fakeSplit = fake.split("\\s", 6);
    idPersonMap.put(lostId, new Person(fakeSplit));
  }
}
