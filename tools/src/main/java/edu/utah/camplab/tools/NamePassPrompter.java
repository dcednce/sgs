package edu.utah.camplab.tools;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;

public class NamePassPrompter {
    public static Map<String, String> prompt() {
        Map<String, String> npMap = new HashMap<>();

        Console console = System.console();
        console.printf("Enter db username: ");
        String userName = console.readLine();
        npMap.put("name", userName);
        console.printf("And the password is: ");
        char[] pwd = console.readPassword();
        String password = new String(pwd);
        npMap.put("password", password);

        return npMap;
    }
}
