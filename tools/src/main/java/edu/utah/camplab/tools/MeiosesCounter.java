
package edu.utah.camplab.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpsgcs.graph.DAG;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageId;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;

public class MeiosesCounter {

    public static Logger logger = LoggerFactory.getLogger(MeiosesCounter.class);

    private LinkagePedigreeData            peddata       = null;
    private LinkagePedigreeData[]          peddataSplits = null;
    private LinkageParameterData           pardata       = null;
    private int                            num_probands  = 0;
    private int                            num_mrca      = 0;
    private LinkageIndividual[]            people        = null;
    private ArrayList<LinkageIndividual>   probands      = null;
    private DAG<LinkageIndividual, Object> peopledag     = null;
    
    public MeiosesCounter(File fullPed, File fullPar) {
        try {
            LinkageFormatter lf = new LinkageFormatter(new BufferedReader(new FileReader(fullPed)), fullPed.getName());
            peddata = new LinkagePedigreeData(lf, null, false);
            peddataSplits = peddata.splitByPedigree();
            if (fullPar != null) {
                lf = new LinkageFormatter(new BufferedReader(new FileReader(fullPar)), fullPar.getName());
                pardata = new LinkageParameterData(lf, false);
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(2);
        }
    }
    
    public MeiosesCounter(File fullPed) {
        this(fullPed, null);
    }
    
    public MeiosesCounter(LinkagePedigreeData data) {
        peddata = data;
        peddataSplits = peddata.splitByPedigree();
    }

    public MeiosesCounter(LinkageInterface LI) {
        this(LI.raw().getPedigreeData());
        pardata = LI.raw().getParameterData();
    }
    
    public LinkagePedigreeData getPeddata() {
        return peddata;
    }

    public void setPeddata(LinkagePedigreeData peddata) {
        this.peddata = peddata;
    }

    public LinkagePedigreeData[] getPeddataSplits() {
        return peddataSplits;
    }

    public void setPeddataSplits(LinkagePedigreeData[] peddataSplits) {
        this.peddataSplits = peddataSplits;
    }

    public int getCount(int splitIndex) {
        people = getSplit(splitIndex).getIndividuals();
        peopledag = new DAG<LinkageIndividual, Object>(); // DAG with everybody
        probands = new ArrayList<LinkageIndividual>();

        for (int i = 0; i < people.length; i++) {
            peopledag.connect(peddata.pa(people[i]), people[i]);
            peopledag.connect(peddata.ma(people[i]), people[i]);
            if (people[i].proband == 1) {
                probands.add(people[i]);
            }
        }

        Set<LinkageIndividual> ids = new LinkedHashSet<LinkageIndividual>();
        CountHolder ch = getCount(0, ids);
        return ch.descendants;
    }

    public CountHolder getCount(int f, Set<LinkageIndividual> indivSet) {
        Set<LinkageIndividual> rel_anc = new LinkedHashSet<LinkageIndividual>();
        Set<LinkageIndividual> common_anc = new LinkedHashSet<LinkageIndividual>();
        common_anc.addAll(peopledag.getVertices());
        num_probands = 0;
        CountHolder ch = new CountHolder();

        for (LinkageIndividual person : people) {
            if ((person.proband == 1) && !(indivSet.contains(person))) {
                num_probands++; // common_anc=\bigcap{i\cup anc(i)}
                Set<LinkageIndividual> temp = new LinkedHashSet<LinkageIndividual>();
                temp.addAll(peopledag.ancestors(person));
                temp.add(person);
                rel_anc.addAll(temp);
                common_anc.retainAll(temp);
            }
        }
        if (num_probands == 0) {
            ch.hasProbands = false;
            return ch;
        }
        else {
            common_anc.remove(null);
            Set<LinkageIndividual> rem_anc = new LinkedHashSet<LinkageIndividual>();

            for (LinkageIndividual i : common_anc) {
                Set<LinkageIndividual> temp = new LinkedHashSet<LinkageIndividual>();
                for (LinkageIndividual j : common_anc) {
                    temp.addAll(peopledag.ancestors(j));
                }
                if (temp.contains(i)) {
                    rem_anc.add(i);
                }
            }

            for (LinkageIndividual j : rem_anc) {
                rel_anc.remove(j);
                common_anc.remove(j);
            }

            num_mrca = common_anc.size();
            if (num_mrca == 0) {
                ch.ancestors = 0;
                return ch;
            }
            else if (num_mrca > 2) {
                ch.countMRCA = num_mrca;
            }
            else if (num_mrca == 2) {
                Collection<LinkageIndividual> com_kids = new LinkedHashSet<LinkageIndividual>();
                com_kids.addAll(rel_anc);
                for (LinkageIndividual i : common_anc) {
                    com_kids.retainAll(peopledag.children(i));
                }
                if (com_kids.isEmpty()) {
                    ch.unmarriedMRCA = true;
                    return ch;
                }
            }
            Collection<LinkageIndividual> desc = new LinkedHashSet<LinkageIndividual>();
            for (LinkageIndividual i : common_anc) {
                desc = peopledag.descendants(i);
                break;
            }
            desc.retainAll(rel_anc);
            ch.ancestors = common_anc.size();
            ch.descendants = desc.size();
        }
        return ch;
    }

    public LinkagePedigreeData[] getSplits() {
        return peddataSplits; 
    }

    public LinkagePedigreeData getSplit(int ix) {
        return peddataSplits[ix];
    }

    public void tallyMeioses() {
        for (int k = 0; k < getSplits().length; k++) {
            people = getSplit(k).getIndividuals();
            peopledag = new DAG<LinkageIndividual, Object>(); // DAG with everybody
            probands = new ArrayList<LinkageIndividual>();

            for (int i = 0; i < people.length; i++) {
                peopledag.connect(peddata.pa(people[i]), people[i]);
                peopledag.connect(peddata.ma(people[i]), people[i]);
                if (people[i].proband == 1) {
                    probands.add(people[i]);
                }
            }
            System.out.println("\n\nPedigree " + getSplit(k).firstPedid().stringGetId() + "\tAll in\n-------------------------------------------------");
            Set<LinkageIndividual> ss = new LinkedHashSet<LinkageIndividual>();
            CountHolder countHolder = getCount(k, ss);

            if (countHolder.countMRCA > 2) {
                System.out.println("\tmore than 2 MRCA");
            }
            else if (countHolder.unmarriedMRCA) {
                System.out.println("\t2 unmarried MRCA");
            }
            else {
                System.out.println("[]\tNumber of common ancestors=" + countHolder.ancestors + "\tNumber of meioses=" + countHolder.descendants);
            }
        }
    }

    public LinkageIdSet pullProbandIdSet() {
        return new LinkageIdSet(peddata);
    }
    
    public int[] getGenderCounts(Set<LinkageId<?>> subset, LinkagePedigreeData lpd) {
        //unknown, male, female
        int[] genderCounts = new int[]{0,0,0};
        
        for(LinkageId<?> lid : subset) {
            genderCounts[lpd.find(lid).sex]++;
        }
        return genderCounts;
    }

    public double[] getKinshipCoeffRange(Set<LinkageId<?>> subset) {
        LinkageInterface linkageInf = new LinkageInterface(new LinkageDataSet(pardata, peddata));
        double minKin = 1.0;
        double maxKin = 0.0;
        for (LinkageId<?> lid1 : subset) {
            for (LinkageId<?> lid2 : subset) {
                if (lid1.equals(lid2)) {
                    continue;
                }
                double kincoef = linkageInf.getKinshipCoeff(peddata.getIndividualIndex(lid1),
                                                            peddata.getIndividualIndex(lid2));
                maxKin = Math.max(maxKin, kincoef);
                minKin = Math.min(minKin, kincoef);
            }
        }
        return new double[]{minKin, maxKin};
    }

    public LinkageId<?> pullPedId(LinkageId<?> ego, LinkagePedigreeData lpd) {
        return lpd.find(ego).pedid;
    }

    public class CountHolder {
        public int               ancestors     = 0;
        public int               descendants   = 0;
        public int               countMRCA     = 0;
        public boolean           unmarriedMRCA = false;
        public boolean           hasProbands   = false;
        ArrayList<LinkageId<?>> ids           = new ArrayList<LinkageId<?>>();

        public boolean isReportable() {
            return (!unmarriedMRCA) && countMRCA <= 2 && ancestors > 0 && hasProbands;
        }
    }
}
