package edu.utah.camplab.tools;

import java.io.File;
import java.io.IOException;

public class FileRotate {
    private File[] fileSet;
    private String rootName;
    private int setSize;

    public FileRotate (String root, int sz) {
        rootName = root;
        setSize = sz;
        fileSet = new File[setSize];
        prefill();
    }
    
    public static void main(String[] args) {
        FileRotate fr = new FileRotate(args[0], Byte.parseByte(args[1]));
        fr.runTest();
    }
    
    public void runTest() {
    	prefill();
    	File seed = new File(rootName);
    	try {
    		seed.createNewFile();
    		fileSet[0] = seed;
    		// Leave first one blank, fake others
    		fileSet[0] = null; 
    		for (byte loop = 0; loop<setSize; loop++) {
    			rotate();
    		}
    	}
    	catch(IOException ioe) {
    		;
    	}
    }

    public synchronized void rotate() throws IOException{
        if (! fileSet[0].exists() ) {
            return;
        }
        for (int i = setSize-2; i >= 0; i--) {
            File f1 = fileSet[i];
            File f2 = fileSet[i+1];
            if (f1.exists()) {
                if (f2.exists()) {
                    f2.delete();
                }
                f1.renameTo(f2);
            }
        }
    }
    
    public void prefill() {
        fileSet[0] = new File(rootName);
        for (int i = 1; i < setSize; i++) {
            fileSet[i] = new File(String.format("%s.%d", rootName, i));
        }
    }

}
