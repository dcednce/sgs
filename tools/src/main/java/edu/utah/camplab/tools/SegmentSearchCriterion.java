package edu.utah.camplab.tools;

import java.io.Writer;
import java.io.IOException;
import java.util.List;

public class SegmentSearchCriterion {
    public final int chromosome;
    public final long basePosition;
    public final List<String> requiredIds;
    public final List<String> restrictedIds;
    public ProtoSegment bestSegment;

    public SegmentSearchCriterion(int c, long b, List<String> req, List<String> rest) {
        chromosome = c;
        basePosition = b;
        requiredIds = req;
        restrictedIds = rest;
        
    }

    public void write(Writer w) throws IOException {
        w.append(String.format("Variant -> chrom: %2d, basePos: %d, required: %s, restricted %s\n",
                               chromosome, basePosition, requiredIds, restrictedIds));
    }

    public void maybeUpdateSegment(ProtoSegment updater) {
        if (bestSegment == null || updater.betterThan(bestSegment)) {
            bestSegment = updater;
        }
    }

}
