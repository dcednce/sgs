package jpsgcs.linkage;


import java.io.IOException;
import java.util.Map;

public class SNPAlleleLocus extends AbstractOrderedAlleleLocus<Character> {

    public LinkagePhenotype makePhenotype(LinkageIndividual subject) {
        return new SNPPhenotype(this);
    }

    public String toString() {
        StringBuffer buf = new StringBuffer(String.format("%d 0 0 5\n", getAlleleMap().size() - 1));
        buf.append(String.format("%d %d %s\n",type,freq.length,line1comment));
        for (double alleleFreq : freq) {
            buf.append(String.format("%8.6f", alleleFreq));
        }
        //TODO: get line2comment going
        for (Map.Entry< Allele<Character>,Integer> me : getAlleleMap().entrySet()) {
            buf.append(String.format(" %c", me.getKey().getValue()));
        }
        buf.append("\n");
        return buf.toString();
    }
    public void read(LinkageFormatter b) throws IOException {
        int na = b.readInt("number of alleles",0,true,true);
        for (int i = 0; i < na; i++) {
            String alleleValue = b.readString("snp allele", '0', true, true);
            getAlleleMap().put(new Allele<Character>(alleleValue.charAt(0)), i+1);
        }
        //Add in the value for unknown
        getAlleleMap().put(new Allele<>(null), 0);
        line1comment = b.restOfLine();
        freq = new double[na];
        b.readLine();
        for (int i=0; i<freq.length; i++)
        {
            freq[i] = b.readDouble("allele frequency "+i,0,true,true);
            if (freq[i] < 0)
                b.crash("Negative allele frequency "+freq[i]);
        }

        checkAndSetAlleleFrequencies(freq,b);

        line2comment = b.restOfLine();

        return;
    }

}
