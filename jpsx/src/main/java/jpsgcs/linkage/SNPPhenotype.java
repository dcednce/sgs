package jpsgcs.linkage;

import java.util.Map;

public class SNPPhenotype extends AbstractOrderedAllelePhenotype {

    public SNPPhenotype() {
        ;
    }
    public SNPPhenotype(SNPAlleleLocus locus) {
      setLocus(locus);
    }
    public Allele<Character> getSNPAllele(int ord) {
         SNPAlleleLocus locus = ((SNPAlleleLocus)getLocus());
         return locus.getOrderedAllele(ord);
     }

     public char getAlleleValue(int ord) {
         Allele<Character> ab = ((SNPAlleleLocus)getLocus()).getOrderedAllele(ord);
         return (char)ab.getValue();
     }

    public boolean setAlleles(int a1, int a2) {
        return setAlleles((Integer)a1, (Integer)a2);
    }

     /**we assume the args are in fact the ordinals*/
    public boolean setAlleles(Integer a1, Integer a2) {
        // all allele ordinals are > 0, which is unknown/untyped value
        SNPAlleleLocus locus = (SNPAlleleLocus) getLocus();
        if ( locus.getAlleleMap().keySet().contains(a1) &&  locus.getAlleleMap().keySet().contains(a2) ) {
            setAllele1Ord(a1);
            setAllele2Ord(a2);
        }
        else {
            throw new IndexOutOfBoundsException(String.format("Allele ordinal(s) [%d, %d] out of range for locus of size %d", a1, a2, locus.getAlleleMap().size()));
        }
        return true;
    }
}
