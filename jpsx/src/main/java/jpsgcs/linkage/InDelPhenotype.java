package jpsgcs.linkage;

public class InDelPhenotype extends AbstractOrderedAllelePhenotype{

    public InDelPhenotype(InDelAlleleLocus locus) {
        setLocus(locus);
    }
    public Allele<String> getInDelAllele(int ord) {
        InDelAlleleLocus locus = ((InDelAlleleLocus)getLocus());
        return locus.getOrderedAllele(ord);
    }

    public String getAlleleValue(int ord) {
        Allele<String> ab = ((InDelAlleleLocus)getLocus()).getOrderedAllele(ord);
        return ab.getValue();
    }

    public boolean setAlleles(int a1, int a2) {
        InDelAlleleLocus locus = (InDelAlleleLocus) getLocus(); //Until LinkageLocus is generic...
        if (a1 > locus.getAlleleMap().size() || a2 >= locus.getAlleleMap().size()) {
            throw new UnsupportedOperationException("setAllele given bad ordinals");
        }
        setAllele1Ord(a1);
        setAllele2Ord(a2);
        return true;
    }
}
