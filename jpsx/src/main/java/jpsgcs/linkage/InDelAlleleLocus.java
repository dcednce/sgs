package jpsgcs.linkage;

import java.io.IOException;

public class InDelAlleleLocus extends AbstractOrderedAlleleLocus<String>{
    public LinkagePhenotype makePhenotype(LinkageIndividual subject) {
        return new InDelPhenotype(this);
    }

    public String toString() {
        return String.format("%d 0 0 5", getAlleleMap().size() - 1);
        //etc.  Really this is expected to make a .par file entry for this locus.
    }
    public void read(LinkageFormatter b) throws IOException {
        int na = b.readInt("number of alleles",0,true,true);
        for (int i = 0; i < na; i++) {
            String alleleValue = b.readString("snp allele", '0', true, true);
            getAlleleMap().put(new Allele<String>(alleleValue), i+1);
        }
        //Add in the value for unknown
        getAlleleMap().put(new Allele<>(null), 0);
        line1comment = b.restOfLine();
        freq = new double[na];
        b.readLine();
        for (int i=0; i<freq.length; i++)
        {
            freq[i] = b.readDouble("allele frequency "+i,0,true,true);
            if (freq[i] < 0)
                b.crash("Negative allele frequency "+freq[i]);
        }

        checkAndSetAlleleFrequencies(freq,b);

        line2comment = b.restOfLine();

        return;

    }

}
