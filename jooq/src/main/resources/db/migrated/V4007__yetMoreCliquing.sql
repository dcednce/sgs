alter table base.ld_clique drop constraint ld_clique_ldid_fkey;
drop index if exists ld_clique_ldid_fkey;
alter table base.ld_clique rename ld_id to markerset_id;

update base.ld_clique c set markerset_id = l.markerset_id
from base.ld l
where c.markerset_id = l.id;

alter table base.ld_clique add foreign key(markerset_id) references base.markerset(id);
alter table base.ld_clique add primary key (markerset_id, ordinal);

drop table base.ld;
