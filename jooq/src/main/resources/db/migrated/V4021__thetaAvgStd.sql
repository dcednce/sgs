alter table base.markerset add column avg_theta float, add column std_theta float;
update base.markerset s set avg_theta = a.thetaavg, std_theta = a.thetastd
from  (select markerset_id, avg(theta) as thetaavg, stddev(theta) as thetastd 
      from base.markerset_member group by markerset_id) as a
where s.id = a.markerset_id;

