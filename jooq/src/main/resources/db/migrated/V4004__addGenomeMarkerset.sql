create table base.genome_markerset (
  id uuid primary key,
  name text not null,
  description text
);

alter table base.markerset add column genome_id uuid;
alter table base.markerset add foreign key (genome_id) references base.genome_markerset(id);
