alter table base.genome_markerset add column primary_build int null,
      add constraint build_check CHECK((primary_build = any(array[37,38]::int[])))
;
