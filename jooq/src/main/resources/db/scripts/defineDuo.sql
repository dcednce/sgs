create or replace function define_duo(duo_name text, people_names text[])
returns void
as $$
declare
  duo_id  uuid;        
  people_ids uuid[];
  alen int;
  blen int;
begin
  select array_agg(id) into people_ids from probandset_group where name = any(people_names);
  alen = array_length(people_ids);
  if alen is null then
     raise notice 'no probandset groups found, trying pedigrees (people)';
     select array_agg(id) into people_ids from people where name = any(people_names);
  end if;
  alen = array_length(people_ids);
  blen = array_length(people_names);
  if alen is null or alen != blen then
     raise exception '% name(s) not found of %', blen - alen, array_to_string(people_names, '/');
  end if;
  select uuid_generate_v4() into duo_id;
  raise notice 'Creating duo entry with id % for %', duo_id, duo_name;
  insert into duo values(
         duo_id
         , duo_name
         , people_ids
         , 0.0
         , 0.0);
end;
$$
language plpgsql;  
         
