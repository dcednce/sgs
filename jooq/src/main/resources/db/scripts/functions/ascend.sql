set search_path=base,public;

create or replace function public.ascend(focus uuid)
returns table (id uuid, name text, pa uuid, ma uuid, gender char(1), gennum int) as
$$
with recursive generation(id, name, pa, ma, gender, gen) as (
     select  id, name, pa, ma, gender, 0::int as gennum from person
     where id = focus
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gen-1
     from person b, generation g
     where (b.gender = 'f' and g.ma = b.id) or (b.gender = 'm' and g.pa = b.id)
)
select * from generation;
$$
language sql;


create or replace function public.ascend_all(focus uuid[])
returns table (id uuid, name text, pa uuid, ma uuid, gender char(1), is_proband boolean) as
$$
with recursive generation(id, name, pa, ma, gender, gennum) as (
     select  id, name, pa, ma, gender, 0::int as gennum from person
     where id = any(focus)
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gennum-1
     from person b, generation g
     where (b.gender = 'f' and g.ma = b.id) or (b.gender = 'm' and g.pa = b.id)
)
select id, name, pa, ma, gender, id = any(focus) as is_proband from generation group by id, name, pa, ma, gender
$$
language sql;

create or replace function public.ascend_trim(focus uuid[])
returns table (id uuid, pa uuid, ma uuid, gender char(1), is_proband bool) as
$$
declare
  delcount int := 1;
begin
  drop table if exists gevt;
  create temp table gevt as
  select * from ascend_all(focus);
  while delcount > 0 loop
    drop table if exists singleton;
    create temp table singleton as
    with ped as (select * from gevt),
    founder as (select p.id, p.gender from ped p where p.ma is null and p.pa is null),
    fmother as (select f.id, count(*) as kids from founder f join ped p on f.id = p.ma where f.gender = 'f' group by f.id having count(*) = 1),
    ffather as (select f.id, count(*) as kids from founder f join ped p on f.id = p.pa where f.gender = 'm' group by f.id having count(*) = 1),
    singleton as (select * from ped p where p.ma in (select fm.id from fmother fm where fm.kids = 1) and p.pa in (select ff.id from ffather ff where ff.kids = 1))
    select * from singleton;
    update gevt g set ma = null, pa = null where g.id in (select s.id from singleton s);
    delete from gevt g where g.id in (select t.ma from singleton t union select s.pa from singleton s);
    get diagnostics delcount = ROW_COUNT;
    raise notice 'dropped % singleton parents', delcount;
  end loop;
  -- return query select g.id, g.pa, g.ma, g.gender, case when d.id is not null then true else false end as proband 
  -- from gevt g left join dna d on g.id = d.id;
  return query select g.id, g.pa, g.ma, g.gender, case when g.id = any(focus) then true else false end as proband 
  from gevt g;
end;
$$
language plpgsql;

create or replace function public.ascend_trimlite(focus uuid[])
returns table (id uuid, name text, pa uuid, ma uuid, gender char(1), is_proband bool) as
$$
declare
  delcount int := 1;
begin
  drop table if exists gevt;
  create temp table gevt as
  select * from ascend_all(focus);
  while delcount > 0 loop
    drop table if exists singleton;
    create temp table singleton as
    with ped as (select * from gevt),
    founder as (select p.id, p.gender from ped p where p.ma is null and p.pa is null),
    fmother as (select f.id, count(*) as kids from founder f join ped p on f.id = p.ma where f.gender = 'f' group by f.id having count(*) = 1),
    ffather as (select f.id, count(*) as kids from founder f join ped p on f.id = p.pa where f.gender = 'm' group by f.id having count(*) = 1),
    singleton as (select * from ped p where p.ma in (select fm.id from fmother fm where fm.kids = 1) and p.pa in (select ff.id from ffather ff where ff.kids = 1))
    select * from singleton;
    update gevt g set ma = null, pa = null where g.id in (select s.id from singleton s where s.id != any(focus));
    delete from gevt g where g.id in (select t.ma from singleton t union select s.pa from singleton s) and g.id != any(focus);
    get diagnostics delcount = ROW_COUNT;
    raise notice 'dropped % singleton parents', delcount;
  end loop;
  delete from gevt g where g.ma is null and g.pa is null and not exists(select 1 from gevt h where (g.id = h.ma or g.id = h.pa));
  get diagnostics delcount = ROW_COUNT;
  raise notice 'removed % duds', delcount;
  -- return query select g.id, g.pa, g.ma, g.gender, case when d.id is not null then true else false end as proband 
  -- from gevt g left join dna d on g.id = d.id;
  return query select g.id, g.name, g.pa, g.ma, g.gender, case when g.id = any(focus) then true else false end as proband 
  from gevt g;
end;
$$
language plpgsql;
