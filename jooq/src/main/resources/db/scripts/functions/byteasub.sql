create or replace function public.byteasub(bits bytea, fbyte int, lbyte int)
returns bytea
-- we expect the incoming firstbyte to be zero-based, db is one-based
as $$
select substring(bits,fbyte+1,lbyte);
$$
language sql;
