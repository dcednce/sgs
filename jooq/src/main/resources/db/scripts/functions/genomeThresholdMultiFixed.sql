CREATE OR REPLACE FUNCTION public.genome_threshold_multi_fixed(fixed_opset uuid, pool_opsets uuid[])
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
  optimalsetnew  uuid;
  fixedname     text;
  genomename    text;
  procid        uuid;
  groups        uuid[];
  tnames        text[];
  rc            int;
begin
  -- get the markerset used in making the fixed 
  select g.name into genomename
  from genome_markerset g
       join process_input i on g.id::text = o.input_ref
       join process_output o on i.process_id = o.process_id
  where i.output_ref::uuid = fixed_opset;
  -- create threshold entry and load optimal paired set
  select array_agg(t.name) into tnames from threshold t where t.id = any(pool_opsets);
  select name into fixedname from optimalset t where t.id = fixed_opset;
--
  select array_agg(g.id) into groups 
  from probandset_group g join threshold t on g.id = any(t.probandset_groups)
  where t.id = any (pool_opsets);
--
  optimalsetnew = uuid_generate_v4();
  raise notice '%: === STARTING multi-threshold-fixed (%) "%"',
        clock_timestamp(), optimalsetnew,  fixedname || ':' || array_to_string(tnames, '/');
  insert into optimalset (id, name, probandset_groups, suggestive, significant, gamma_shape, gamma_rate)
        values (optimalsetnew,
                (select fixedname || ':' || array_to_string(array_agg(t.name),'/') 
                        from optimalset t where t.id = any(pool_opsets)), 
                groups,
                0.0, 0.0, 0.0, 0.0);
  insert into optimalset_duosegment(id, threshold_id, optimalset_segment1_id, optimalset_segment2_id, fisher_pvalue, mu_t)
         select uuid_generate_v4(), optimalsetnew, tseg1, tseg2, f.fisher_combined, null
         from genome_fisher_fixed(fixed_opset, pool_opsets) as f;
  get diagnostics rc = row_count;
  select threshold_process(optimalsetnew, genomename, false) into procid;
  raise notice '%: === Added % duo segments for new multi-people optimalset id = %',
        clock_timestamp(), rc, optimalsetnew;
end;
$function$
