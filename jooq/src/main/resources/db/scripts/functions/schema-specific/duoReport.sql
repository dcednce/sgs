create or replace function duo_report(optima text)
returns table(segment_count bigint, ped_combo text)
as $$
begin
    select count(*) as segment_count, case when (o1.name=o.name) then o1.name||'-'||o2.name else o2.name||'-'||o1.name end as ped_combo
    from people o
    join optimalset t on t.name like o.name||'%not%'||o.name||'%'
    join optimalset_duosegment tds on tds.optimalset_id = t.id
    join optimalset_segment ts1 on ts1.id = tds.optimalset_segment1_id
    join optimalset_segment ts2 on ts2.id = tds.optimalset_segment2_id
    join segment s1 on s1.id = ts1.segment_id
    join segment s2 on s2.id = ts2.segment_id
    join probandset p1 on p1.id = s1.probandset_id
    join probandset p2 on p2.id = s2.probandset_id
    join people o1 on o1.id = p1.people_id
    join people o2 on o2.id = p2.people_id
    where o.name ~ optima
    group by ped_combo
    order by ped_combo;
end;
$$
language plpgsql;
