-- We want to get rid of a multiassess run
-- Written at a time where there was no chasing done
create or replace function public.rollback_ma(procid UUID)
returns void
as $$
declare 
  mksetid uuid;
  peepid uuid;
  rowcount int;
  deletables_found int;
  membercount int;
  setcount int;
  othertypes text;
begin
  select count(*) into rowcount from process where id = procid;
  if rowcount != 1 then
    raise exception 'Did not find exactly ONE process by id %. % actually', procid, rowcount;
  end if;
--
  select array_to_string(array_agg(distinct output_type), ' ,', 'nil') into othertypes
  from process_output 
  where process_id = procid and output_type != 'segmentset';
--
  if not othertypes is null then
    raise exception 'Cannot drop process with output type(s) %', othertypes;
  end if;
  delete from process_output where process_id = procid;  
  get diagnostics rowcount = ROW_COUNT;
  raise notice '%: % outputs dropped', clock_timestamp(), rowcount;
  delete from process_input where process_id = procid;
  get diagnostics rowcount = ROW_COUNT;
  raise notice '%: % inputs dropped', clock_timestamp(), rowcount;
  delete from process_arg where process_id = procid;
  get diagnostics rowcount = ROW_COUNT;
  raise notice '%: % args dropped', clock_timestamp(), rowcount;
  delete from process where id = procid;
--
  raise notice '%: Process % deleted', clock_timestamp(), procid;
end;
$$
language plpgsql;
