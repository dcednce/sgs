create or replace function sgscost(popname text)
returns money
language plpgsql
as $$
declare
  popsize int;
  pbcount int;
  cost money;
begin
  select count(*) into popsize 
  from people p join people_member m on p.id = m.people_id 
  where p.name = popname;
  select count(*) into pbcount
  from people p join people_member m on p.id = m.people_id
       join genotype g on m.person_id = g.person_id
       join markerset s on g.markerset_id = s.id
  where p.name = popname and s.chrom=22;
  select sgscost(pbcount, popsize) into cost;
  return cost;
end;
$$
;
  

create or replace function sgscost(probn int, popn int)
returns money
language sql
as $$
select (370 + power(2,(probn - 3.9)) + 6.47*popn)::money;
$$
;
