create or replace function public.segment_calls(segid uuid)
returns table (name text, firsti int, lasti int, calls text)
as
$$
begin
select 
    p.name, 
    s.firstmarker, 
    s.lastmarker, 
    regexp_replace(substr(g.calls,1+(2*s.firstmarker), 2*(s.lastmarker-s.firstmarker+1))::text, '(..)', ' \1','g') as calls
from
    segment s
    join probandset b on s.probandset_id = b.id
    join people l on b.people_id = l.id
    join people_member m on l.id = m.people_id
    join person p on m.person_id = p.id
    join genotype g on g.markerset_id = s.markerset_id and g.person_id = p.id
where s.id = segid;
end;
$$
language plpgsql;
;
