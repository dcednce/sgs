create or replace function public.wilson_ci(z float, iev bigint, tev bigint)
 returns float[]
 language plpgsql
 immutable strict
as $$
declare
  p float;
  q float;
  ex1 float;
  ex2 float;
  dex float;
  ucl float;
  lcl float;
  ci float[];
begin
  p := (iev*1.0)/tev;
  ex1 := ((iev) + (power(z,2)/2))/((tev) + power(z,2));
  ex2 := (z/((tev)+power(z,2))) * sqrt((tev)*((p*(1-p)) + (power(z,2)/(4*(tev)))));
  dex = ex1 - ex2;
  if (dex) < 0.0 then ci[1] = 0.0; else ci[1] = dex; end if;
  ci[2] := ex1 + ex2;
  return ci;
end;
$$
