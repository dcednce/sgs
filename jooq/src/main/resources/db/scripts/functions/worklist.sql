create or replace function worklist_fill(groupname text, maxpv float)
returns void
as $$
declare
  workitems int;
begin
  insert into chaseable
  select uuid_generate_v4() as id, s.id as segment_id , t.id as optimalset_id 
  from segment s
      join probandset p on s.probandset_id = p.id 
      join probandset_group_member m on p.id = m.member_id 
      join probandset_group g on m.group_id = g.id 
      join optimalset t on array[g.id] = t.probandset_groups
  where 
      g.name = groupname
      and (
        pv(s.events_less, s.events_equal, s.events_greater, 0) = 0
        or (
            pv(s.events_less, s.events_equal, s.events_greater, 0) <= maxpv
            and (
                ( pv_upper95(s.events_less, s.events_equal, s.events_greater, 0) > t.significant 
                  and pv_lower95(s.events_less, s.events_equal, s.events_greater, 0) < t.significant
                ) 
                or 
                ( pv_upper95(s.events_less, s.events_equal, s.events_greater, 0) > t.suggestive 
                  and pv_lower95(s.events_less, s.events_equal, s.events_greater, 0) < t.suggestive 
                )
            )
          )
        );
  get diagnostics workitems = ROW_COUNT;
  raise notice 'added % worklist items for %', workitems, groupname;
end;
$$ language plpgsql;
   
