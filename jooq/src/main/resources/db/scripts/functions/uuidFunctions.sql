create or replace function public.idset_lookup(aids text[])
returns uuid as
$$
declare
        setid uuid;
        tids uuid[];
begin
        select array_agg(p.id) into tids
        from seg.person as p
             join seg.alias as a on p.id = a.person_id
        where a.aka = any(aids);
--
        if array_length(tids,1) != array_length(aids,1)
        then
                raise exception 'failed to find all person ids';
        end if;
--
        select s.id into setid
        from seg.probandset s
        where s.probands = sortuuidvector(tids);
--
        return setid;
end;
$$
language plpgsql immutable;

create or replace function public.nameset_lookup(aids uuid[])
returns text[] as
$$
declare
        tids text[];
begin
        select array_agg(p.name) into tids
        from seg.person as p
        where p.id = any(aids);
--
        if array_length(tids,1) != array_length(aids,1)
        then
                raise exception 'failed to find name for all person ids';
        end if;
--
        return tids;
end;
$$
language plpgsql immutable;


create or replace function public.issorteduuids(uset uuid[])
 returns boolean
 language plpgsql
 immutable
as $function$
declare
   sset uuid[];
begin
   select array_agg( u order by u) from unnest(uset) as t(u) into sset;
   return sset = uset;
end;
$function$
