create or replace function public.remove_people(pname text)
returns void
as $$
declare 
  segmentsetid uuid;        
  peopleid uuid;
  pbsgroupid uuid;
  rowc int;
begin
  select id into peopleid from people  where name = pname;
  if peopleid is null then
    raise exception 'No such people: %', pname;
  else
    raise notice '% id is %', pname, peopleid;
  end if;
  -- select id into segmentsetid from segmentset s where exists (
  --        select 1 from people p join probandset b on p.id = b.people_id 
  --                 join segment t on b.id = t.probandset_id
  --                 join segmentset_member m on t.id = m.segment_id 
  --                 where m.segmentset_id = s.id)
--
  delete from segmentset_member m
         using segmentset ss, projectfile f
         where m.segmentset_id = ss.id and ss.pedfile_id = f.id and f.people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % segmentset members', rowc;
  delete from segmentset ss
         using projectfile f
         where ss.pedfile_id = f.id and f.people_id = peopleid;
--
  delete from projectfile where people_id = peopleid;      
  get diagnostics rowc = ROW_COUNT;
  raise notice 'delete % files for %', rowc, pname;
--  
  select g.id into pbsgroupid from probandset_group g join probandset s on g.proband_superset_id = s.id and s.people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  if pbsgroupid is null then
    raise notice 'Did not find a proband superset';
  end if;
--  
  create temp table peeps on commit drop as 
         select person_id as killid from people_member where people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'People % has % members', pname, rowc;
--
  delete from peeps s where exists 
  (select 1 from people_member m where m.person_id = s.killid and m.people_id != peopleid);
  get diagnostics rowc = ROW_COUNT;
  raise notice '% persons exits in at least one other people, not removed', rowc;
--  
  create temp table segid on commit drop as
  select distinct s.id from segment s join probandset b on s.probandset_id = b.id 
         where b.people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Dealing with % segments', rowc;
  create unique index on segid(id);
  delete from duo_segment d using segid s where d.segment1_id = s.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % duos 1', rowc;
  delete from duo_segment d using segid s where d.segment2_id = s.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % duos 2', rowc;
  delete from chaseable c using segid s where c.segment_id = s.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % chaseables', rowc;
  
  delete from segment t using segid i where t.id = i.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % segments', rowc;
--
  delete from optimalset t where pbsgroupid = any(t.probandset_groups);
  delete from probandset_group_member where group_id = pbsgroupid;
  delete from probandset_group where id = pbsgroupid;
  delete from probandset where people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % probandsets', rowc;
--
  delete from people_member where people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % people members', rowc;
--
  delete from alias where people_id = peopleid;
--
  delete from person p where exists (select 1 from peeps s where s.killid = p.id);
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % persons', rowc;
--
  delete from people where id = peopleid;
end;
$$ language plpgsql;
