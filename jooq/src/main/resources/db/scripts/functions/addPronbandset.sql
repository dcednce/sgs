create or replace function public.addprobandset(names text[], people uuid, groupname text)
returns void
as $$
declare
  pbsid uuid;
  pbsset uuid[];
  groupid uuid;
begin
  select id into groupid from probandset_group where name = groupname;
  if groupid is null then
    select uuid_generate_v4() into groupid;
    insert into probandset_group values(groupid, groupname, null, 'water boarding');
  end if;
  select uuid_generate_v4() into pbsid;
  select array_agg(id order by id) into pbsset from person
  where name = any (names);
--
  insert into probandset(id, name, probands, people_id)
  values(pbsid, array_to_string(names, ','), pbsset, people);
--
  insert into probandset_group_member values (groupid, pbsid);
end;
$$ language plpgsql;
