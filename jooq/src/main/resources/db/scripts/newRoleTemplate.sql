SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sgstemplate; Type: SCHEMA; Schema: -; Owner: postgres
--
begin;
create schema if not exists sgstemplate\p\g
create role sgstemplate with login encrypted password 'sgstemplate_notnull'\p\g
alter role sgstemplate set search_path=sgstemplate,base,public\p\g
grant connect on database PROJDB to sgstemplate\p\g
set search_path = sgstemplate, base, bulk, public\p\g
--
-- Allow this role to diddle with base, bulk and project tables
--
grant all on schema base, bulk, sgstemplate to sgstemplate\p\g
grant all on all tables in schema base, bulk, sgstemplate to sgstemplate\p\g
commit;
