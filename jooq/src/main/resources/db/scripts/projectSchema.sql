--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sgstemplate; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgstemplate;


ALTER SCHEMA sgstemplate OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: segment; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.segment (
    id uuid NOT NULL,
    chrom integer NOT NULL,
    markerset_id uuid NOT NULL,
    probandset_id uuid NOT NULL,
    startbase integer NOT NULL,
    endbase integer NOT NULL,
    firstmarker integer NOT NULL,
    lastmarker integer NOT NULL,
    events_less bigint DEFAULT 0 NOT NULL,
    events_equal bigint DEFAULT 0 NOT NULL,
    events_greater bigint DEFAULT 0 NOT NULL,
    threshold_events integer
)
WITH (fillfactor='75');


ALTER TABLE sgstemplate.segment OWNER TO postgres;

--
-- Name: pvr(sgstemplate.segment, double precision); Type: FUNCTION; Schema: sgstemplate; Owner: postgres
--

CREATE FUNCTION sgstemplate.pvr(seg sgstemplate.segment, plus double precision DEFAULT 0.0) RETURNS double precision
    LANGUAGE sql
    AS $$
  select ((1.0*seg.events_equal)+seg.events_greater+plus)/((1.0*seg.events_less)+seg.events_equal+seg.events_greater+plus)::float as result;
$$;


ALTER FUNCTION sgstemplate.pvr(seg sgstemplate.segment, plus double precision) OWNER TO postgres;

--
-- Name: chaseable; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.chaseable (
    id uuid,
    segment_id uuid,
    optimalset_id uuid,
    passes text,
    CONSTRAINT chaseable_passes_check CHECK ((passes = ANY (ARRAY['sug'::text, 'sig'::text])))
);


ALTER TABLE sgstemplate.chaseable OWNER TO postgres;

--
-- Name: duo_chaseable; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.duo_chaseable (
    id uuid NOT NULL,
    optimalset_id uuid NOT NULL,
    segment1_id uuid NOT NULL,
    segment2_id uuid NOT NULL,
    fisher_pvalue double precision NOT NULL,
    passes text,
    CONSTRAINT duo_chaseable_passes_check CHECK ((passes = ANY (ARRAY['sug'::text, 'sig'::text])))
);


ALTER TABLE sgstemplate.duo_chaseable OWNER TO postgres;

--
-- Name: genotype; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.genotype (
    id uuid NOT NULL,
    person_id uuid NOT NULL,
    markerset_id uuid NOT NULL,
    calls bytea NOT NULL,
    people_id uuid NOT NULL
);


ALTER TABLE sgstemplate.genotype OWNER TO postgres;

--
-- Name: optimalset; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.optimalset (
    id uuid NOT NULL,
    name text,
    probandset_groups uuid[],
    suggestive double precision,
    significant double precision,
    gamma_shape double precision,
    gamma_rate double precision
);


ALTER TABLE sgstemplate.optimalset OWNER TO postgres;

--
-- Name: optimalset_duosegment; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.optimalset_duosegment (
    id uuid NOT NULL,
    optimalset_id uuid NOT NULL,
    optimalset_segment1_id uuid NOT NULL,
    optimalset_segment2_id uuid NOT NULL,
    fisher_pvalue double precision NOT NULL,
    mu_t double precision
);


ALTER TABLE sgstemplate.optimalset_duosegment OWNER TO postgres;

--
-- Name: optimalset_segment; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.optimalset_segment (
    id uuid NOT NULL,
    optimalset_id uuid NOT NULL,
    segment_id uuid NOT NULL,
    pvalue double precision NOT NULL,
    mu_t double precision
);


ALTER TABLE sgstemplate.optimalset_segment OWNER TO postgres;

--
-- Name: probandset; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.probandset (
    id uuid NOT NULL,
    name text,
    probands uuid[] NOT NULL,
    meioses integer,
    min_kincoef double precision,
    max_kincoef double precision,
    people_id uuid NOT NULL,
    CONSTRAINT sortedset CHECK (public.issorteduuids(probands))
);


ALTER TABLE sgstemplate.probandset OWNER TO postgres;

--
-- Name: probandset_group; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.probandset_group (
    id uuid NOT NULL,
    name text NOT NULL,
    proband_superset_id uuid,
    purpose text
);


ALTER TABLE sgstemplate.probandset_group OWNER TO postgres;

--
-- Name: probandset_group_member; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.probandset_group_member (
    group_id uuid NOT NULL,
    member_id uuid NOT NULL
);


ALTER TABLE sgstemplate.probandset_group_member OWNER TO postgres;

--
-- Name: process; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.process (
    id uuid NOT NULL,
    who text NOT NULL,
    what text NOT NULL,
    pit timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    runhost text
);


ALTER TABLE sgstemplate.process OWNER TO postgres;

--
-- Name: process_arg; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.process_arg (
    id uuid NOT NULL,
    process_id uuid NOT NULL,
    argname text NOT NULL,
    argvalue_int bigint,
    argvalue_float double precision,
    argvalue_text text
);


ALTER TABLE sgstemplate.process_arg OWNER TO postgres;

--
-- Name: process_input; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.process_input (
    id uuid NOT NULL,
    process_id uuid NOT NULL,
    input_type text NOT NULL,
    input_ref text NOT NULL
);


ALTER TABLE sgstemplate.process_input OWNER TO postgres;

--
-- Name: process_output; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.process_output (
    id uuid NOT NULL,
    process_id uuid NOT NULL,
    output_type text NOT NULL,
    output_ref uuid NOT NULL
);


ALTER TABLE sgstemplate.process_output OWNER TO postgres;

--
-- Name: projectfile; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.projectfile (
    id uuid NOT NULL,
    name text,
    filetype text NOT NULL,
    uri text NOT NULL,
    people_id uuid,
    CONSTRAINT ftypes CHECK ((filetype = ANY (ARRAY['ped'::text, 'par'::text, 'leg'::text, 'ld'::text, 'pval'::text]))),
    CONSTRAINT pedpeople CHECK ((NOT ((people_id IS NOT NULL) AND (filetype <> 'ped'::text))))
);


ALTER TABLE sgstemplate.projectfile OWNER TO postgres;

--
-- Name: segmentset; Type: TABLE; Schema: sgstemplate; Owner: postgres
--

CREATE TABLE sgstemplate.segmentset (
    id uuid NOT NULL,
    name text,
    markerset_id uuid NOT NULL,
    people_id uuid NOT NULL
);


ALTER TABLE sgstemplate.segmentset OWNER TO postgres;

--
-- Name: duo_chaseable duo_chaseable_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.duo_chaseable
    ADD CONSTRAINT duo_chaseable_pkey PRIMARY KEY (id);


--
-- Name: genotype genotype_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.genotype
    ADD CONSTRAINT genotype_pkey PRIMARY KEY (person_id, markerset_id, people_id);


--
-- Name: optimalset_duosegment optimalset_duo_segment_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_duosegment
    ADD CONSTRAINT optimalset_duo_segment_pkey PRIMARY KEY (id);


--
-- Name: optimalset optimalset_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset
    ADD CONSTRAINT optimalset_pkey PRIMARY KEY (id);


--
-- Name: optimalset_segment optimalset_segment_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_segment
    ADD CONSTRAINT optimalset_segment_pkey PRIMARY KEY (id);


--
-- Name: probandset_group probandset_group_name_key; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset_group
    ADD CONSTRAINT probandset_group_name_key UNIQUE (name);


--
-- Name: probandset_group probandset_group_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset_group
    ADD CONSTRAINT probandset_group_pkey PRIMARY KEY (id);


--
-- Name: probandset probandset_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset
    ADD CONSTRAINT probandset_pkey PRIMARY KEY (id);


--
-- Name: process_arg process_arg_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process_arg
    ADD CONSTRAINT process_arg_pkey PRIMARY KEY (id);


--
-- Name: process_input process_input_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process_input
    ADD CONSTRAINT process_input_pkey PRIMARY KEY (id);


--
-- Name: process_output process_output_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process_output
    ADD CONSTRAINT process_output_pkey PRIMARY KEY (id);


--
-- Name: process process_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process
    ADD CONSTRAINT process_pkey PRIMARY KEY (id);


--
-- Name: projectfile projectfile_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.projectfile
    ADD CONSTRAINT projectfile_pkey PRIMARY KEY (id);


--
-- Name: probandset_group_member psgm_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset_group_member
    ADD CONSTRAINT psgm_pkey PRIMARY KEY (group_id, member_id);


--
-- Name: segment segment_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segment
    ADD CONSTRAINT segment_pkey PRIMARY KEY (id);


--
-- Name: segmentset segmentset_name_key; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segmentset
    ADD CONSTRAINT segmentset_name_key UNIQUE (name);


--
-- Name: segmentset segmentset_pkey; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segmentset
    ADD CONSTRAINT segmentset_pkey PRIMARY KEY (id);


--
-- Name: projectfile u_uri; Type: CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.projectfile
    ADD CONSTRAINT u_uri UNIQUE (uri);


--
-- Name: arg_proc; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX arg_proc ON sgstemplate.process_arg USING btree (process_id);


--
-- Name: chaseablesegref; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX chaseablesegref ON sgstemplate.chaseable USING btree (segment_id);


--
-- Name: fpvx; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX fpvx ON sgstemplate.segment USING btree (public.pv(events_less, events_equal, events_greater, 0));


--
-- Name: gt_person_marker; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX gt_person_marker ON sgstemplate.genotype USING btree (person_id, markerset_id);


--
-- Name: input_proc; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX input_proc ON sgstemplate.process_input USING btree (process_id);


--
-- Name: optimalset_segment_optimalset_id_segment_id_idx; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE UNIQUE INDEX optimalset_segment_optimalset_id_segment_id_idx ON sgstemplate.optimalset_segment USING btree (optimalset_id, segment_id);


--
-- Name: output_proc; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX output_proc ON sgstemplate.process_output USING btree (process_id);


--
-- Name: probands_gin; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX probands_gin ON sgstemplate.probandset USING gin (probands);


--
-- Name: probandset_group_member_member_id_idx; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX probandset_group_member_member_id_idx ON sgstemplate.probandset_group_member USING btree (member_id);


--
-- Name: probandsetunique; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE UNIQUE INDEX probandsetunique ON sgstemplate.probandset USING btree (people_id, probands);


--
-- Name: segment_markerset_id_idx; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX segment_markerset_id_idx ON sgstemplate.segment USING btree (markerset_id);


--
-- Name: segment_probandset_id_idx; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE INDEX segment_probandset_id_idx ON sgstemplate.segment USING btree (probandset_id);


--
-- Name: useg; Type: INDEX; Schema: sgstemplate; Owner: postgres
--

CREATE UNIQUE INDEX useg ON sgstemplate.segment USING btree (probandset_id, markerset_id, startbase, endbase) WITH (fillfactor='95');


--
-- Name: chaseable chaseable_optimalset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.chaseable
    ADD CONSTRAINT chaseable_optimalset_id_fkey FOREIGN KEY (optimalset_id) REFERENCES sgstemplate.optimalset(id);


--
-- Name: chaseable chaseable_segment_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.chaseable
    ADD CONSTRAINT chaseable_segment_id_fkey FOREIGN KEY (segment_id) REFERENCES sgstemplate.segment(id);


--
-- Name: duo_chaseable duo_chaseable_optimalset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.duo_chaseable
    ADD CONSTRAINT duo_chaseable_optimalset_id_fkey FOREIGN KEY (optimalset_id) REFERENCES sgstemplate.optimalset(id);


--
-- Name: duo_chaseable duo_chaseable_segment1_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.duo_chaseable
    ADD CONSTRAINT duo_chaseable_segment1_id_fkey FOREIGN KEY (segment1_id) REFERENCES sgstemplate.segment(id);


--
-- Name: duo_chaseable duo_chaseable_segment2_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.duo_chaseable
    ADD CONSTRAINT duo_chaseable_segment2_id_fkey FOREIGN KEY (segment2_id) REFERENCES sgstemplate.segment(id);


--
-- Name: genotype genotype_markerset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.genotype
    ADD CONSTRAINT genotype_markerset_id_fkey FOREIGN KEY (markerset_id) REFERENCES base.markerset(id);


--
-- Name: genotype genotype_people_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.genotype
    ADD CONSTRAINT genotype_people_id_fkey FOREIGN KEY (people_id) REFERENCES base.people(id);


--
-- Name: genotype genotype_person_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.genotype
    ADD CONSTRAINT genotype_person_id_fkey FOREIGN KEY (person_id) REFERENCES base.person(id);


--
-- Name: optimalset_duosegment optimalset_duo_segment_optimalset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_duosegment
    ADD CONSTRAINT optimalset_duo_segment_optimalset_id_fkey FOREIGN KEY (optimalset_id) REFERENCES sgstemplate.optimalset(id);


--
-- Name: optimalset_duosegment optimalset_duo_segment_optimalset_segment1_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_duosegment
    ADD CONSTRAINT optimalset_duo_segment_optimalset_segment1_fkey FOREIGN KEY (optimalset_segment1_id) REFERENCES sgstemplate.optimalset_segment(id);


--
-- Name: optimalset_duosegment optimalset_duo_segment_optimalset_segment2_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_duosegment
    ADD CONSTRAINT optimalset_duo_segment_optimalset_segment2_fkey FOREIGN KEY (optimalset_segment2_id) REFERENCES sgstemplate.optimalset_segment(id);


--
-- Name: optimalset_segment optimalset_segment_optimalset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_segment
    ADD CONSTRAINT optimalset_segment_optimalset_id_fkey FOREIGN KEY (optimalset_id) REFERENCES sgstemplate.optimalset(id);


--
-- Name: optimalset_segment optimalset_segment_optimalset_segment_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.optimalset_segment
    ADD CONSTRAINT optimalset_segment_optimalset_segment_fkey FOREIGN KEY (segment_id) REFERENCES sgstemplate.segment(id);


--
-- Name: probandset_group_member probandset_group_member_group_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset_group_member
    ADD CONSTRAINT probandset_group_member_group_id_fkey FOREIGN KEY (group_id) REFERENCES sgstemplate.probandset_group(id);


--
-- Name: probandset_group_member probandset_group_member_member_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset_group_member
    ADD CONSTRAINT probandset_group_member_member_id_fkey FOREIGN KEY (member_id) REFERENCES sgstemplate.probandset(id);


--
-- Name: probandset probandset_people_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.probandset
    ADD CONSTRAINT probandset_people_id_fkey FOREIGN KEY (people_id) REFERENCES base.people(id);


--
-- Name: process_arg process_arg_process_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process_arg
    ADD CONSTRAINT process_arg_process_id_fkey FOREIGN KEY (process_id) REFERENCES sgstemplate.process(id);


--
-- Name: process_input process_input_process_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process_input
    ADD CONSTRAINT process_input_process_id_fkey FOREIGN KEY (process_id) REFERENCES sgstemplate.process(id);


--
-- Name: process_output process_output_process_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.process_output
    ADD CONSTRAINT process_output_process_id_fkey FOREIGN KEY (process_id) REFERENCES sgstemplate.process(id);


--
-- Name: projectfile projectfile_people_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.projectfile
    ADD CONSTRAINT projectfile_people_id_fkey FOREIGN KEY (people_id) REFERENCES base.people(id);


--
-- Name: segment segment_markerset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segment
    ADD CONSTRAINT segment_markerset_id_fkey FOREIGN KEY (markerset_id) REFERENCES base.markerset(id);


--
-- Name: segment segment_probandset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segment
    ADD CONSTRAINT segment_probandset_id_fkey FOREIGN KEY (probandset_id) REFERENCES sgstemplate.probandset(id);


--
-- Name: segmentset segmentset_markerset_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segmentset
    ADD CONSTRAINT segmentset_markerset_id_fkey FOREIGN KEY (markerset_id) REFERENCES base.markerset(id);


--
-- Name: segmentset segmentset_people_id_fkey; Type: FK CONSTRAINT; Schema: sgstemplate; Owner: postgres
--

ALTER TABLE ONLY sgstemplate.segmentset
    ADD CONSTRAINT segmentset_people_id_fkey FOREIGN KEY (people_id) REFERENCES base.people(id);


--
-- Name: SCHEMA sgstemplate; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA sgstemplate TO sgstemplate;


--
-- Name: TABLE segment; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.segment TO sgstemplate;


--
-- Name: TABLE chaseable; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.chaseable TO sgstemplate;


--
-- Name: TABLE duo_chaseable; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.duo_chaseable TO sgstemplate;


--
-- Name: TABLE genotype; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.genotype TO sgstemplate;


--
-- Name: TABLE optimalset; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.optimalset TO sgstemplate;


--
-- Name: TABLE optimalset_duosegment; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.optimalset_duosegment TO sgstemplate;


--
-- Name: TABLE optimalset_segment; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.optimalset_segment TO sgstemplate;


--
-- Name: TABLE probandset; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.probandset TO sgstemplate;


--
-- Name: TABLE probandset_group; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.probandset_group TO sgstemplate;


--
-- Name: TABLE probandset_group_member; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.probandset_group_member TO sgstemplate;


--
-- Name: TABLE process_arg; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.process_arg TO sgstemplate;


--
-- Name: TABLE process_input; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.process_input TO sgstemplate;


--
-- Name: TABLE process_output; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.process_output TO sgstemplate;


--
-- Name: TABLE projectfile; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.projectfile TO sgstemplate;


--
-- Name: TABLE segmentset; Type: ACL; Schema: sgstemplate; Owner: postgres
--

GRANT ALL ON TABLE sgstemplate.segmentset TO sgstemplate;


--
-- PostgreSQL database dump complete
--

