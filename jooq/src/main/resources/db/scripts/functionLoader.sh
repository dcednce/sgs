#!/bin/bash -e
function usage {
    printf "usage: %s sqlfile hostname PI [ PI ... ]\n" $1
}
if [[ ! -n $PG_SUSER ]]
then
    printf "You must set PG_SUSER to postgres admin account name\n"
    exit 2
fi
sqlfile=$1
if [[ "$sqlfile"x == "x" ]]; then echo no sqlfile; usage $0; exit; fi
shift
hostnm=$1
if [[ "$hostnm"x == "x" ]]; then echo no host name; exit; fi
shift

if [[ "$@"x == "x" ]]; then echo no PIs; exit; fi

for pi in $@;
do
    printf "loading %s with %s\n" $pi $sqlfile
    psql --dbname=$pi --user=postgres --host=$hostnm --file=$sqlfile
done
