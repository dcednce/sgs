#!/bin/bash
if [ $# -ne 2 ]; then printf "usage: %s <hostname> <dbname>\n" $0; exit 2; fi

echo dumping db $2 from host $1
pg_dump --host $1 --user postgres  --schema-only --schema=base --schema=public --schema=bulk --file baselineSchema.sql $2
pg_dump --host $1 --user postgres  --schema-only --schema=sgstemplate --file projectSchema.sql $2

