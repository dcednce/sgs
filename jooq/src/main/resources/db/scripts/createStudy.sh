#!/bin/bash -e
function usage {
    printf "usage: %s --dbhost|-h <postgresHost> --study|-s <projectName> --investigator|-i <investigator> --sqldir <dir> [--dba|-a <dba role>]\n" $1
    printf "or\n\t%s --help \n" $1
    printf "This is used within createInvestigator\n"
    exit 1
}
prog=$0
unset dba

while [[ ${#@} -gt 0 ]]
do
    case $1 in
        -h|--dbh*)
            shift
            dbhost=$1;
            shift;;
        -i|--investigator|--inv*)
            shift
            sgsdb=$1;
            shift;;
        -s|--study|--st*)
            shift
            projname=$1;
            shift;;
        -q|--sqldir|--sch*)
            shift
            sqldir=$1
            shift;;
	-a|--dba)
	    shift;
	    dba=$1;
	    shift;;
	--help)
	    usage $prog;;
	*)
	    echo Unknow arg $1
	    usage $prog;;
	esac
done
if [[ "${dbhost}x" == "x" ]]
then
    echo need to set --dbhost
    usage $prog
fi

if [[ "${sgsdb}x" == "x" ]]
then
    echo need to set --sgsdb
    usage $prog
fi

if [[ "${projname}x" == "x" ]]
then
    echo need to set --name
    usage $prog
fi

if [[ "${sqldir}x" == "x" ]]
then
    if [[ -e ./projectRoleDDL.sql ]]
    then
        sqldir=`pwd`
        echo "===" using current dir for role definition
    else
        echo need to set --sqldir
        usage $prog
    fi
fi

PRJ=$projname
DBA=$dba
roleFile=/tmp/role-$$_$PRJ.sql

sed -e s/sgstemplate/$PRJ/g -e s/PROJDB/$sgsdb/g ${sqldir}/projectSchema.sql >> $roleFile
sed -e s/sgstemplate/$PRJ/g -e s/PROJDB/$sgsdb/g ${sqldir}/newRoleTemplate.sql >> $roleFile
printf "run the %s specific sql from %s\n" $projname $roleFile
psql --user=${DBA:=postgres} --host=$dbhost --dbname $sgsdb --file $roleFile

funcfile=/tmp/schemafuncs.$$
printf "\c %s\n" $sgsdb > $funcfile
printf "set check_function_bodies = false;\n" >> $funcfile
printf "set search_path = %s,base,public;\n" $PRJ >> $funcfile
printf "set role %s;\n" $PRJ >> $funcfile
# for t in $(find $sqldir/../tabledata -type f)
# do
#     tabname=$(basename $t)
#     printf "\\copy base.%s from %s CSV;\n" $tabname $t
# done

mkdir -p /tmp/$PRJ/functions

for f in $(find ${sqldir}/functions/schema-specific -name \*.sql); 
do
    funcname=$(basename $f)
    sed s/__STUDYNAME__/$PRJ/ $f > /tmp/$PRJ/functions/$funcname
    echo adding function file $funcname 
    printf "\i %s\n" /tmp/$PRJ/functions/$funcname >> $funcfile
done
psql --host=$dbhost --user=${DBA} --file $funcfile
if [[ -z $DEBUG ]]; then rm $funcfile; fi

##  $sgsdb $projname
which aws > /dev/null 2>&1
if [[ $? ]]; 
then 
    echo NOT AN AWS INSTALLATION \(No queue needed\); 
else 
    #progDir may be set in createInvestigator
    ${progDir:-$sqldir}/addAWSQueue.sh  $sgsdb $projname || echo Did not make queue for $projname; 
fi

printf "======================================\n"
printf "======================================\n"
printf "==                                  ==\n"
printf "==   Remember to add                ==\n"
printf "==   %-30s ==\n"  $PRJ
printf "==   to the list of known bouncers  ==\n"
printf "==                                  ==\n"
printf "======================================\n"
printf "======================================\n"
