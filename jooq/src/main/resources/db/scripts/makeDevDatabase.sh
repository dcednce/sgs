#!/usr/bin/env -S bash -e
function usage {
    if [[ ! "$1"x == "x" ]]; then echo bad arg $1; fi
    printf "%s: [--server|-s <hostname>] [--base|-b <name>] [--help|-h] [--uuid] [--port nnnn]\n" $0
    printf "Build the database against which jOOQ will generate java code and flyway migrations are applied\n"
    printf "Use '--uuid' if the template1 database does not have UUID package installed. (Or fix template1.)\n"
    printf "The default dev database name is 'sgsdev'. --base othername to override"
    printf "Expect a prompt for the db-admin password (or apply appropriate .pgpass magic)\n"
    exit 
}
script=$(realpath $0)
servername=localhost
port=5432
user=postgres
addUUID=
devdb=sgsdev
scriptDir=$(dirname $script)
while [[ $# > 0 ]];
do
    case $1 in
	--se*|-s)
	    shift;
	    servername=$1
	    shift;;
	--base)
	    shift;
	    devdb=$1;
	    shift;;
	--port)
	    shift;
	    port=$1;
	    shift;;
	--uuid)
	    shift;
	    addUUID=1;;
	--he|-h)
	    usage;;
	*)
	    usage $1;;
    esac
done

if [[ $addUUID == 1 ]]
then
    psql --host $servername --port $port --user postgres  <<EOF
\c template1
create extension "uuid-ossp";
EOF
fi

createdb --username=postgres --host=$servername --port=$port $devdb "SGS code development"

psql --host $servername --port $port --user postgres --dbname $devdb > $0.log <<EOF
\i $scriptDir/baselineSchema.sql
\i $scriptDir/projectSchema.sql
EOF

gitdir=$(echo $scriptDir | sed s:/jooq.\*::)
cd $gitdir
gradle :jooq:flywayBaseline >> $0.log

printf "Successfully create the developer database (%s).\n" $devdb
