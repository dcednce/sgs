#!/usr/bin/bash -e

usage() {
    msg="$1"
    if [[ -n $msg ]]; then printf "!!! %s\n" "$msg !!!"; fi
    printf "%s: investigator project\n" $0
    printf "%7s investigator will correspond to database name at AWS\n" "where:"
    printf "%7s project will correspond to database schema\n" "and"
    printf "\nThe effective user account will need the appropriate configuration for running AWS CLI\n"
    exit 2
}
if [ $# -ne 2 ]; then usage "gimme two args, man"; exit 2; fi

INV=$1
PRJ=$2
queueName=ppr-${INV}_${PRJ}-chase
(aws sqs list-queues | grep -l $queueName) && usage "$queueName already known"
aws sqs create-queue --queue-name $queueName && printf "success: created queue %s\n" $queueName
