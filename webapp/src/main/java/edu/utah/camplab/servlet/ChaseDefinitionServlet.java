package edu.utah.camplab.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.utah.camplab.jx.AbstractPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.juli.logging.Log;

@WebServlet(
        name = "ChaseDefinition",
        urlPatterns = {"/chasedef"}
)
public class ChaseDefinitionServlet extends HttpServlet {
    public final Log logger = new LoggerDelegate(getClass().getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query= req.getQueryString();
        String[] qparts = query.split("=");
        UUID segId = UUID.fromString(qparts[1]);

        ServletOutputStream out = resp.getOutputStream();

        out.write("There is a saver".getBytes());
        out.write(("\nWould be writing to " + req.getHeader("dbname")).getBytes());
        boolean found = false;
        for (Enumeration<String> pnames = getInitParameterNames(); pnames.hasMoreElements();) {
            found = true;
            out.write(("\n"+ pnames).getBytes());
        }
        if ( ! found ) {
            out.write("\nNo params found".getBytes());
        }
        out.write(("\n" + getClass().getName()).getBytes());
        out.flush();
        out.close();
    }
}
