package edu.utah.camplab.servlet;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import edu.utah.camplab.jx.AbstractPayload;

import org.jooq.*;
import org.jooq.conf.ParamType;
import org.jooq.exception.DataAccessException;
import org.jooq.exception.DataTypeException;
import org.jooq.impl.DSL;
import org.apache.juli.logging.Log;

@WebServlet(
  name = "PayloadSave",
  urlPatterns = {"/payloadsave"}
)

public class PayloadSaveServlet extends AbstractSGSServlet {
  public final Log logger = new LoggerDelegate(getClass().getName());

  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    //HashMap<String, ArrayList<String>> pmap = getParameterMap(req);
    //logger.error("payload save called");
    try (Connection copyConn = getDbConn(req, resp)) {
	ObjectMapper jsonMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
	jsonMapper.setSerializationInclusion(Include.NON_NULL);

        AbstractPayload payload = jsonMapper.readValue(req.getInputStream(), AbstractPayload.class);
        log("received payload");
        String redoUrl = String.format("jdbc:postgresql://%s:%d/%s", getDbHost(), getDbPort(), getDbName(req));
        payload.setConnection(copyConn);
        payload.setDbUrl(redoUrl);
        payload.write();
        log("finished db write");
        resp.setContentType("plain/text");
        resp.setStatus(200);
        resp.getOutputStream().write("SGS_OK".getBytes());
        resp.getOutputStream().flush();
        resp.getOutputStream().close();
      }
    catch (com.fasterxml.jackson.databind.exc.MismatchedInputException mie) {
      log("transform failed: " + mie.getMessage());
      resp.setContentType("plain/text");
      resp.setStatus(461);
      sendBadNews(resp, mie, "json formatting issue, perhaps");
    }
    catch (IOException | SQLException ioe) {
      resp.setStatus(460);
      sendBadNews(resp, ioe, "database trouble, likely");
    }
  }

  private void sendBadNews(HttpServletResponse response, Exception exp, String blurb) {
    try {
      String emsg = "PAYLOAD::%s\n%s\n".format(exp.getMessage(), blurb);
      response.getOutputStream().write(emsg.getBytes());
      response.getOutputStream().flush();
      response.getOutputStream().close();
    }
    catch (IOException ioe) {
      logger.error("lost payload: {}", ioe);
    }
  }
}
