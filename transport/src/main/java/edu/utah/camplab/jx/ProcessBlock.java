package edu.utah.camplab.jx;

import edu.utah.camplab.db.generated.default_schema.tables.pojos.Process;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessArg;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessInput;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessOutput;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ProcessBlock {
    private Process             process      = null;
    private List<ProcessInput>  inputList    = null;
    private List<ProcessOutput> outputList   = null;
    private List<ProcessArg>    argList      = null;
    Map<String, String>         environment  = null;

    public ProcessBlock() { }

    public void initProcess(String procType) {
        process = new Process();
        process.setWho(System.getProperty("user.name", "unknown"));
        process.setWhat(procType);
        process.setPit(LocalDateTime.now());
        try {
            process.setRunhost(InetAddress.getLocalHost().getHostName());
        }
        catch (UnknownHostException uhe) {
            process.setRunhost("unknowable");
        }
    }

    public void setProcess(Process p) {
        process = p;
    }

    public Process getProcess() {
        return process;
    }

    public List<ProcessInput> getInputList() {
        if (inputList == null) {
            inputList = new ArrayList<ProcessInput>();
        }
        return inputList;
    }

    public void addInput(String piType, String piValue) {
        ProcessInput pi = new ProcessInput();
        pi.setInputType(piType);
        pi.setInputRef(piValue);
        getInputList().add(pi);
    }
    
    public void addInputs(List<ProcessInput> ilist) {
        getInputList().addAll(ilist);
    }
        
    public List<ProcessOutput> getOutputList() {
        if (outputList == null) {
            outputList = new ArrayList<ProcessOutput>();
        }
        return outputList;
    }

    public void addOutput(String poType, String poValue) {
        ProcessOutput po = new ProcessOutput();
        po.setOutputType(poType);
        po.setOutputRef(UUID.fromString(poValue));
        
        getOutputList().add(po);
    }

    public void addOutputs(List<ProcessOutput> olist) {
        getOutputList().addAll(olist);
    }
        
    public void addOutput(ProcessOutput out) {
        getOutputList().add(out);
    }

    public List<ProcessArg>getArgList() {
        if (argList == null) {
            argList = new ArrayList<ProcessArg>();
        }
        return argList;
    }

    public void addTextArg(String tag, String value) {
        ProcessArg pa = new ProcessArg();
        pa.setArgname(tag);
        pa.setArgvalueText(value);
	addArg(pa);
    }
        
    public void addIntArg(String tag, String value) {
        ProcessArg pa = new ProcessArg();
        pa.setArgname(tag);
        pa.setArgvalueInt(Long.parseLong(value));
	addArg(pa);
    }
        
    public void addFloatArg(String tag, String value) {
        ProcessArg pa = new ProcessArg();
        pa.setArgname(tag);
        pa.setArgvalueFloat(Double.parseDouble(value));
	addArg(pa);
    }
    
    public void addVoidArg(String tag) {
	addTextArg(tag, "true");
    }
        
    public void addArgs(List<ProcessArg> alist) {
        getArgList().addAll(alist);
    }
        
    public void addArg(ProcessArg arg) {
        getArgList().add(arg);
    }

    public String getArgValue(String argname) {
        //tedious; maybe coalesce? A map at least?
        for (ProcessArg pa : getArgList()) {
            if (pa.getArgname().equals(argname)) {
                if (pa.getArgvalueText() != null) 
                    return pa.getArgvalueText();
                else if (pa.getArgvalueInt() != null)
                    return String.valueOf(pa.getArgvalueInt());
                else if (pa.getArgvalueFloat() != null)
                    return String.valueOf(pa.getArgvalueFloat());
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        StringBuffer sbuf = new StringBuffer("Process Block:");
        sbuf.append("\nInput block:");
        for (ProcessInput i : getInputList()) {
            sbuf.append ("\n\t" + i.toString());
        }
        sbuf.append("\nOutput block:");
        for (ProcessOutput o : getOutputList()) {
            sbuf.append ("\n\t" + o.toString());
        }
        
        sbuf.append("\nArguments block:");
        for (ProcessArg a : getArgList()) {
            sbuf.append ("\n\t" + a.toString());
        }
        return sbuf.toString();
    }
}
