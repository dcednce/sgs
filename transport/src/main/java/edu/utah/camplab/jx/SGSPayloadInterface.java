package edu.utah.camplab.jx;

import java.io.OutputStream;
import java.io.Writer;
import java.util.UUID;

public interface SGSPayloadInterface {
    
    public void setProjectName(String projectName);
    public String getProjectName();

    public void setClientUsername(String user);
    public String getClientUsername();

    public void setDbName(String s);
    public String getDbName();

    public UUID getRunTag();
    public void setRunTag(UUID t);

    public void setDbUrl(String s);
    public String getDbUrl();
    
    public void asJson(Writer w);
    public void asJson(OutputStream s);

    public String getPayloadType();
    
}
