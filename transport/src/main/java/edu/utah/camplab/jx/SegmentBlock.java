package edu.utah.camplab.jx;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segmentset;

import java.util.*;

public class SegmentBlock {
  //TODO: maybe make a local struct for this pair
  //IFF this is part of a multi-assess
  private Map<String, Segmentset> segmentsetMap = null;  //keyed by idset.csv
  private Map<String, List<Segment>> segmentMap = null;  //keyed by idset.csv

  public SegmentBlock() { }

  public Map<String, Segmentset> getSegmentsetMap() {
    if (segmentsetMap == null) {
      segmentsetMap = new HashMap<String, Segmentset>();
    }
    return segmentsetMap;
  }
    
  public void addSegmentset(String key, String name) {
    Segmentset segmentset = new Segmentset();
    segmentset.setName(name);
    getSegmentsetMap().put(key, segmentset);
  }

  public Segmentset getSegmentset(String key) {
    // could be null
    return segmentsetMap.get(key);
  }
        
  public void addSegment(UUID segId, String idlist, int chrm, int startb, int endb, int firstm, int lastm, long lt, long eq, long gt, int thev) {
    Segment seg = new Segment(segId,
			      chrm,
			      (UUID) null,
			      (UUID) null,
			      startb,
			      endb,
			      firstm,
			      lastm,
			      lt,
			      eq,
			      gt,
			      thev);
    getSegmentList(idlist).add(seg);  // slightly dangerous
  }

  public void addSegment(String idlist, int chrm, int startb, int endb, int firstm, int lastm, long lt, long eq, long gt, int thev) {
    addSegment((UUID)null, idlist, chrm, startb, endb, firstm, lastm, lt, eq, gt, thev);
  }

  public Map<String, List<Segment>> getSegmentMap() {
    if (segmentMap == null) {
      segmentMap = new HashMap<String,List<Segment>>();
    }
    return segmentMap;
  }
    
  public List<Segment> getSegmentList(String ids) {
    List<Segment> segs =  getSegmentMap().get(ids);
    if ( segs == null) {
      segs = new ArrayList<Segment>();
      segmentMap.put(ids, segs);
    }
    return segs;
  }

  @JsonIgnore
  public int getChromosome() {
    Segment firstSegment = segmentMap.entrySet().iterator().next().getValue().iterator().next();
    return firstSegment.getChrom();
  }

  @JsonIgnore
  public UUID getFirstSegmentId() {
    Segment firstSegment = segmentMap.entrySet().iterator().next().getValue().iterator().next();
    return firstSegment.getId();
  }

  @Override
  public String toString() {
    StringBuffer sbuf = new StringBuffer("Segment Block:\n");
    for (Map.Entry<String, Segmentset> me : segmentsetMap.entrySet()) {
      sbuf.append(String.format("\t segmentset:\n\t\tname: %s\n", me.getValue().getName()));
      sbuf.append(String.format("\t\t probands: %s\n", me.getKey()));
      sbuf.append("\t segment list:\n");
      // write each segment
      for (Segment seg : segmentMap.get(me.getKey())) {
	sbuf.append(seg.toString()); //TODO.
      }
    }
    return sbuf.toString();
  }
}
