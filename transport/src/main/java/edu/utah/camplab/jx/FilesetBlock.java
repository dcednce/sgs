package edu.utah.camplab.jx;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class FilesetBlock {

    private Set<File> filesetIn;
    private Set<File> filesetOut;
    
    public FilesetBlock() {}
    
    public Set<File> getFilesetIn() {
        if (filesetIn == null) {
            filesetIn = new HashSet<File>();
        }
        return filesetIn;
    }
    
    public void setFilesetIn(Set<File> files) {
        getFilesetIn().addAll(files);
    }

    public void addFileIn(File f) {
        getFilesetIn().add(f);
    }
    
    public Set<File> getFilesetOut() {
        if (filesetOut == null) {
            filesetOut = new HashSet<File>();
        }
        return filesetOut;
    }

    public void setFilesetOut(Set<File> files) {
        getFilesetOut().addAll(files);
    }

    public void addFileOut(File f) {
        getFilesetOut().add(f);
    }

    public String toString() {
        StringBuffer sbuf = new StringBuffer("Fileset Block:\n\tInput:\n");
        try {
            for (File f : getFilesetIn()) {
                sbuf.append(String.format("\t\t%s\n", f.getCanonicalPath()));
            }
            sbuf.append("\tOutput:\n");
            for (File f : getFilesetOut()) {
                sbuf.append(String.format("\t\t%s\n", f.getCanonicalPath()));
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return sbuf.toString();
    }
}
