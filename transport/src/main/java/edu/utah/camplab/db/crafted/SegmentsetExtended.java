package edu.utah.camplab.db.crafted;

import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentsetRecord;
import org.jooq.DSLContext;
import org.jooq.exception.TooManyRowsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static edu.utah.camplab.db.generated.default_schema.Tables.SEGMENT;

public class SegmentsetExtended extends SegmentsetRecord {
    private static final long serialVersionUID = 4449594411517441553L;
    
    public static final Logger logger = LoggerFactory.getLogger(SegmentsetExtended.class);
    
    private DSLContext          transaction   = null;
    private List<Segment>       segmentPojos  = new ArrayList<>();
    private ProcessExtended     procEx        = null;
    private ProbandsetRecord    probandsetRec = null;

    public SegmentsetExtended() { }
    
    public SegmentsetExtended(DSLContext ctx,
                              String name,
                              List<Segment> segList,
                              String project,
                              ProbandsetRecord pbsRec,
                              ProcessExtended px) {
        setId(UUID.randomUUID());
        setName(name);
        transaction = ctx;
        procEx = px;
        setPeopleId(procEx.getPeopleRecord().getId());
        setMarkersetId(procEx.getMarkersetRecord().getId());
        transaction.attach((SegmentsetRecord)this);
        probandsetRec = pbsRec;
        segmentPojos = segList;
    }

    public int setsize() {
        return segmentPojos.size();
    }

    public boolean isForUpdate() {
        boolean updating = false;
        try {
            Segment test = segmentPojos.get(0);
            SegmentRecord testRec = transaction.selectFrom(SEGMENT)
                .where(SEGMENT.PROBANDSET_ID.equal(probandsetRec.getId())
                       .and(SEGMENT.CHROM.equal(test.getChrom()))
                       .and(SEGMENT.STARTBASE.equal(test.getStartbase())))
                .fetchOne();
            if (testRec != null) {
                updating = true;
            }
        }
        catch (TooManyRowsException tmre) {
            updating = false; //just for something todo
        }
        return updating;
    }

    public void updateSegmentsById() {
        // The chaser's call
        for (Segment chasedSeg : segmentPojos) {
            SegmentRecord testSegment = transaction.selectFrom(SEGMENT).where(SEGMENT.ID.equal(chasedSeg.getId())).fetchOne();
            if (testSegment == null) {
                throw new RuntimeException("Did not find segment by id: " + chasedSeg.getId().toString());
            }
            transaction.update(SEGMENT)
                .set(SEGMENT.EVENTS_LESS, SEGMENT.EVENTS_LESS.add(chasedSeg.getEventsLess()))
                .set(SEGMENT.EVENTS_EQUAL, SEGMENT.EVENTS_EQUAL.add(chasedSeg.getEventsEqual()))
                .set(SEGMENT.EVENTS_GREATER, SEGMENT.EVENTS_GREATER.add(chasedSeg.getEventsGreater()))
                .where(SEGMENT.ID.equal(chasedSeg.getId()))
                .execute();
            procEx.addInput(chasedSeg.getClass().getSimpleName(), chasedSeg.getId().toString());
            procEx.addArg("eventsLess", chasedSeg.getEventsLess());
            procEx.addArg("eventsEqual", chasedSeg.getEventsEqual());
            procEx.addArg("eventsGreater", chasedSeg.getEventsGreater());
        }
    }
}
