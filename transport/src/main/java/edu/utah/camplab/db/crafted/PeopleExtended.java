package edu.utah.camplab.db.crafted;

import edu.utah.camplab.db.generated.base.tables.records.PeopleRecord;
import edu.utah.camplab.db.generated.base.tables.records.PersonRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetRecord;
import jpsgcs.graph.DAG;
import org.jooq.DSLContext;

import java.util.*;

import static edu.utah.camplab.db.generated.base.Tables.*;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET;

public class PeopleExtended extends PeopleRecord {
    private static final long serialVersionUID = 4379673933557413311L;
    private Map<UUID, PersonRecord> personsIdMap            = new HashMap<UUID, PersonRecord>();
    private DAG<PersonRecord, UUID> dag                     = new DAG<PersonRecord, UUID>();
    private Map<String, ProbandsetRecord> nameProbandsetMap = null;


    public PeopleExtended() {}
    public PeopleExtended(PeopleRecord pop) {
        super(pop.getId(),
              pop.getName(),
              null);
    }

    public void setPersonsIdMap(DSLContext context) {
        if ( personsIdMap.size() == 0) {
            personsIdMap.putAll(context.select(PERSON.fields())
                                .from(PERSON.join(PEOPLE_MEMBER).on(PERSON.ID.equal(PEOPLE_MEMBER.PERSON_ID))
                                      .join(PEOPLE).on(PEOPLE.ID.equal(PEOPLE_MEMBER.PEOPLE_ID)))
                                .where(PEOPLE.NAME.equal(getName()))
                                .fetchMap(PERSON.ID, r -> r.into(PERSON)));
        }
        for (PersonRecord personRec: personsIdMap.values()) {
            dag.connect(personRec, personsIdMap.get(personRec.getPa()));
            dag.connect(personRec, personsIdMap.get(personRec.getMa()));
        }
    }

    public DAG<PersonRecord, UUID> getDag(DSLContext context) {
        if (personsIdMap == null) {
            setPersonsIdMap(context); //sets DAG too
        }
        return dag;
    }

    public Map<String, UUID> getProbandsetIdMap(DSLContext context) {
    	// So long as "File based" SGS is supported
        Map<String, UUID> csvUUIDMap = new HashMap<>();
        Map<String, ProbandsetRecord> fullmap = getNameProbandsetMap(context);
        for (Map.Entry<String, ProbandsetRecord> fullMapME : fullmap.entrySet()) {
        	String[] pbsNameParts = fullMapME.getKey().split(",");
        	// should use LinkageIdSet here, but don't want the import?
        	Arrays.sort(pbsNameParts);
        	StringBuilder sb = new StringBuilder(pbsNameParts[0]);
        	for (int i = 1; i < pbsNameParts.length; i++) {
        		sb.append("," + pbsNameParts[i]);
        	}
                csvUUIDMap.put(sb.toString(), fullMapME.getValue().getId());
        }
        return csvUUIDMap;
    }
            
    public Map<String, ProbandsetRecord> getNameProbandsetMap( DSLContext context ) {
        if (nameProbandsetMap == null) {
            // CAVEAT: expecting csv format in name of pbs
            nameProbandsetMap = new HashMap<>();
            nameProbandsetMap = context.selectFrom(PROBANDSET)
                .where(PROBANDSET.PEOPLE_ID.equal(getId()))
                .fetchMap(PROBANDSET.NAME);
        }
        return nameProbandsetMap;
    }
        
    // Waiting for ProbandSetExtended
    public static boolean idIn(UUID[] idset, UUID id) {
        boolean found = false;
        for(UUID u : idset) {
            if (u.equals(id)) {
                found = true;
                break;
            }
        }
        return found;
    }

    public void assignMeiosesCount(DSLContext context, ProbandsetRecord pbsRec) {
        Set<PersonRecord>rel_anc = new TreeSet<>();
        Set<PersonRecord>common_anc = new TreeSet<>();
        common_anc.addAll(getDag(context).getVertices());
        
        Set<PersonRecord>tmp = new TreeSet<>();
        for (PersonRecord perRec : personsIdMap.values()) {
            if ( ! PeopleExtended.idIn(pbsRec.getProbands(),perRec.getId())) {
                continue;
            }
            tmp.addAll(dag.ancestors(perRec));
            tmp.add(perRec);
            rel_anc.addAll(tmp);
            common_anc.retainAll(tmp);
        }
        common_anc.remove(null);
        tmp.clear();

        Set<PersonRecord>rem_anc = new TreeSet<>();
        for (PersonRecord id1 : common_anc) {
            for (PersonRecord id2 : common_anc) {
                tmp.addAll(dag.ancestors(id2));
                if ( tmp.contains(id1) ) {
                    rem_anc.add(id1);
                }
            }
            tmp.clear();
        }
        for (PersonRecord prec : rem_anc) {
            rel_anc.remove(prec);
            common_anc.remove(prec);
        }
        Collection<PersonRecord> descendants = dag.descendants(common_anc.iterator().next());
        descendants.retainAll(rel_anc);
        
        pbsRec.setMeioses(descendants.size());
        pbsRec.store();
    }

    // TODO: This is a save-time lookup, in a loop!
    public ProbandsetRecord getSingleProbandset(DSLContext context, String csvName) {
        ProbandsetRecord rec = context.selectFrom(PROBANDSET).where(PROBANDSET.NAME.equal(csvName)
                                                                .and(PROBANDSET.PEOPLE_ID.equal(getId())))
            .fetchOne();
        return rec;
    }
}
                                    

