package edu.utah.camplab.db.crafted;

import edu.utah.camplab.db.generated.base.tables.records.PeopleRecord;
import edu.utah.camplab.db.generated.base.tables.records.PersonRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetGroupMemberRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetGroupRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetRecord;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

import static edu.utah.camplab.db.generated.base.Tables.*;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET_GROUP_MEMBER;

public class ProbandsetExtended {
    public static final Logger logger = LoggerFactory.getLogger(ProbandsetExtended.class);
    public static UUIDComparator uidComp = new UUIDComparator();


    public static ProbandsetRecord findProbandsetByNames(DSLContext dbc, List<String> probandNames, PeopleRecord peopleRec) {
        UUID[] uids = ProbandsetExtended.idsFromPersonNames(dbc, probandNames, peopleRec);
        ProbandsetRecord pRec = dbc.selectFrom(PROBANDSET).where(PROBANDSET.PROBANDS.equal(uids)).fetchOne();
        return pRec;
    }
    
    public static ProbandsetRecord findOrMakeProbandset(DSLContext dbc, List<String> probandNames) {
        return ProbandsetExtended.findOrMakeProbandset(dbc, probandNames, null, null);
    }

    public static ProbandsetRecord findOrMakeProbandset(DSLContext dbc, List<String> probandNames, PeopleRecord peopleRec) {
        return ProbandsetExtended.findOrMakeProbandset(dbc, probandNames, peopleRec, null);
    }

    public static ProbandsetRecord findOrMakeProbandset(DSLContext dbc, List<String> probandNames, PeopleRecord peopleRec, Integer meioses) {
        UUID[] memberIds = idsFromPersonNames(dbc, probandNames, peopleRec);
        ProbandsetRecord pRec =
            dbc.selectFrom(PROBANDSET)
            .where(PROBANDSET.PROBANDS.equal(memberIds))
            .fetchOne();
        
        // TODO: undo the postgres specific array trick. Maybe.
        if (pRec == null) {
        	
            UUID newsetId = UUID.randomUUID();
            String setName = probandNames.stream().collect(Collectors.joining(","));
            
            pRec = dbc.newRecord(PROBANDSET);
            pRec.set(PROBANDSET.ID, newsetId);
            pRec.set(PROBANDSET.NAME, setName);
            pRec.set(PROBANDSET.PROBANDS, memberIds);
            pRec.set(PROBANDSET.PEOPLE_ID, peopleRec.getId());
        }
        pRec.set(PROBANDSET.MEIOSES, meioses);  //
        return pRec;
    }

    public static ProbandsetRecord makeProbandset(DSLContext dbc, String name, Collection<UUID> persons, PeopleRecord peopleRec, Integer meioses, double[] kincoef) {
        ProbandsetRecord pbsRec = ProbandsetExtended.makeProbandset(dbc, name, persons, peopleRec, meioses);
        if (kincoef != null) {
            pbsRec.setMinKincoef(kincoef[0]);
            pbsRec.setMaxKincoef(kincoef[1]);
        }
        return pbsRec;
    }
    
    public static ProbandsetRecord makeProbandset(DSLContext dbc, String name, Collection<UUID> persons, PeopleRecord peopleRec, Integer meioses) {
        
        UUID newsetId = UUID.randomUUID();
        UUID[] ids = new UUID[persons.size()];
        ids = persons.toArray(ids);
        Arrays.sort(ids, uidComp);
        ProbandsetRecord pRec = dbc.newRecord(PROBANDSET);
        pRec = dbc.newRecord(PROBANDSET);
        pRec.set(PROBANDSET.ID, newsetId);
        pRec.set(PROBANDSET.NAME, name);
        pRec.set(PROBANDSET.PROBANDS, ids);
        pRec.set(PROBANDSET.PEOPLE_ID, peopleRec.getId());
        pRec.set(PROBANDSET.MEIOSES, meioses);
        return pRec;
    	
    }

    private static UUID[] idsFromPersonNames(DSLContext dbc, List<String> names, PeopleRecord peeps) {
        UUID[] idArray = null;
        idArray = dbc.select(PERSON.ID)
            .from(PERSON)
            .join(PEOPLE_MEMBER).on(PERSON.ID.equal(PEOPLE_MEMBER.PERSON_ID))
            .where(PERSON.NAME.in(names).and(PEOPLE_MEMBER.PEOPLE_ID.equal( peeps.getId())))
            .orderBy(PERSON.ID)
            .fetchArray(PERSON.ID);
        if (idArray.length != names.size()) {
            logger.error("Trouble making probandset for {} in people {}", names, peeps.getName());
            idArray = null;
        }
        return idArray;
    }

    public static List<ProbandsetGroupMemberRecord> getProbandsetGroupMembership(DSLContext dbc, ProbandsetGroupRecord grouper) {
	return dbc.selectFrom(PROBANDSET_GROUP_MEMBER).where(PROBANDSET_GROUP_MEMBER.GROUP_ID.equal(grouper.getId())).fetch();
    }

    public static ProbandsetGroupMemberRecord extendProbandsetGroup(DSLContext dbc, ProbandsetRecord pbsRec, ProbandsetGroupRecord grouper) {
        //TODO: this should get people_id
        ProbandsetGroupMemberRecord pgRec = dbc.newRecord(PROBANDSET_GROUP_MEMBER);
        pgRec.setGroupId(grouper.getId());
        pgRec.setMemberId(pbsRec.getId());
        logger.debug("(MAYBE) added probandset {} to group {}", pgRec.getGroupId().toString(),  grouper.getId().toString());
        return pgRec;
    }

    public static ProbandsetRecord findByProbandIds(DSLContext dsl, PeopleRecord peopleRec, Set<PersonRecord> probands) {
        Set<UUID> idset = new HashSet<>();
        for (PersonRecord p : probands) {
            idset.add(p.getId());
        }
        return ProbandsetExtended.findByProbandIds(dsl, idset, peopleRec);
    }

    public static ProbandsetRecord findByProbandIds(DSLContext dsl, Set<UUID> probandIds, PeopleRecord peopleRec) {
        UUID[] pbsIds = probandIds.toArray(new UUID[probandIds.size()]);
        Arrays.sort(pbsIds, uidComp);
        ProbandsetRecord pbsRec = dsl
            .selectFrom(PROBANDSET)
            .where(PROBANDSET.PROBANDS.equal(pbsIds)
                   .and(PROBANDSET.PEOPLE_ID.equal(peopleRec.getId())))
            .fetchOne();
        return pbsRec;
    }

    public static ProbandsetRecord findByProbands(DSLContext dsl, Set<PersonRecord> probands, PeopleRecord peopleRec) {
        Set<UUID> pids = new HashSet<>();
        for (PersonRecord p : probands) {
            pids.add(p.getId());
        }
        return ProbandsetExtended.findByProbandIds(dsl, pids, peopleRec);
    }

    // TODO: is this really peopleExtended?
    public UUID getPeopleId(DSLContext dsl, String[] names) {
        List<String> nameList = Arrays.stream(names).collect(Collectors.toList());
        return getPeopleId(dsl, nameList);
    }
    
    public UUID getPeopleId(DSLContext dsl, List<String> names) {
        UUID thisPeople = dsl
            .selectDistinct(PEOPLE_MEMBER.PEOPLE_ID)
            .from(PEOPLE_MEMBER)
            .join(PERSON).on(PEOPLE_MEMBER.PERSON_ID.equal(PERSON.ID))
            .join(ALIAS).on(PERSON.ID.equal(ALIAS.PERSON_ID))
            .where(ALIAS.AKA.in(names))
            .fetchOne().value1();
        if (thisPeople == null) {
            throw new RuntimeException("Failed to find exactly one people named " + names.toString());
        }
        return thisPeople;
    }
}
