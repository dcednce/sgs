package edu.utah.camplab.db.crafted;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.utah.camplab.db.generated.base.tables.pojos.People;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Probandset;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static edu.utah.camplab.db.generated.base.Tables.PEOPLE;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET;
import static edu.utah.camplab.db.generated.default_schema.Tables.SEGMENT;

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "class" )
public class SegmentExtended extends Segment {
  private static final long serialVersionUID = -1L;

  public static final Logger logger = LoggerFactory.getLogger(SegmentsetExtended.class);

  private Segment segment;
  private Probandset probandset = null;
  private People people;
  
  public SegmentExtended() {}

  public void fill(DSLContext ctx, UUID segmentId) {
    segment = ctx.selectFrom(SEGMENT).where(SEGMENT.ID.equal(segmentId)).fetchOne().into(Segment.class);
    probandset = ctx.selectFrom(PROBANDSET).where(PROBANDSET.ID.equal(segment.getProbandsetId())).fetchOne().into(Probandset.class);
    people = ctx.selectFrom(PEOPLE).where(PEOPLE.ID.equal(probandset.getPeopleId())).fetchOne().into(People.class);
  }
  
  /**
   * Gets the value of serialVersionUID
   *
   * @return the value of serialVersionUID
   */
  public static final long getSerialVersionUID() {
    return SegmentExtended.serialVersionUID;
  }

  /**
   * Gets the value of segment
   *
   * @return the value of segment
   */
  public final Segment getSegment() {
    return this.segment;
  }

  /**
   * Sets the value of segment
   *
   * @param argSegment Value to assign to this.segment
   */
  public final void setSegment(final Segment argSegment) {
    this.segment = argSegment;
  }

  /**
   * Gets the value of probandset
   *
   * @return the value of probandset
   */
  public final Probandset getProbandset() {
    return this.probandset;
  }

  /**
   * Sets the value of probandset
   *
   * @param argProbandset Value to assign to this.probandset
   */
  public final void setProbandset(final Probandset argProbandset) {
    this.probandset = argProbandset;
  }

  /**
   * Gets the value of people
   *
   * @return the value of people
   */
  public final People getPeople() {
    return this.people;
  }

  /**
   * Sets the value of people
   *
   * @param argPeople Value to assign to this.people
   */
  public final void setPeople(final People argPeople) {
    this.people = argPeople;
  }

}
