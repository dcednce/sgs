import java.net.http.*;
import java.net.*;

// Can we raise our server
public class TestPingMonitor {
  private String sgsHost;
  private int sgsPort;

  public TestPingMonitor(String h, int p) {
    sgsHost = h;
    sgsPort = p;
  }

  public static void main(String[] args) {
    String host = args[0];
    int port = Integer.parseInt(args[1]);

    TestPingMonitor tpm = new TestPingMonitor(host, port);
    tpm.ping();
  }

  public void ping() {
    String url = String.format("https://%s:%d", sgsHost, sgsPort);
    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder()
      .uri(URI.create(url+"/sgs/webmonitor"))
      .build();
    client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
      .thenApply(HttpResponse::statusCode)
      .thenAccept(System.out::println)
      .join();
  }
}
