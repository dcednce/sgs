import java.util.UUID;
import java.util.TreeSet;

public class TestUUIDSort {

  public static void main(String[] args) {

    TreeSet<String> tree = new TreeSet<>();
    for (int i = 0; i < 6; i++) {
      UUID id = UUID.randomUUID();
      System.out.printf("%d: %s\n", i, id);
      tree.add(id.toString());
    }
    System.out.println("From string tree iter");
    for (String id : tree) {
      System.out.println(id);
    }
  }
}
