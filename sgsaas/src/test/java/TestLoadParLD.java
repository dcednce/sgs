
import edu.utah.camplab.jps.SGSFormatter;
import edu.utah.camplab.jps.SGSParameterData;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageParameterData;

import java.util.List;

public class TestLoadParLD
{
  public static void main(String[] args)
  {
    try
      {
	boolean forceLinkageEquilibrium = false;

	SGSFormatter parin = null;

	switch(args.length)
	  {
	  case 0:
	  case 1:
	    //parin = new LinkageFormatter();
	    System.err.println(" Need par, ld file with optional toggle-on forceing ld build");
	    break;
	  case 2:
	  default:
	    String[] s3uriParts = parseS3URI(args[0]);
	    parin = new SGSFormatter(s3uriParts[0], s3uriParts[1]);
	    forceLinkageEquilibrium = args.length == 2;
	    break;
	  }

	LinkageParameterData lpd = new SGSParameterData(parin);
	LDModel ld = null;

	if (forceLinkageEquilibrium)
	  ld = new LDModel(new LinkageInterface(new LinkageDataSet(lpd,null)));
	else
	  ld = new LDModel(parin);

	ld.report();
	ld.writeTo(System.out);
      }
    catch (Exception e)
      {
	e.printStackTrace();
      }
  }
  private static String[] parseS3URI(String sarg) {
    String retval[] = new String[2];
    int idx = sarg.indexOf('/',5);
    //int idx = sarg.lastIndexOf('/');
    String bucket = sarg.substring(0,idx);
    String path = sarg.substring(idx+1);
    retval[0] = bucket;
    retval[1] = path;
    return retval;
  }
//  private static void listBuckets() {
//    List<Bucket> buckets = .listBuckets();
//
//    System.out.println("Your Amazon S3 buckets are:");
//    for (Bucket b : buckets) {
//      System.out.println("* " + b.getName());
//    }
//
//  }
}
