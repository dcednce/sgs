import jpsgcs.linkage.LinkageData;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.InputFormatter;

import java.io.*;

public class TestLoadLD {

  public static void main(String[] args) {
    try {
      //par, then ped
      LinkageData lds = new LinkageData(args[0], args[1]);
      LDModel ld = new LDModel(lds);
      ld.report();
      ld.writeTo(new PrintStream(new BufferedOutputStream(new FileOutputStream("rewritten.lg"))));
    }
    catch(IOException ioe) {
      ioe.printStackTrace();
    }
  }
}
