#+TITLE: SGS Project Construction
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=0.75in]{geometry}*
#+OPTIONS: ^:nil
* Database initialization
We presume the installation of a PostgreSQL server and the necessary
permissions to complete the tasks below. Bash shell scripts are
provided which
  - must be run by a user with create database privilege (e.g. postgres)
  - create separate postgres databases per investigator
  - create separate schema for each of study for a given investigator
  - create separate schema for bulk operations
    - an implementation detail (speeds up save of multi-assess results)
** Database structure
  - all studies share a common schema for
    - *people* (populations, pedigrees, aribitrary named collections of persons)
    - *person* (one human)
    - *alias* (any known name for any individual)
    - *markerset* (a collection of markers, e.g. one chromosome from one chip)
    - *marker* (known SNP)
    - administrative tables (*definer*, *definervalue*) managing meanings
      of terms
    - membership tables, *people_member*, *markerset_member* grouping
      persons, markers respectively.
  - study specific data tables
    - tables related to segment generation and management
      - *segment*, *duo_segment*, *threshold*, *chaseable*, *imputed_pvalue*,
    - operational history
      - *process*, *process_input*, *process_output*, *process_arg*
    - proband set management
      - *probandset*, *probandset_group*, *probandset_group_member*
** Database scripts
   - /createProject.sh/ creates all shared tables and functions for
     one investigator including an initial SGS study.
#+BEGIN_SRC
usage: ./createProject.sh --dbhost|-d <postgresHost> --project|-p <projectName> --sgsdb|-s <sgs database> --schema-dir|-c <schemaFile> [--regen|-r]
       ./createProject.sh --help|-h 
future: ./createSGSdb.sh --dbhost|-d <postgresHost> --investigator|-n <sgs database>  --study|-s <studyName> --schema-dir|-c <schemaFile>  --regen|-r
#+END_SRC
   - /createRole.sh/ adds necessary components for a new set of study
     specific tables and functions
#+BEGIN_SRC
usage: ./createRole.sh --dbhost|-d <postgresHost> --project|-p <projectName> --sgsdb|-s <sgs database> --schema-def|-c 
       ./createRole.sh --help|-h 
future: ./createStudy.sh --dbhost|-h <postgresHost> --investigator|-n <projectName> --study|-s <studyName> --schema-def|-c
#+END_SRC
   - initial DDL supplied
     - /baselineSchema.sql/ adds all shared tables per investigator
     - /projectSchema.sql/ add necessary study specific tables (*should rename: studySchema*)
     - /newRoleTemplate.sql/ adds necessary components for study specific database access

* Project initialization
  Loading the pedigree and marker and genotypic data is accomplished via the java
  programme *PedParLoader*
   - command line invocation:
#+BEGIN_SRC
java edu.utah.camplab.app.PedParLoader --investigator <projectName> --pedfof file --parfof --db host:port
#+END_SRC
   - a local definition of CLASSPATH is necessary
   - use *--help* for complete operational description
   - *projectName* same as used for /createProject.sh --project <name>/
   - uses file-of-filenames for the .ped files and .ld files
     - able to load addtional pedigrees later in project lifespan

* Project execution
  How one completes an SGS project is entirely driven by the available
  computing infrastructure.  That said, it proceeds in the following
  order:
  ** MultiAssess
    - one invocation of SGSMultiAssess per chromosome per pedigree
    - from 300,000 to 1,000,000 simulations per above, depending on
      the maximum number of genotyped cases.
  ** Threshold ascertainment
    - generate the genome-wide set of segment which provide the
      optimal per-marker p-values
    - sgsAnalysis: an R package which fits the optimal set to a
      (Guassian) curve and assigns the suggestive and significant
      boundaries.
  ** P-value refinement
    - invocations of SGSRun directed at specific segments
    - additional simulations on all segments for which there remains
      uncertainly as to significance or suggestiveness
  ** Literature and bench work on interesting segment

