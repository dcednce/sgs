alter table sgstemplate.threshold_duo_segment rename column entry1_id to threshold_segment1_id;
alter table sgstemplate.threshold_duo_segment rename column entry2_id to threshold_segment2_id;
alter table sgstemplate.chaseable add column passes text null check (passes = any(array['sug', 'sig']));
