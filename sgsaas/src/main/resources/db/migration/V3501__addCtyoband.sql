create table if not exists base.cytoband(
  chrom        text,
  chrom_start  integer,
  chrom_end    integer,
  name         text,
  gie_stain    text
);

grant all on table base.cytoband to sgstemplate;
