alter table sgstemplate.threshold_segment drop column begin_contribution;
alter table sgstemplate.threshold_segment drop column end_contribution;
alter table sgstemplate.threshold_segment rename column threshold_segment to segment_id;
create unique index on sgstemplate.threshold_segment(threshold_id, segment_id);
--
alter table sgstemplate.threshold_duo_segment drop column overlap_start;
alter table sgstemplate.threshold_duo_segment drop column overlap_end;
alter table sgstemplate.threshold_duo_segment rename column threshold_segment1 to entry1_id;
alter table sgstemplate.threshold_duo_segment rename column threshold_segment2 to entry2_id;


