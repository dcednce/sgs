create table if not exists sgstemplate.threshold_segment(
       id uuid primary key,
       threshold_id uuid not null /*references sgstemplate.threshold(id)*/,
       threshold_segment uuid not null /*references sgstemplate.segment(id)*/,
       begin_contribution int null,
       end_contribution int null,
       smooth_pvalue float not null,
       mu_t float );
--
insert into sgstemplate.threshold_segment (id, threshold_id, threshold_segment, smooth_pvalue, mu_t)
select o.id, o.threshold_id, o.segment1_id, o.duo_optval, mu_t
from sgstemplate.duo_segment o
where o.segment2_id is null;
----alter table bdef.segmentset_member add constraint segmentset_member_segment_id_fkey foreign key (segment_id) references bdef.segment(id);
alter table sgstemplate.threshold_segment add constraint threshold_segment_threshold_id_fkey foreign key (threshold_id) references sgstemplate.threshold(id);
alter table sgstemplate.threshold_segment add constraint threshold_segment_threshold_segment_fkey foreign key (threshold_segment) references sgstemplate.segment(id);
--
create table if not exists sgstemplate.threshold_duo_segment (
       id uuid primary key,
       threshold_id uuid not null references sgstemplate.threshold(id),       
       threshold_segment1 uuid not null /*references sgstemplate.threshold_segment(id)*/,
       threshold_segment2 uuid not null /*references sgstemplate.threshold_segment(id)*/,
       overlap_start int null,
       overlap_end int null,
       fisher_pvalue float not null,
       mu_t float );
--
-- Here we're forcing the old duo_segment into new structure: the lie
-- here is that the old optimal value was a fisher-combined of the
-- segments' actual event counts
insert into sgstemplate.threshold_duo_segment (id, threshold_id, threshold_segment1, threshold_segment2, overlap_start, overlap_end, fisher_pvalue, mu_t)
select o.id, o.threshold_id, s.id, t.id, null::int, null::int, o.duo_optval, o.mu_t
from sgstemplate.duo_segment o
     join sgstemplate.threshold_segment s on o.segment1_id = s.threshold_segment
     join sgstemplate.threshold_segment t on o.segment2_id = t.threshold_segment
where o.segment2_id is not null;
--
alter table sgstemplate.threshold_duo_segment add constraint threshold_duo_segment_threshold_segment1_fkey foreign key (threshold_segment1) references sgstemplate.segment(id);
alter table sgstemplate.threshold_duo_segment add constraint threshold_duo_segment_threshold_segment2_fkey foreign key (threshold_segment2) references sgstemplate.segment(id);
--
-- new paired chaseables
create table if not exists sgstemplate.duo_chaseable(
     id             uuid primary key,
     threshold_id   uuid not null references sgstemplate.threshold(id),
     segment1_id    uuid not null references sgstemplate.segment(id),
     segment2_id    uuid not null references sgstemplate.segment(id),
     fisher_pvalue  double precision not null,
     passes         text null check( passes = any(array['sug', 'sig']))
 );
--
-- unhook some references en route to shedding segmentset_member\p\r
-- alter table sgstemplate.duo_segment drop constraint duo_segment_segment1_id_fkey;
-- alter table sgstemplate.duo_segment drop constraint duo_segment_segment2_id_fkey;
-- alter table sgstemplate.segmentset_member drop constraint segmentset_member_segment_id_fkey;
drop  table sgstemplate.segmentset_member;
-- stash duo_segment for posterity
alter table sgstemplate.duo_segment rename to original_duo_segment;
