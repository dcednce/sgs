create or replace function public.threshold_process( thresholdid uuid, pbsgid uuid[], marker_rgx text, empirical boolean)
returns UUID
language plpgsql
as $$
declare
  pid uuid;
  i uuid;
  purpose text;
begin
  if empirical then
     purpose := 'empirical optimal set generation';
  else
     purpose := 'threshold optimal set generation';
  end if;
  pid = uuid_generate_v4();
  insert into process values( pid, current_user, purpose, clock_timestamp(), inet_client_addr()::text);
  foreach i in array pbsgid loop
    insert into process_input values (uuid_generate_v4(), pid, 'probandset_group', i);
  end loop;
  insert into process_output values (uuid_generate_v4(), pid, 'threshold', thresholdid);
  insert into process_arg values (uuid_generate_v4(), pid, 'marker regex', null, null, marker_rgx);
  return pid;
end;
$$;
