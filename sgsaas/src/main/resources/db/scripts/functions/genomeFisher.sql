create or replace function public.genome_fisher(tids uuid[])
returns table(tseg1 uuid, tseg2 uuid, fisher_combined float)
as $$
declare
  rc           int;
  markersdone  int = 0;
  segn         int = 0;
  tid          uuid;
  mkset        uuid;
  fisher       float;
  t1           RECORD;
  t2           RECORD;
  
begin
    raise notice '%: Off we go', clock_timestamp();
    create temp table fishers(tseg1 uuid, tseg2 uuid, fisher_combined float) on commit drop;
    -- we believe all input thresholds use the same markers
    for chrm in 1..22
    loop
        select distinct s.markerset_id into mkset from segment s 
               where exists(select * from threshold_segment t where t.segment_id = s.id and t.threshold_id = tids[1]) and s.chrom = chrm;
--
        create temp table tlist on commit drop as    
        select t.id as threshid, v.id as tsid, v.smooth_pvalue, s.id as segid, s.firstmarker, s.lastmarker, s.lastmarker-s.firstmarker as seglength
        from threshold t
             join threshold_segment v on t.id = v.threshold_id 
             join segment s on v.segment_id = s.id
        where t.id = any (tids) and s.markerset_id = mkset;
--        
        get diagnostics rc = row_count;
        raise notice '%: Starting % with % threshold segments across %', clock_timestamp(), chrm, rc, array_to_string(tids,'/');
        create index on tlist(smooth_pvalue, threshid);
        create index on tlist(tsid);
        raise notice '%: indexing done', clock_timestamp();
--
        drop table if exists mkridx;
        create temp table mkridx on commit drop as
            select m.member_id, m.ordinal from markerset_member m where m.markerset_id = mkset;
        get diagnostics rc = row_count;
        raise notice '%: chromosome % has % markers', clock_timestamp(), chrm, rc;
        create index on mkridx(ordinal);
        --
        for t1 in 
            select * from tlist t order by smooth_pvalue asc, seglength desc
        loop
        segn = 0;
        for t2 in
                select * from tlist l
                    where l. smooth_pvalue >= t1.smooth_pvalue and l.threshid != t1.threshid
                    order by l.smooth_pvalue
            loop
                segn = segn + 1;
                -- raise notice '%: working with t2=%', clock_timestamp(), t2.tsid;
                select count(*) into rc from mkridx x where x.ordinal >= t1.firstmarker and x.ordinal <= t1.lastmarker;  --still some markers for boss tseg
                if rc > 0 then
                -- if FOUND then
                   -- raise notice '%: Still % markers for boss %', clock_timestamp(), rc, t1.tsid;
                   delete from mkridx x 
                          where (x.ordinal >= t1.firstmarker and x.ordinal >= t2.firstmarker) and (x.ordinal <= t1.lastmarker and x.ordinal <= t2.lastmarker);
                   get diagnostics rc = row_count;
                   if rc > 0 then
                       -- raise notice '%: did a delete', clock_timestamp();
                       markersdone = markersdone + rc;                       
                       fisher = duopv(t1.smooth_pvalue, t2.smooth_pvalue);
                       -- raise notice '%: pairing %,% accounts for %, fish = %, markersdone = %', clock_timestamp(), t1.segid, t2.segid, rc, fisher, markersdone;
                       insert into fishers values(t1.tsid, t2.tsid, fisher);
                   -- else
                       -- raise notice '%: zero delete %/%', clock_timestamp(), t1.tsid, t2.tsid;
                   end if;
                else
                   -- raise notice 'zero markers left for %', t1.tsid;
                   exit;
                end if;
            end loop; --t2
            select count(*) into rc from mkridx;
            if rc = 0 then
               raise notice '%: zero markers left after % primary segments', clock_timestamp(), segn;
               exit;
            end if;
        end loop; -- t1
        -- select count(*) into rc from mkridx;
        raise notice '%: finished chromosome %, covered % markers', clock_timestamp(), chrm, markersdone;
        drop table if exists tlist;
    end loop; --chrom
    return query select * from fishers;
end;
$$ language plpgsql;
