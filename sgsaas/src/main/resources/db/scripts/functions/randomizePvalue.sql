create or replace function public.rand_pv(bounds float[]) 
returns float
as 
$$
select random()*(bounds[2]-bounds[1]) + bounds[1];
$$
language sql
;
