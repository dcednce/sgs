----
-- when two persons need to be unified
----
create or replace function person_meld(loser uuid, keeper uuid)
returns void
as
$$
declare
  peoplelinks uuid[];
begin
  select array_agg(p.name) into peoplelinks
  from people p join people_member m on p.id = m.people_id
  where m.person_id = loser;
--
  delete from people_member where person_id = loser;
  create temp table repset as
         select * from probandset where probands @> array[loser];
  update repset t
          set probands = sortuuidvector(array_replace(probands, loser, keeper));
-- might prefer alias here but I hit a case where id did not have an
-- alias, which really should be an error case
  create temp table dename as
  select s.id
         , array_agg(a.name order by a.name) as dnames
         , array_length(array_agg(a.name order by a.name),1) namecount
  from probandset s join person a on a.id = any(s.probands)
       join people_member m on a.id = m.person_id
       join people p on m.people_id = p.id
  where p.id = s.people_id
  group by s.id;
end;
$$
language plpgsql;

