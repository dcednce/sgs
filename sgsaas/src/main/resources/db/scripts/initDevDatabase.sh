#!/bin/bash -e
function usage() { 
    printf "This script will build the prototype database from which the \n"
    printf "jOOQ code generation will attain necessary table structures \n"
    printf "\nusage:\n"
    printf "%s --host <postgres-host> --dbname <database-name> [ --path <checkout-dir> --admin <db-admin> --port <database-port>]\n" $0
    printf "where:"
    printf "\n\t%-15s names the postgres server" "postgres-host"
    printf "\n\t%-15s is a database previously created on postgres server" "database-name"
    printf "\n\t%-15s git checkout directory. Defaults to pwd" "script-dir"
    printf "\n\t%-15s is postgres superuser account, owner of the database. Defaults to postgres" "db-admin"
    printf "\n\t%-15s is the port postgres uses. Defaults to 5432" "database-port"
    printf "\n"
    exit 2
 }

dbport=5432
codepath=`pwd`
scriptDir="$codepath/src/main/resources/db/scripts"
dbadmin="postgres"
while [[ ${#@} -gt 0 ]]
do
    case $1 in
        -a|--ad*)
            shift
            dbadmin=$1;
            shift;;
        -p|--pa*)
            shift;
            codepath=$1
            scriptDir="$codepath/src/main/resources/db/scripts"
            if [[ ! -e ${scriptDir}/$0 ]] 
            then
                printf "** %s does not look like the correct place!\n" $scriptDir
                usage
            fi
            shift;;
        -o|--ho*)
            shift;
            hostname=$1
            shift;;
        -d|--db*)
            shift
            dbname=$1;
            shift;;
        -t|--po*)
            shift
            dbport=$1
            shift;;
        -h|--he*)
            usage;;
        *)
            usage;;
    esac
done
if [[ -z $hostname || -z $dbname ]]
then
    printf "ERROR: We need both host and database name\n\n"
    usage
fi
printf "Any messages stating \'ERROR:  role \"sgstemplate\" does not exist\' are expected\n"
psql --host=$hostname --dbname=$dbname --user=$dbadmin --port $dbport <<EOF
\i $scriptDir/functions/pv.sql
\i $scriptDir/functions/uuidFunctions.sql
\i $scriptDir/baselineSchema.sql
\i $scriptDir/projectSchema.sql
EOF
printf "Any messages stating \'ERROR:  role \"sgstemplate\" does not exist\' are expected\n"

sed -i -e s/sgsmigration/$dbname/ -e s/kitsegas/$hostname/ -e s/5433/$dbport/ $codepath/gradle.properties
printf "%s/gradle.properties has been updated to reflect the local template installation\n" $codepath
