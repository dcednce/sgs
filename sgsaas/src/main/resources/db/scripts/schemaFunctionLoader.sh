#!/bin/bash -e
declare -a sqlList
declare -a piList
if [[ ! -n $PG_SUSER ]]
then
    printf "You must set PG_SUSER to postgres admin account name\n"
    exit 2
fi

function usage {
    printf "usage: %s dbHost pi schemataCSV sqlfile1 [ sqlfile2 ... ]\n" $0
    exit 2
}


if [[ ${#@} < 4 ]]
then
    usage;
fi
hostnm=$1; shift
pi=$1; shift
schemata=(`echo $1 | sed s/,/\ /g`); shift
sqlList=( "$@" )

for schema in "${schemata[@]}"
do
    echo doing sch $schema
    for sqlfile in ${sqlList[*]}
    do
        echo loading $pi on $hostnm
        psql --dbname=$pi --user=$PG_SUSER --host=$hostnm <<EOF
        set search_path = $schema,base,public;
        \i $sqlfile
EOF

    done
done

