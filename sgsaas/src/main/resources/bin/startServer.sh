#!/bin/bash -ex
#if [[ -z "$WORKAREA" ]]; then echo set WORKAREA and try again; exit 2; fi
if [[ -z "$VCDIR" ]]; then echo set VCDIR and try again; exit 2; fi
if [[ -z $CATALINA_HOME ]]
then
    export CATALINA_HOME=${WORKAREA:-/home/rob/utah/server/tc}/aws/deploy/webroot
    mkdir -p $CATALINA_HOME
fi
export CATALINA_BASE=$CATALINA_HOME
#export SGSWEBDIR=$CATALINA_HOME/tomcat.16004/work/Tomcat/localhost
export SGSWEBDIR=$CATALINA_HOME/webhost
if [[ "$1" == "rebuild" ]]
then
    shift;
    if [[ ! -e $SGSWEBDIR/sgs ]]
    then
        mkdir -p $SGSWEBDIR/sgs
    else
        echo clean out sgs app
        rm -rf $SGSWEBDIR/sgs/*
    fi
    printf "webapp at %s\n" $SGSWEBDIR
    cp ${VCDIR}/webapp/build/libs/sgs-1.0.war $SGSWEBDIR/sgs/sgs.war
    cd $SGSWEBDIR/sgs; jar -xf sgs.war;
    cd $CATALINA_BASE
    mkdir -p conf
    
    bindir=$VCDIR/sgsaas/src/test/resources/bin
    sed -f ${bindir}/context.sed ${VCDIR}/webapp/src/main/webapp/META-INF/context.xml > $SGSWEBDIR/sgs/META-INF/context.xml
    awk -f ${bindir}/webxml.awk -v PILIST="$1" $SGSWEBDIR/sgs/WEB-INF/web.xml > $SGSWEBDIR/sgs/WEB-INF/web.xml.pi
    mv $SGSWEBDIR/sgs/WEB-INF/web.xml.pi $SGSWEBDIR/sgs/WEB-INF/web.xml
    echo <<EOF > $SGSWEBDIR/sgs/WEB-INF/classes/logging.properties 
handlers = org.apache.juli.FileHandler, java.util.logging.ConsoleHandler
############################################################
# Handler specific properties.
# Describes specific configuration info for Handlers.
############################################################
org.apache.juli.FileHandler.level = FINE
org.apache.juli.FileHandler.directory = /home/rob/utah/server/tc/aws/log
org.apache.juli.FileHandler.prefix = weblo.g
java.util.logging.ConsoleHandler.level = FINE
java.util.logging.ConsoleHandler.formatter = java.util.logging.OneLineFormatter
EOF

fi
. $HOME/bin/sgsCP.sh #classpath
printf "============\nSGSSelector using classpath as:\n"
echo $CLASSPATH | sed s/:/\\n/g

#     -Dorg.jooq.settings=/home/rob/utah/server/jooq-settings.xml 
dater=$(date +%Y%m%d-%H%M)
logfile=$CATALINA_BASE/tomcat.${dater}.log
CLASSPATH=${CLASSPATH}
cd $CATALINA_BASE
java -Dorg.jooq.settings=/home/rob/utah/server/jooq-settings.xml \
     edu.utah.camplab.server.SGSSelector /home/rob/utah/server/selector.properties \
      > $logfile 2>&1
