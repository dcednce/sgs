
{
  if (NR == 1) {
    template = 0
    split(PVLIST, pis,":")
    for ( i in pis ) {
      split(pis[i], userpass,",")
      unm = userpass[1]
      ups = userpass[2]
      ofiles[ups]="rsrc" ups
    }
    printf "\n"
  }
}
/dbname_template/{
  orig = $0  
  for ( i in pis ) {
    split(pis[i], userpass,",")
    unm = userpass[1]
    ups = userpass[2]
    gsub("dbname_template", unm, orig)
    if ( orig ~ /dbhost_template/ ) {
        gsub("dbhost_template", pgserver, orig)
    }
    if ( orig ~ /dbport_template/ ) {
        gsub("dbport_template", pgport, orig)
    }
    printf "%s\n", orig > ofiles[ups]
    orig = $0
  }
  template=1
}
/role_template/{
  orig = $0  
  for ( i in pis ) {
    split(pis[i], userpass,",")
    unm = userpass[1]
    ups = userpass[2]
    gsub("role_template", ups, orig)
    printf "%s\n", orig > ofiles[ups]
    orig = $0
  }
  template=1
}
/rolepwd_template/{
  orig = $0  
  for ( i in pis ) {
    split(pis[i], userpass,",")
    unm = userpass[1]
    ups = userpass[2]
    gsub("rolepwd_template", ups "_notnull", orig)
    printf "%s\n", orig > ofiles[ups]
    orig = $0
  }
  template=1
}
{
    if (! template) {
        for ( i in pis ) {
            split(pis[i], userpass, ",")
            printf "%s\n",$0 > ofiles[userpass[2]]
        }
    }
    else {
        template = 0
    }
}
END {
  pcmd = sprintf("<Context reloadable=\"true\">\n")
  system("echo '" pcmd "'")  # > context.xml
  for ( i in pis ) {
	   split(pis[i], userpass, ",")
    system("cat " ofiles[userpass[2]] ) ## " >> context.xml"
  }
  pcmd = sprintf("</Context>")
  system( "cat ${VCDIR}/webapp/src/main/webapp/META-INF/context-valve.xml")  ##  >> context.xml
  system( "echo '" pcmd "'")   ## >> context.xml
}
