package edu.utah.camplab.sgs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import jpsgcs.genio.GeneticDataSource;
import edu.utah.camplab.sgs.SharingRun;

public class AlleleSharing {
    public static final int SHARING_OPTION_SGS    = 0;
    public static final int SHARING_OPTION_SGShom = 1;
    public static final int SHARING_OPTION_HGS    = 2;

    public static float[] weightedPairedHGS(GeneticDataSource x) {
        return weightedPaired(x, 2);
    }

    public static float[] weightedPairedHomSGS(GeneticDataSource x) {
        return weightedPaired(x, 1);
    }

    public static float[] weightedPairedSGS(GeneticDataSource x) {
        return weightedPaired(x, 0);
    }

    public static float[] weightedPaired(GeneticDataSource x, int option) {
        return bothPairedAndWeighted(x, option).get(1);
    }

    public static float[] pairedHomSGS(GeneticDataSource x) {
        return paired(x, 1);
    }

    public static float[] pairedHGS(GeneticDataSource x) {
        return paired(x, 2);
    }

    public static float[] pairedSGS(GeneticDataSource x) {
        return paired(x, 0);
    }

    public static float[] paired(GeneticDataSource x, int option) {
        return bothPairedAndWeighted(x, option).get(0);
    }

    public static List<float[]> bothPairedAndWeighted(GeneticDataSource x, int option) {
        int[] probs = probands(x);
        List<float[]> tBoth = new ArrayList<float[]>();
        int nloci = x.nLoci();
        float[] tPaired = new float[nloci];
        float[] tWeighted = new float[nloci];

        float totalweight = 0.0f;
        int denominator = 0;

        for (int i = 0; i < probs.length; i++) {
            for (int j = i + 1; j < probs.length; j++) {
                denominator++;
                int meiosesij = (int) AlleleSharing.kinshipAsMeioses(x, probs[i], probs[j]);
                // a length in terms of markers
                float expectedRunLengthij = (1.0f * nloci) / meiosesij;
                int[] sharingVector = null; // first off this is the sharing vector
                switch (option) {
                case 0:
                    sharingVector = hetSharing(x, probs[i], probs[j]);
                    break;
                case 1:
                    sharingVector = homSharing(x, probs[i], probs[j]);
                    break;
                case 2:
                    sharingVector = homozygotes(x, probs[i], probs[j]);
                    break;
                }
                int[] lengthsij = runs(sharingVector, 2); // run lengths for this i,j

                // float w = (float) Math.pow(2.0, 1 / x.getKinshipCoeff(probs[i], probs[j]));
                // float w = 1.0f / (float)x.getKinshipCoeff(probs[i], probs[j]);
                float weightij = 1 / expectedRunLengthij;

                for (int k = 0; k < nloci; k++) {
                    tPaired[k] += lengthsij[k];
                    tWeighted[k] += weightij * lengthsij[k];

                }
                totalweight += weightij; // accumulate weight
            }
        }
        // normalization.
        for (int k = 0; k < nloci; k++) {
            tPaired[k] /= (1.0f * denominator); // An averaging of run lengths by number of pairs
            tWeighted[k] /= totalweight; // Normalizing the weighted values by total (proband) weight
        }
        tBoth.add(tPaired);
        tBoth.add(tWeighted);
        return tBoth;
    }

    public static float accumulatedPedigreeKinship(GeneticDataSource gds) {
        // Assuming single pedigree in gds
        int[] probs = probands(gds);
        float accumulatedKinship = 0.0f;
        for (int i = 0; i < probs.length; i++) {
            for (int j = i + 1; j < probs.length; j++) {
                accumulatedKinship += 1 / gds.getKinshipCoeff(probs[i], probs[j]);
            }
        }
        return accumulatedKinship;
    }

    public static int[] hetSharing(GeneticDataSource x) {
        return hetSharing(x, probands(x));
    }

    public static int[] hetSharing(GeneticDataSource x, List<SharingRun> r) {
        return hetSharing(x, probands(x), r);
    }

    public static int[] hetSharingExhaustive(GeneticDataSource x, List<SharingRun> r) {
        return hetSharingExhaustive(x, probands(x), r);
    }
    
    private static int[] hetSharing(GeneticDataSource x, int a, int b) {
        int[] pair = { a, b };
        return hetSharing(x, pair);
    }

    private static int[] hetSharing(GeneticDataSource x, int[] probs) {
        int[] s = new int[x.nLoci()];
        for (int i = 0; i < s.length; i++) {
            int[] c = new int[x.nAlleles(i)];

            for (int j = 0; j < probs.length; j++) {
                int a = x.getAllele(i, probs[j], 0);
                int b = x.getAllele(i, probs[j], 1);

                if (a < 0 || b < 0) { // does this need a while loop? presume -1
                    // as only neg. value?
                    for (int k = 0; k < c.length; k++) {
                        c[k]++;
                    }
                }
                else if (a == b) {
                    c[a]++;
                }
                else {
                    c[a]++;
                    c[b]++;
                }
            }

            s[i] = 0;
            for (int k = 0; k < c.length; k++) {
                if (s[i] < c[k]) {
                    s[i] = c[k];
                }
            }
        }

        return s;
    }

    private static int[] hetSharing(GeneticDataSource x, int[] probs, List<SharingRun> runList) {
        SharingRun sharingRun = null;
        int maxIndex = x.nLoci() - 1;
        boolean inRun = false;

	// TODO: pull this into stand-alone test, seen also in DropEval
        for (int locIndex = 0; locIndex <= maxIndex; locIndex++) {
            boolean allShare = true;
            int homozygousFound = -1;
            for (int j = 0; j < probs.length && allShare; j++) {
                int a = x.getAllele(locIndex, probs[j], 0);
                int b = x.getAllele(locIndex, probs[j], 1);
                if (a == b && a != -1) {
                    if (homozygousFound == -1) {
                        homozygousFound = a;
                    }
                    else {
                        if (homozygousFound != a) {
                            // found second homozygote; end of run
                            allShare = false;
                        }
                    }
                }
            }
            if (! allShare) {
                if (inRun) {
                    finishRun(sharingRun, locIndex-1, x);
                    inRun = false;
                    sharingRun = null;
                }
            }
            else { //All N sharing
                if (! inRun) {
                    sharingRun = startRun(locIndex, x);
                    runList.add(sharingRun);
                    inRun = true;
                }
            }
        }
        if (inRun) {
            sharingRun.setLastmarker(maxIndex);
            sharingRun.setEndbase(pullBasePosition(x.locusName(maxIndex)));
        }
        return new int[1]; // Just bogus! keeps the original interface, but why bother?
    }
    
    private static int[] hetSharingExhaustive(GeneticDataSource gds, int[] probs, List<SharingRun> runList) {
        int[] s = new int[gds.nLoci()];
        SharingRun sharingRun = null;
        int maxIndex = gds.nLoci() - 1;
        boolean inRun = false;
        for (int i = 0; i < s.length; i++) {
            int[] c = new int[gds.nAlleles(i)];

            for (int j = 0; j < probs.length; j++) {
                int a = gds.getAllele(i, probs[j], 0);
                int b = gds.getAllele(i, probs[j], 1);

                if (a < 0 || b < 0) { // does this need a while loop? presume -1
                    // as only neg. value?
                    for (int k = 0; k < c.length; k++) {
                        c[k]++;
                    }
                }
                else if (a == b) {
                    c[a]++;
                }
                else {
                    c[a]++;
                    c[b]++;
                }
            }

            s[i] = 0;
            for (int k = 0; k < c.length; k++) {
                if (s[i] < c[k]) {
                    s[i] = c[k];
                }
            }
            // I think this defines an end of run
            if (s[i] < probs.length) {
                if (inRun) {
                    finishRun(sharingRun, i-1, gds);
                    inRun = false;
                    sharingRun = null;
                }
            }
            else { //All N sharing
                if (! inRun) {
                    if (i < maxIndex ) {
                        sharingRun = startRun(i, gds);
                        runList.add(sharingRun);
                    }
                    inRun = true;
                }
            }
        }
        if (inRun) {
            sharingRun.setLastmarker(maxIndex);
            sharingRun.setEndbase(pullBasePosition(gds.locusName(maxIndex)));
        }
        return s;
    }

    private static void finishRun(SharingRun currentRun, int lastIndex, GeneticDataSource gds) {
        if (currentRun == null)
            return;
        if (lastIndex > currentRun.getLastmarker()) {
            currentRun.setLastmarker(lastIndex);
            currentRun.setEndbase(pullBasePosition(gds.locusName(lastIndex)));
        }
    }

    private static SharingRun startRun(int start, GeneticDataSource gds) {
        SharingRun sr = new SharingRun();
        sr.setFirstmarker(start);
        sr.setProbands(pullProbands(gds));
        sr.setLastmarker(start);
        sr.setStartbase(pullBasePosition(gds.locusName(start)));
        sr.setEndbase(sr.getStartbase()); // Don't know actual end yet
        return sr;
    }

    private static int pullBasePosition(String formattedName) {
        String[] nameParts = formattedName.split("\\|");
        int baseIndex = 1; //For the base position
        if (nameParts.length < 2) {
            //logger.error("What sort of markers is this? %s", formattedName);
            baseIndex = 0; //This better be a number
        }
        return Integer.parseInt(nameParts[baseIndex]);
    }
    
    public static int[] homSharing(GeneticDataSource x, int a, int b) {
        int[] pair = { a, b };
        return homSharing(x, pair);
    }

    public static int[] homSharing(GeneticDataSource x) {
        return homSharing(x, probands(x));
    }

    public static int[] homSharing(GeneticDataSource x, int[] probs) {
        int[] s = new int[x.nLoci()];

        for (int i = 0; i < s.length; i++) {
            int[] c = new int[x.nAlleles(i)];

            for (int j = 0; j < probs.length; j++) {
                int a = x.getAllele(i, probs[j], 0);
                int b = x.getAllele(i, probs[j], 1);

                if (a < 0 || b < 0) {
                    for (int k = 0; k < c.length; k++) {
                        c[k]++;
                    }
                }
                else if (a == b) {
                    c[a]++;
                }
            }

            s[i] = 0;
            for (int k = 0; k < c.length; k++) {
                if (s[i] < c[k]) {
                    s[i] = c[k];
                }
            }
        }

        return s;
    }

    public static int[] homozygotes(GeneticDataSource x, int a, int b) {
        int[] pair = { a, b };
        return homozygotes(x, pair);
    }

    public static int[] homozygotes(GeneticDataSource x) {
        return homozygotes(x, probands(x));
    }

    public static int[] homozygotes(GeneticDataSource x, int[] probs) {
        int[] s = new int[x.nLoci()];

        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < probs.length; j++) {
                int a = x.getAllele(i, probs[j], 0);
                int b = x.getAllele(i, probs[j], 1);
                if (a < 0 || b < 0 || a == b) {
                    s[i]++;
                }
            }
        }

        return s;
    }

    public static int[] probands(GeneticDataSource x) {
        int[] probs = new int[x.nProbands()];
        int np = 0;
        for (int i = 0; i < x.nIndividuals(); i++) {
            if (x.proband(i) == 1) {
                probs[np++] = i;
            }
        }
        return probs;
    }

    public static float pedigreeWeight(GeneticDataSource gds) {
        int[] probands = AlleleSharing.probands(gds);
        float weight = 0.0f;
        // a length in terms of markers
        int nloci = gds.nLoci();
        for (int i = 0; i < probands.length; i++) {
            for (int j = i + 1; j < probands.length; j++) {
                int meiosesij = (int) AlleleSharing.kinshipAsMeioses(gds, probands[i], probands[j]);
                float expectedRunLengthij = (1.0f * nloci) / meiosesij;
                weight += 1.0f / expectedRunLengthij;
            }
        }
        return weight;
    }

    public static int[] runs(int[] s, int n) {
        int[] u = new int[s.length];
        if (s[0] >= n) {
            u[0] = 1;
        }
        for (int i = 1; i < s.length; i++) {
            if (s[i] >= n) {
                u[i] = u[i - 1] + 1;
            }
        }

        int[] v = new int[s.length];
        if (s[s.length - 1] >= n) {
            v[s.length - 1] = 1;
        }
        for (int i = s.length - 2; i >= 0; i--) {
            if (s[i] >= n) {
                v[i] = v[i + 1] + 1;
            }
        }

        for (int i = 0; i < s.length; i++) {
            u[i] = u[i] + v[i] - 1;
            if (u[i] < 0) {
                u[i] = 0;
            }
        }

        return u;
    }

    public static int max(int[] x) {
        int m = 0;
        for (int i = 0; i < x.length; i++) {
            if (m < x[i]) {
                m = x[i];
            }
        }
        return m;
    }

    public static float max(float[] x) {
        float m = -Float.MAX_VALUE;
        for (int i = 0; i < x.length; i++) {
            if (m < x[i]) {
                m = x[i];
            }
        }
        return m;
    }

    public static Number max(Number[] x) {
        Float t = Float.MIN_VALUE;
        for (int i = 0; i < x.length; i++) {
            Float c = ((Float) x[i]).floatValue();
            if (t.compareTo(c) < 0) {
                t = c;
            }
        }
        return t;
    }

    public static HashMap<String, int[]> reportPairSGSContributions(GeneticDataSource gds) {
        HashMap<String, int[]> contributions = new HashMap<String, int[]>();
        int[] probs = probands(gds);
        // Only if weighted
        // int[] rank = Pedigrees.canonicalRank(gds);

        for (int i = 0; i < probs.length; i++) {
            for (int j = i + 1; j < probs.length; j++) {
                int[] hetshares = hetSharing(gds, probs[i], probs[j]);
                int[] shareRuns = runs(hetshares, 2);
                int pairmeioses = (int) AlleleSharing.kinshipAsMeioses(gds, probs[i], probs[j]);
                contributions.put(String.format("%s:%s@%d", gds.individualName(probs[i]), gds.individualName(probs[j]), pairmeioses), shareRuns);
            }
        }
        return contributions;
    }

    public static long kinshipAsMeioses(GeneticDataSource gds, int probandi, int probandj) {
        // d = log(psi)/log(1/2)
        double numerator = Math.log10(gds.getKinshipCoeff(probandi, probandj));
        double denominator = Math.log10(1.0 / 2.0);
        return (long) (Math.floor(numerator / denominator));
    }

    static public void printGenotypes(int idx, int locx, int span, GeneticDataSource lif) {
        System.out.print(String.format("\nproband idx=%7d - ", idx));
        for (int i = locx; i <= span; i++) {
            System.out.print(String.format("%6d:%d/%d ",
                                             i,
                                             lif.getAllele(i, idx, 0),
                                             lif.getAllele(i, idx, 1) ));
        }
    }
    public static String pullProbands(GeneticDataSource gd){
      TreeSet<String> ptree = new TreeSet<>();
      StringBuilder stringBuilder = new StringBuilder();
      for (int p : probands(gd)) {
        ptree.add(""+ gd.individualName(p));
      }
      String comma = "";
      for(String s : ptree) {
        stringBuilder.append(comma);
        comma = ",";
        stringBuilder.append(s);
      }
      return stringBuilder.toString();
    }
}
