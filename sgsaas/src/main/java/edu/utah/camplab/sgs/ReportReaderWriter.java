package edu.utah.camplab.sgs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.utah.camplab.tools.LinkageIdSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageLocus;

public class ReportReaderWriter {
    private static final Logger logger = LoggerFactory.getLogger(AbstractSGSRun.class);

    public static void WriteReport(File pvalFile, 
                                   File parfile, 
                                   File pedfile, 
                                   LinkageIdSet probandSet,
                                   List<SharingRun> sharingRuns,
                                   int simsCompleted,
                                   int meioses,
                                   LinkageInterface linkageInterface) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(pvalFile))) {
            
            // Write the report header      
            StringBuffer header = new StringBuffer();
            header.append(String.format("##Simulations: %d\n", simsCompleted));
            header.append(String.format("# Ped file: %s\n", pedfile.getAbsolutePath()));
            header.append(String.format("# Par file: %s\n", parfile.getAbsolutePath()));
            header.append(String.format("# Probands: %s\n# Meioses: %d\n", probandSet.asCSV(), meioses));
            header.append(AbstractSGSRun.addManifest());
            header.append(String.format("# %s\t%s\t%s\t%s\t%s\t%s\t%s\n", "missing pieces", "Span", "StartBP", "EndBP", "LT", "EQ", "GT"));
    
            writer.write(header.toString());
            
            int sindex = 0;
            for (SharingRun run : sharingRuns) {
                StringBuffer reportLine = new StringBuffer();
                
                int startLocus = run.getFirstmarker();
                int endLocus   = run.getLastmarker();
                
                String[] startNameParts = getLocusNameParts(startLocus, linkageInterface);
                String[] endNameParts = getLocusNameParts(endLocus, linkageInterface);
                
                int startBase = Integer.valueOf(startNameParts[1]);
                int endBase   = Integer.valueOf(endNameParts[1]);
                String chromosome = startNameParts[2];
                
                reportLine.append(String.format("%d\t%s\t", sindex++, chromosome));
                                
                reportLine.append(String.format("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
                                                run.getFirstmarker(),
                                                run.getLastmarker(),
                                                run.getLastmarker() - run.getFirstmarker() + 1,
                                                startBase,
                                                endBase,
                                                run.getEventsLess(),
                                                run.getEventsEqual(),
                                                run.getEventsGreater()));
                writer.write(reportLine.toString());
            }
            
            writer.flush();            
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
    }       
    
    public static String[] getLocusNameParts(int index, LinkageInterface linkageInterface) {
        // TODO: do we need to cache the parts?
        return getLocusNameParts(linkageInterface.raw().getParameterData().getLoci()[index]);
    }

    public static String[] getLocusNameParts(LinkageLocus locus) {
        // TODO: do we need to cache the parts?
        String[] nameParts = locus.locName().split("\\|");
        if (nameParts.length < 3) {
            String[] fullname = new String[3];
            fullname[0] = nameParts[0];
            fullname[1] = "0";
            fullname[2] = "nil";
            if (nameParts.length == 2) {
                fullname[1] = nameParts[1];
            }
            nameParts = fullname;
        }
        return nameParts;
    }
    
    public static List<SharingRun> LoadSharingRuns(File pvalfile) {
        List<SharingRun> runs = new ArrayList<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(pvalfile));) {
                String line = reader.readLine();
                while (line != null) {
                    if ( ! line.startsWith("#")) {
                        runs.add(parseSharingRunLine(line));
                    }
                    line = reader.readLine();
                }
            }
        catch (IOException ioe) {
            System.err.println("Cannot restart from file: " + pvalfile.getName());
            ioe.printStackTrace();
            System.exit(2);
        }

        return runs;
    }
    
    private static SharingRun parseSharingRunLine(String line) {
        SharingRun run = new SharingRun();
        String[] parts    = line.split("\\s+");
        if (parts.length != 10) {
            String emsg ="Malformed .pval line: " +  line;
            logger.error("{}", emsg);
            throw new RuntimeException (emsg);
        }
        run.setFirstmarker(Integer.valueOf(parts[2])); 
        run.setLastmarker(Integer.valueOf(parts[3]));
        run.setStartbase(Integer.valueOf(parts[5]));
        run.setEndbase(Integer.valueOf(parts[6]));
	run.setEventsLess(Long.valueOf(parts[7]));
        run.setEventsEqual(Long.valueOf(parts[8]));
	run.setEventsGreater(Long.valueOf(parts[9]));
        return run;
    }
}
