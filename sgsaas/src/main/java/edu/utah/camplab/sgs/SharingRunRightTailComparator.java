package edu.utah.camplab.sgs;

import java.util.Comparator;

public class SharingRunRightTailComparator implements Comparator<SharingRun> {
    private SharingRun srun = null;

    public SharingRunRightTailComparator() {
        ;
    }

    public SharingRunRightTailComparator(SharingRun run) {
        srun = run;
    }

    public int compare(SharingRun left, SharingRun right) {
        if (left == null && right != null) {
            return -1;
        }
        if (left != null && right == null) {
            return 1;
        }
        if (left == null && right == null) {
            return 0;
        }
        long leftTally = left.getEventsEqual() + left.getEventsGreater();
        long rightTally = right.getEventsEqual() + right.getEventsGreater();
        return (int)(leftTally - rightTally);
    }

    @Override
    public boolean equals(Object other) {
        if(srun == null && other == null) {
            return true;
        }
        if (srun == null  || other == null) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (other instanceof SharingRun) {
            SharingRun otherRun = (SharingRun)other;
            return srun.getEventsLess() == otherRun.getEventsLess()
                && srun.getEventsEqual() == otherRun.getEventsEqual()
                && srun.getEventsGreater() == otherRun.getEventsGreater();
        }
        else {
            return false;
        }
    }
}
