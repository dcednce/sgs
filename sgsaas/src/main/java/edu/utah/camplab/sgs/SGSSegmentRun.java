package edu.utah.camplab.sgs;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import jpsgcs.linkage.LinkageInterface;
import edu.utah.camplab.sgs.AlleleSharing;

public class SGSSegmentRun /* extends SGSWithError */{

    public int[][]   sharingRuns              = null;
    public int[]     max                      = null;
    public int[][]   runCompareCounts         = null;
    public int[]     maxCompareCounts         = null;
    // private int segmentID = -1;

    private float[]  sgsPaired                = null;
    private float[]  sgsPairedWeighted        = null;
    private int[]    pvalCountsPaired         = null;
    private int[]    pvalCountsPairedWeighted = null;

    public SGSSegmentRun(LinkageInterface x, Params params) {

        int probandCount = x.nProbands();
        int markerCount = x.nLoci();
        int[] hetshares = null;

        // if (params.isPaired() && params.isWeighted()) {
        //     List<double[]> bothVectors = AlleleSharing.bothPairedAndWeighted(x, 0);
        //     sgsPaired = bothVectors.get(0);
        //     sgsPairedWeighted = bothVectors.get(1);
        //     pvalCountsPaired = new int[markerCount];
        //     pvalCountsPairedWeighted = new int[markerCount];
        // }
        // else {
	if (params.isPaired()) {
	    float[] t = AlleleSharing.pairedSGS(x);
	    sgsPaired = new float[t.length];
	    // float to double
	    for (int i = 0; i < t.length; i++) {
		sgsPaired[i] = t[i];
		// TODO: make this a test of "both"
		// if ((Math.abs(sgsPaired[i] - t[i]) > 0.00001)) {
		// System.out.println(String.format("%d: both: %8.4f, single %8.4f",
		// i,
		// sgsPaired[i], t[i]));
		// System.exit(2);
	    }
	}
	pvalCountsPaired = new int[markerCount];

	if (params.isWeighted()) {
	    sgsPairedWeighted = AlleleSharing.weightedPairedSGS(x);
	    // TODO: make this a test of "both"
	    // double[] tsgsPairedWeighted =
	    // AlleleSharing.weightedPairedSGS(x);
	    // for (int p = 0; p < hetshares.length; p++) {
	    // if (!(tsgsPairedWeighted[p] == sgsPairedWeighted[p])) {
	    // System.exit(2);
	    // }
	    // }
	    //
	    pvalCountsPairedWeighted = new int[markerCount];
	}
        /* } */
        int nSharingTypes = params.get_sharingTypes();
        // int errorTypes = 0;

        boolean useErrorModel = params.getErrorModelStatus();
        int[][] error_r = null;

        if (nSharingTypes > 0) {
            sharingRuns = new int[nSharingTypes][markerCount];
            hetshares = AlleleSharing.hetSharing(x);
            for (int i = 0; i < nSharingTypes; i++) {
                sharingRuns[i] = AlleleSharing.runs(hetshares, probandCount - i);
            }
        }
        if (useErrorModel && nSharingTypes > 0) {
            error_r = new int[nSharingTypes][hetshares.length];
            // sharingTypes = errorTypes;
            nSharingTypes = 1;
            for (int j = 0; j < nSharingTypes; j++) {
                for (int i = 0; i < sharingRuns[0].length; i++) {
                    // TODO: just point error_r at sharingRuns??
                    error_r[j][i] = sharingRuns[j][i];
                }
            }
        }

        int analysis_type = params.get_analysisType();
        if (analysis_type == 1 || analysis_type == 3) {
            // Keep max run only
            if (nSharingTypes == 0) {
                return;
            }
            max = new int[nSharingTypes];
            for (int i = 0; i < nSharingTypes; i++) {
                // max[i] = AlleleSharing.max(error_r[i]);
                max[i] = AlleleSharing.max(sharingRuns[i]);
            }
        }
        if ((analysis_type == 2 || analysis_type == 3) && useErrorModel) {
            // Keep all marker run lengths
            // sharingRuns = error_r;
            error_r = sharingRuns;
        }

        // the few, the chosen
        // if (analysis_type == 4 && nSharingTypes > 0) {
        // // Keep all marker run lengths for specific marker list
        // sharingRuns = new int[nSharingTypes][params.get_markerlst().size()];
        // for (int j = 0; j < params.get_markerlst().size(); j++) {
        // for (int i = 0; i < nSharingTypes; i++) {
        // sharingRuns[i][j];
        // }
        // }
        // }
    }

    public void compareRuns(SGSSegmentRun compareTo) {
        if (sharingRuns != null) {
            if (runCompareCounts == null) {
                runCompareCounts = new int[sharingRuns.length][sharingRuns[0].length];
            }
            for (int i = 0; i < sharingRuns.length; i++) {
                runCompare(sharingRuns[i], compareTo.sharingRuns[i], runCompareCounts[i]);
            }
        }

        if (max != null) {
            if (maxCompareCounts == null) {
                maxCompareCounts = new int[max.length];
            }
            for (int i = 0; i < max.length; i++) {
                if (max[i] <= compareTo.max[i]) {
                    maxCompareCounts[i]++;
                }
            }
        }
        if (sgsPaired != null) {
            for (int i = 0; i < sgsPaired.length; i++) {
                if (sgsPaired[i] <= compareTo.sgsPaired[i]) {
                    pvalCountsPaired[i]++;
                }
            }
        }
        if (sgsPairedWeighted != null) {
            for (int i = 0; i < sgsPairedWeighted.length; i++) {
                if (sgsPairedWeighted[i] <= compareTo.sgsPairedWeighted[i]) {
                    pvalCountsPairedWeighted[i]++;
                }
            }
        }

    }

    public void getPeaks(List<Peak> p, int sharingIndex) {
        // A peak is just a run of markers > 1. The run's nominal significance
        // is the smallest p-value
        // amongst the markers in the run.
        int currentPeakLength = 0;
        int minPeakCount = -1;
        int endPeakIndex = 0;
        int[] runlet = sharingRuns[sharingIndex];
        int[] compareCounts = runCompareCounts[sharingIndex];
        for (int i = 0; i < runlet.length; i++) {
            int runlength = runlet[i];
            int count = compareCounts[i];
            if (currentPeakLength > 0) {
                if (count < minPeakCount) {
                    minPeakCount = count;
                }
                if (i == endPeakIndex) {
                    Peak sPeak = new Peak(minPeakCount);
                    p.add(sPeak);
                    currentPeakLength = 0;
                }
            }
            else if (runlength > 1) {
                currentPeakLength = runlength;
                minPeakCount = compareCounts[i];
                endPeakIndex = i + runlength - 1;
            }
        }
    }

    public void log(LinkageInterface linkageInterface, Params params, String filename) {
        String fullFileName = filename;
        int nsims = params.get_nsims();

        FileWriter fw = null;
        try {
            fw = new FileWriter(fullFileName);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        PrintWriter pwLog = new PrintWriter(new BufferedWriter(fw), true);

        pwLog.write(params.makeHeader(linkageInterface));

        // Write results
        for (int i = 0; i < linkageInterface.nLoci(); i++) {
            StringBuffer reportLine = new StringBuffer();
            int markerIndex = i;
            if (params.get_analysisType() == 4) {
                markerIndex = params.get_markerlst().get(i);
            }
            String locName = linkageInterface.locusName(markerIndex);
            reportLine.append(locName);

            for (int j = 0; j < params.get_sharingTypes(); j++) {
                reportLine.append("\t" + sharingRuns[j][i]);
                if (params.isPvalued()) {
                    reportLine.append(String.format("\t%7.4f", runCompareCounts[j][i] / (1.0 * nsims)));
                }
            }
            if (params.isPaired()) {
                reportLine.append(String.format("\t%8.3f", sgsPaired[i]));
                if (params.isPvalued()) {
                    reportLine.append(String.format("\t%8.6f", pvalCountsPaired[i] / (1.0 * nsims)));
                }
            }
            if (params.isWeighted()) {
                reportLine.append(String.format("\t%8.3f", sgsPairedWeighted[i]));
                if (params.isPvalued()) {
                    reportLine.append(String.format("\t%8.6f", pvalCountsPairedWeighted[i] / (1.0 * nsims)));
                }
            }
            pwLog.write(reportLine.toString() + "\n");
        }

        if (max != null) {
            // Log the max run lengths.
            StringBuffer maxStr = new StringBuffer("Max run:");
            for (int i = 0; i < max.length; i++) {
                maxStr.append("\t");
                maxStr.append(max[i]);
                maxStr.append(" (");
                float pval = maxCompareCounts[i] / nsims;
                maxStr.append(pval);
                maxStr.append(")");

            }
            // outStr.append("\t");
            // outStr.append(pvaluableCounts[i]/(1.0*nsims)
            pwLog.write(maxStr.toString() + "\n");
        }
        pwLog.close();
    }

    public String print(Writer w, int i) throws IOException {

        return (String.format("\t%8.6f\t%8.6f", sgsPaired[i], sgsPairedWeighted == null ? 0.0f : sgsPairedWeighted[i]));

    }

    private void runCompare(int[] r, int[] c, int[] count) {
        for (int i = 0; i < r.length; i++) {
            if (r[i] <= c[i]) {
                count[i]++;
            }
        }
    }
}
