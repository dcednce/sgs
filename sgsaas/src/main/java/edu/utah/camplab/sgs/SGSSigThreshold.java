package edu.utah.camplab.sgs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.utah.camplab.jx.SegmentBlock;
import edu.utah.camplab.jx.AbstractPayload;

import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import jpsgcs.pedapps.ThreadedGeneDropper;

public class SGSSigThreshold extends AbstractSGSRun {

    final static Logger logger = LoggerFactory.getLogger(SGSSigThreshold.class);

    public static int DEFAULT_BLOCKSIZE = 1000;
    public static int DEFAULT_NULLSETSIZE = 2000;
    public static int DEFAULT_SUPERBLOCK = 100000;

    private int coresCount;
    private int blockSize;
    private int superBlock;
    private int observedCount;  // number of simulations to use as observed

    private long startTick = System.currentTimeMillis();
    private Writer writer = null;

    private ArrayList<DropEvaluation> nullSet = null;
    private ThreadedGeneDropper dropper = null;

    private OptionSpec<Integer> nullSimsSpec;
    private OptionSpec<Integer> superThresholdSpec;
    private OptionSpec<Integer> blockSpec;
    
    public SGSSigThreshold() {
        super();
        nullSimsSpec = op.acceptsAll(Arrays.asList("g", "nullsims"), "number of simulations to use as null observed").withOptionalArg().ofType(Integer.class).defaultsTo(1000);
        superThresholdSpec = op.accepts( "superBlock", "max simulations per iteration").withOptionalArg().ofType(Integer.class).defaultsTo(SGSSigThreshold.DEFAULT_SUPERBLOCK);
        blockSpec = op.accepts("block", "size of work block to multithread").withOptionalArg().ofType(Integer.class).defaultsTo(1000);
    }
    
  @Override public UUID getPedigreeId() {return null; }
  @Override public void setPedigreeId(UUID id) {  }
  @Override public UUID getMarkersetId() {return null;}
  @Override public void setMarkersetId(UUID id) {}

  @Override
  protected void init(OptionSet opSet) throws Exception {
        super.init(opSet);
        observedCount = SGSSigThreshold.DEFAULT_NULLSETSIZE;;
        blockSize = SGSSigThreshold.DEFAULT_BLOCKSIZE;
        superBlock = SGSSigThreshold.DEFAULT_SUPERBLOCK;

        if (opSet.has(nullSimsSpec)) {
            observedCount = nullSimsSpec.value(opSet);
        }
        if (opSet.has(superThresholdSpec)) {
            superBlock = superThresholdSpec.value(opSet);
        }
        if (opSet.has(blockSpec)) {
            blockSize = blockSpec.value(opSet);
        }
        
        coresCount = Runtime.getRuntime().availableProcessors();
    }
    
    public void init(String[] args) throws Exception {
        init(op.parse(args));
    }

  public void buildLinkageDataSet() {
    ;
  }

    @Override
    public void run() throws Exception {
        logger.debug(" {}: init LI", getTag());
        establishNull();

        long simsLeft = getSimulationCount();
        while (simsLeft > 0) {
            logger.info("{}: {} sims to go", getTag(), simsLeft);
            long nextBlockSize = simsLeft > superBlock ? superBlock : simsLeft;
            simsLeft -= nextBlockSize;
            evaluate(nextBlockSize, simsLeft);
        }
        reportThreshold();
    }

    private void establishNull() {
        long start = System.currentTimeMillis();
        nullSet = simulate(observedCount);
        logger.info("Built observed sims in {} s", ( System.currentTimeMillis() -start )/1000);
    }

    private ArrayList<DropEvaluation> simulate(int maxSimulations) {
        ArrayList<DropEvaluation> evals = new ArrayList<>(maxSimulations);
        long startTime = System.currentTimeMillis();
        long evalTime = 0;
        while (maxSimulations-- > 0) {
            drop();
            long dtime = System.nanoTime();
            DropEvaluation eval = new DropEvaluation(getLinkageInterface(), getMinimumSegmentSize());
            evalTime += (System.nanoTime() - dtime);
            evals.add(eval);
        }
        logger.debug("null set size {} generated in {} ms", evals.size(), System.currentTimeMillis() - startTime);
        logger.debug("the evaluations took {} ms", evalTime);
        return evals;
    }

    private void evaluate( long block, long simsLeft) {
        long simsDone = getSimulationCount() - simsLeft;
        while (block-- > 0) {
            drop();
            EvaluationExecutor ee = new EvaluationExecutor(nullSet,
                                                           getLinkageInterface(),
                                                           0,
                                                           observedCount,
                                                           observedCount / Runtime.getRuntime().availableProcessors(),
                                                           getMinimumSegmentSize());
            ee.tally();
        }
        
        int zeroCount = 0;
        int minspan = getMinimumSegmentSize();
        for (DropEvaluation dt : nullSet) {
            for (SharingRun srun : dt.getSharingRunList()) {
                if ((srun.runLength() >= minspan && (srun.getEventsEqual() + srun.getEventsGreater() == 0))) {
                    zeroCount++;
                }
            }
        }

        long duration = System.currentTimeMillis() - startTick;
        write(String.format("After %d simulations (%6.2f hours) found %d zero-count segments over %d null-observed\n",
                            simsDone,
                            (1.0f * duration)/3600000,
                            zeroCount,
                            nullSet.size()));
        flush();
    }
            
    private void reportThreshold() {
        ArrayList<SharingRun> runList = new ArrayList<>();
        
        for (DropEvaluation de : nullSet) {
            reportNullRuns(de);
            List<SharingRun> evalRuns = filterTooSmall(de);
            Collections.sort(evalRuns, new SharingRunRightTailComparator());
            runList.addAll(evalRuns.subList(0, observedCount <= evalRuns.size() ? observedCount : evalRuns.size()));
            //write the null evaluation
            Collections.sort(runList, new SharingRunRightTailComparator());
            if (runList.size() > observedCount) {
                runList.subList(observedCount, runList.size()).clear();
            }
        }

        Collections.sort(runList, new SharingRunRightTailComparator());
        nonZeroCheck(runList, "Final top N");
        int ptiles[] = {20,10,5,2,1};
        ArrayList<SharingRun> ptileRuns  = new ArrayList<>();
        for (int ptile : ptiles) {
            int ptileIndex = (int)(Math.round(observedCount * (ptile * 0.01)));
            ptileRuns.add(runList.get(ptileIndex));
        }
                                           
        write(String.format("We have %d stretches from %d nulls, %d simulations.\n",
                            runList.size(),
                            observedCount,
                            getSimulationCount()));
        for (int i = 0; i < ptiles.length; i++) {
            SharingRun tileRun = ptileRuns.get(i);
            int tileReported = 100 - ptiles[i];
                
            write(String.format("%dth percentile p-value is %5.3e. Run start %d: end %d: length %d. Counts LT: %d, EQ: %d, GT: %d\n",
                                tileReported,
                                (tileRun.getEventsGreater() + tileRun.getEventsEqual())/(1.0f * getSimulationCount()),
                                tileRun.getFirstmarker(),
                                tileRun.getLastmarker(),
                                tileRun.runLength(),
                                tileRun.getEventsLess(),
                                tileRun.getEventsEqual(),
                                tileRun.getEventsGreater()));
        }
        flush();
        reportRuns(runList); // list sorted by eventsGreaterEqual
    }

    private void reportNullRuns(DropEvaluation eval) {
        try {
            Writer nullWriter = getNullWriter();
            //eval.writePvalData(nullWriter, getLinkageInterface(), null);
        }
        catch (Exception ioe) {
            logger.error("Failed to write one null");
        }
    }

    private Writer getNullWriter() {
        Writer writer = null;
        String repname = getJsonDir().getName();
        try {
            File nullFile = File.createTempFile(repname + ".", ".sst", getJsonDir().getParentFile());
            writer = new BufferedWriter(new FileWriter(nullFile));
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(2);
        }
        return writer;
    }

    private void reportRuns(ArrayList<SharingRun> rundefs)  {
        writeln(String.format("\n# Listing top %d runs", rundefs.size()));
        writeln(String.format("# %7s %7s %7s %7s %7s", "LT", "EQ", "GT", "startbp", "markers"));
        for (SharingRun srun : rundefs) {
            writeln(String.format("  %7d %7d %7d %7d %7d",
                                  srun.getEventsLess(),
                                  srun.getEventsEqual(),
                                  srun.getEventsGreater(),
                                  srun.getFirstmarker(),
                                  srun.runLength()));
        }
        flush();
    }

    private void nonZeroCheck(List<SharingRun> runList, String context) {
        boolean foundLess = false;
        boolean foundGreater = false;
        for (int i = 0; i < runList.size() ; i++) {
            SharingRun srun = runList.get(i);
            if (srun.getEventsLess() > 0)
                foundLess = true;
            if (srun.getEventsGreater() > 0)
                foundGreater = true;
            if ( (srun.getEventsEqual() + srun.getEventsGreater()) > 0 ) {
                write(String.format("%s-%s: First non-zero at element %d\n", getTag(), context, i));
                break;
            }
        }
        if ( ! (foundLess || foundGreater ) ) {
            write(String.format("%s-%s: Nothing but zeros\n", getTag(), context));
        }
        flush();
    }

    private void drop() {
        if (dropper == null) {
            dropper = new ThreadedGeneDropper(getLinkageInterface(),
                                              getLdModel(),
                                              new Random(System.currentTimeMillis()),
                                              Runtime.getRuntime().availableProcessors());
        }
        dropper.geneDrop();
    }

    private List<SharingRun> filterTooSmall(DropEvaluation dropeval) {
        ArrayList<SharingRun> filtered = new ArrayList<SharingRun>();
        for (SharingRun srun : dropeval.getSharingRunList()) {
            if (srun.runLength() >= getMinimumSegmentSize()) {
                filtered.add(srun);
            }
        }
        return filtered;
    }

    // private void reportPreamble() {
    //     writeln("# Ped file:    " + getPedFile().getAbsolutePath());
    //     writeln("# Par file:    " + getParFile().getAbsolutePath());
    //     writeln("# Probands: " + probandListAsCsv());
    //     for (String jar : AbstractSGSRun.addManifest()) {
    //     	writeln(jar);
    //     }
    //     writeln("# Start time:  " + getDateTime());
    //     flush();
    // }
    
    private Writer getWriter() {
        if (writer == null) {
            try {
                writer = new BufferedWriter(new FileWriter(getJsonDir()));
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
                System.exit(2);
            }
        }
        return writer;
    }

    private void flush() {
        write(null);
    }

    private void writeln(String s) {
        write(s + "\n");
    }

    private void write(String s) {
        try {
            if (s != null) {
                getWriter().append(s);
            }
            else {
                getWriter().flush();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(2);
        }
    }

    public SegmentBlock buildSegmentBlock() {
        return null;
    }


}
