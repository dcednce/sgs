package edu.utah.camplab.sgs;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import edu.utah.camplab.tools.LinkageIdSet;
import jpsgcs.linkage.LinkageInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MATupleInitExecutor extends RecursiveTask<HashMap<LinkageIdSet, DropEvaluation>> {
    private static final long serialVersionUID = 6853821412058945874L;
    final static Logger logger = LoggerFactory.getLogger(MATupleInitExecutor.class);

    final List<LinkageIdSet> probandSets;
    final int startIndex;
    final int endIndex;
    final int fit;
    final File outDir;
    final int minSegment;
    public MATupleInitExecutor(List<LinkageIdSet> pbIds,
                               int start,
                               int end,
                               int chunk,
                               File outDir,
                               int minseg) {
        probandSets = pbIds;
        startIndex = start;
        endIndex = end;
        fit = chunk;
        this.outDir = outDir;
        minSegment = minseg;
    }
    
    @Override
    public HashMap<LinkageIdSet, DropEvaluation> compute() {
        HashMap<LinkageIdSet, DropEvaluation> MATmap = new HashMap<>();
        if (endIndex - startIndex <= fit) {
            UUID tag = UUID.randomUUID();
            LITuple workingTuple = LITuple.getAvailableLI();
            logger.info("{}: MAT init from {} to {}", tag, startIndex, endIndex);
            long loopstart = System.currentTimeMillis();
            for (LinkageIdSet lis : probandSets.subList(startIndex, endIndex)) {
                DropEvaluation evaluator = null;
                resetProbands(lis, workingTuple.getLinkageInterface());
                evaluator = SGSMultiAssess.createDropEval(lis, outDir, workingTuple.getLinkageInterface(), minSegment);
                MATmap.put(lis, evaluator);
            }
            logger.debug("{}: init loop for block of size {} took {} ms", tag, endIndex-startIndex, System.currentTimeMillis() - loopstart);
            LITuple.setAvailableLI(workingTuple);
        }
        else {
            int half = (endIndex - startIndex)/2;
            MATupleInitExecutor nextExec = new MATupleInitExecutor(probandSets, startIndex, startIndex+half, fit, outDir, minSegment);
            MATupleInitExecutor futureExec = new MATupleInitExecutor(probandSets, startIndex+half, endIndex, fit, outDir, minSegment);

            nextExec.fork();
            HashMap<LinkageIdSet, DropEvaluation> futureMATs = futureExec.compute();
            HashMap<LinkageIdSet, DropEvaluation> nextMATs = nextExec.join();
            MATmap.putAll(futureMATs);
            MATmap.putAll(nextMATs);
        }
        return MATmap;
    }

    public HashMap<LinkageIdSet, DropEvaluation> generate() {
        long time = System.currentTimeMillis();
        HashMap<LinkageIdSet, DropEvaluation> result =  ForkJoinPool.commonPool().invoke(this);
        logger.info("ma tuple generation across all {} subsets took {} ms", result.size(), System.currentTimeMillis() - time);
        return result;
    }
    
    private void resetProbands(LinkageIdSet probandSet, LinkageInterface li) {
        li.raw().getPedigreeData().resetProbands(probandSet.asList());
        li.setProbandsCounted(probandSet.asList().size());
    }
}
