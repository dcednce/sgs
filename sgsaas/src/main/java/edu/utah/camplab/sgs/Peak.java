package edu.utah.camplab.sgs;

public class Peak implements Comparable<Object> {
    private int simcount = 0;

    public Peak(int s) {
        simcount = s;
    }

    @Override
    public int compareTo(Object sPeak) {
        int sCount = ((Peak) sPeak).getSimCount();
        return this.simcount - sCount;
    }

    public int getSimCount() {
        return simcount;
    }
}
