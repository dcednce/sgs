package edu.utah.camplab.sgs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import jpsgcs.linkage.LinkageInterface;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.InputFormatter;

public class Params {
    String             pedfile_name        = null;
    String             parfile_name        = null;
    int                nsims               = 1000;
    String             ldfile_name         = null;
    // -1 = max run only
    // -2 = point by point
    // -3 = point by point and max run
    // list of marker indices to assess point by point
    int                analysis_type       = 2;
    int                nsig_sims           = 100;
    ArrayList<Integer> analysis_markers    = null;
    String             outfile             = "SGSPValue";
    // 1 = nominal significance for observed
    // 2 = nominal significance + null significance
    // 3 = nominal significance + genome/chr-wide thresholds
    // 4 = genome / chr-wide thresholds
    int                nullsim_option      = 1;
    // int[] chrs = null;
    int                sporadic_cases      = 0;
    int                maxconserror        = 2;
    float             maxerrorrate        = 0.01f;
    float             mincorrectrate      = 0.99f;
    int                minkeep;
    boolean            errormodel          = false;
    boolean            verbose             = false;
    int                cutoff              = -1;
    boolean            haveAffectionStatus = false;
    boolean            weighted            = false;
    boolean            paired              = false;
    boolean            debug               = false;

    public Params(String[] args) {
        if (args.length < 4) {
            System.out.println("mimimum args: -p <ldfile> -d <pedfile>");
            System.out.println("-o <null sim. option");
        }
        List<String> largs = new ArrayList<String>();
        for (int i = 0; i < args.length; i++) {
            largs.add(args[i]);
            System.out.println(args[i]);
        }
        int iter = 0;
        boolean remove = false, remove1 = false;
	
        while (largs.size() > 0) {

            String arg = args[iter];
	    if (arg.equals("--help")) {
		usage();
		System.exit(0);
	    }
            else if (arg.equals("-a")) {
                haveAffectionStatus = Boolean.parseBoolean(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-w")) {
                weighted = Boolean.parseBoolean(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-r")) {
                paired = Boolean.parseBoolean(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-d")) {
                pedfile_name = args[iter + 1];
                remove = true;
            }
            else if (arg.equals("-e")) {
                maxerrorrate = Float.parseFloat(args[iter + 1]);
                mincorrectrate = 1 - maxerrorrate;
                errormodel = true;
                remove = true;
            }
            else if (arg.equals("-x")) {
                maxconserror = Integer.parseInt(args[iter + 1]);
                errormodel = true;
                remove = true;
            }
            else if (arg.equals("-k")) {
                minkeep = Integer.parseInt(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-v")) {
                verbose = true;
                remove1 = true;
            }
            else if (arg.equals("-p")) {
                parfile_name = args[iter + 1];
                remove = true;
            }
            else if (arg.equals("-s")) {
                sporadic_cases = Integer.parseInt(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-n")) {
                nsims = Integer.parseInt(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-g")) {
                nsig_sims = Integer.parseInt(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-t")) {
                remove = true;
                String at = args[iter + 1];
                if (at.equals("-1")) {
                    // Max run only
                    analysis_type = 1;
                }
                else if (at.equals("-2")) {
                    // Point by point analysis
                    analysis_type = 2;
                }
                else if (at.equals("-3")) {
                    // Point by point for specific markers and max run
                    analysis_type = 3;
                }
            }
            else if (arg.equals("-f")) {
                outfile = args[iter + 1];
                remove = true;
            }
            else if (arg.equals("-o")) {
                nullsim_option = Integer.parseInt(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("-c")) {
                cutoff = Integer.parseInt(args[iter + 1]);
                remove = true;
            }
            else if (arg.equals("--debug")) {
                debug = true;
                remove1 = true;
            }
            if (remove1) {
                largs.remove(args[iter]);
                remove1 = false;
            }
            if (remove) {
                largs.remove(args[iter]);
                largs.remove(args[iter + 1]);
                remove = false;
            }
            iter++;
        }
        if (errormodel) {
            if (sporadic_cases > 0) {
                System.err.println("Warning: Error model only implemented for case of no sporadics");
                // System.exit(1);
            }
        }
    }

    public String makeHeader(LinkageInterface linkageInterface) {
        StringBuffer header = new StringBuffer("Marker\t");

        for (int i = 0; i < get_sharingTypes(); i++) {
            if (i == 0) {
                header.append("\tN=" + linkageInterface.nProbands());
                if (isPvalued()) {
                    header.append("\t(Npval)");
                }
            }
            else {
                String nminusi = "N-" + i;
                header.append("\t" + nminusi);
                if (isPvalued()) {
                    header.append("\t(" + nminusi + "pval)");
                }
            }
        }

        if (isPaired()) {
            header.append("\tpSGS");
            if (isPvalued()) {
                header.append("\tppval");
            }
        }
        if (isWeighted()) {
            header.append("\twpSGS");
            if (isPvalued()) {
                header.append("\twpval");
            }
        }

        return header.toString() + "\n";
    }

    public boolean hasAffectionStatus() {
        return haveAffectionStatus;
    }

    public void setHaveAffectionStatus(boolean status) {
        haveAffectionStatus = status;
    }

    public int get_analysisType() {
        return analysis_type;
    }

    public int get_cutoff() {
        return cutoff;
    }

    public String get_inputfile(int type) {
        String input_type = pedfile_name;
        String suffix = ".ped";
        if (type == 1) {
            suffix = ".ld";
            input_type = parfile_name;
        }

        return input_type + suffix;
    }

    public LDModel get_ldmodel() {
        LDModel ldmodel = null;
        try {
	    BufferedReader br = new BufferedReader(new FileReader(ldfile_name));
	    InputFormatter inputFormatter = new InputFormatter(br);
            ldmodel = new LDModel(inputFormatter);
        }
        catch (Exception e) {
            System.err.println("IO troubles in making LD model.");
            e.printStackTrace();
        }
        return ldmodel;
    }

    public List<Integer> get_markerlst() {
        return analysis_markers;
    }

    public int get_nsig_sims() {
        return nsig_sims;
    }

    public int get_nsims() {
        return nsims;
    }

    public int get_nullsim_option() {
        return nullsim_option;
    }

    public String get_outfile() {
        return outfile;
    }

    public int get_sharingTypes() {
        return sporadic_cases;
    }

    public boolean getErrorModelStatus() {
        return errormodel;
    }

    public int getMaxConsecutiveErrors() {
        return maxconserror;
    }

    public float getMaxErrorRate() {
        return maxerrorrate;
    }

    public float getMinCorrectRate() {
        return mincorrectrate;
    }

    public int getMinKeep() {
        return minkeep;
    }

    public boolean getVerbose() {
        return verbose;
    }

    public boolean isWeighted() {
        return weighted;
    }

    public void setWeighted(boolean weighted) {
        this.weighted = weighted;
    }

    public boolean isPaired() {
        return paired;
    }

    public void setPaired(boolean paired) {
        this.paired = paired;
    }

    public boolean isPvalued() {
        return nsims != 0;
    }

    public boolean isDebug() {
        return debug;
    }

    public void usage() {
	System.out.println("  This program calculates, depending on which options are chosen");
	System.out.println("\ta) the runs of heterozygous sharing for a set of" );
	System.out.println("\tb) the pairwise sharing at each locus amongst designated individuals" );
	System.out.println("\tc) given pedigree infomation the weighted pairwise sharing at each locus" );
	System.out.println("  Sharers are specified by having a 1 in the proband field of the input");
	System.out.println("  LINKAGE pedigree file." );
	System.out.println("");
	System.out.println("  The output is a table with 1 line for each locus in the input LINKAGE");
	System.out.println("  parameter file. On each line appear:");

	System.out.println("  The name of the locus");
	System.out.println("  The largest number of individuals at that locus that can share one alleles. We denote this by S.");
	System.out.println("  A pair of columns for each sharing statistic (N, pval; N-1, pval;...");
	System.out.println("  A pair of columns for pairwise values: pSGS, pval");
	System.out.println("  A pair of columns for weighted pairwise values: wpSGS, pval");

	System.out.println("\n  Note that this format choice means that a run of length r will appear r");
	System.out.println("  times in the output: once for each locus in the run. This should be taken");
	System.out.println("  into account in any estimates of the run length distribution. ");
          
	// System.out.println("\nThe largest runs for which S_i = n, S_i >= n-1, etc seen across the");
	// System.out.println("whole data set is written to the standard output stream.");

	System.out.println("  Usage : java SGSPValue -p <ld> -d <ped> <options>");
	System.out.println("  where");
	System.out.println("  options consist of:");
	System.out.println("  -d\tName of the LINKAGE pedigree file. It is assumed that the ");
	System.out.println("\tfile has extension .ped which should not be included in this input name ");
	System.out.println("\t(i.e. Use 'input' for input.ped)");
	System.out.println("  -p\tName of the LINKAGE parameter + ld file.");
	System.out.println("\tIt is assumed that the file has extension .ld which should not be included in");
	System.out.println("\tthis input name (i.e. Use 'input' for input.ld)");
	System.out.println("  -o\tNullsim option");
	System.out.println("\t1 = nominal significance for observed run lengths");
	System.out.println("\t2 = nominal significance for observed and type 1 error rate for a null data set");
	System.out.println("\t3 = nominal significance for observed and calculation of chrom./genome wide significance thresholds");
	System.out.println("\t4 = chrom./genome wide significance thresholds.");
	System.out.println("  -s\tNumber of sporadic cases considered when calculating run lengths of N cases sharing");
	System.out.println("\t 0=none, 1=all \"probands\" (aka N); 2=N, N-1; 3=N, N-1, N-2; ... ");
          
	System.out.println("  -n\tNumber simulations for evaluating significance of run lengths.");
	System.out.println("  -g\tNumber of null simulations to use for determining the significance threshold.");
	System.out.println("\tusing a point by point run length analysis.");
	System.out.println("  -t\tAnalysis options. ");
	System.out.println("\t-1 = assess max run length");
	System.out.println("\t-2 = assess point by point [default]");
	System.out.println("\t-3 = assess both max run and point by point.");
	System.out.println("  -f\tOutput file name with .obs suffix appended ['SGSPValue' is the default].");
	System.out.println("\tWhen using nullsim option 2 it uses this name with '.obs' and '.null' concatenated.");

	System.out.println("  -a\tLINKAGE files have affection status data [default: false] ");
	System.out.println("  -r\tdo pairwise calculations [default: false] ");
	System.out.println("  -w\tdo weighted pairwise calculations [default: false] ");
	  
	/*
          System.out.println(" -c Cutoff value for empirical simulation. If
          all runs on a chromosome have > c nulls more extreme than the
          observed run lengths then drop this chromosome and stop simulations.
          System.out.println(" -e Maximum number of consecutive loci with
          errors.
          System.out.println(" -r Maximum error rate.

	*/

    }

}
