package edu.utah.camplab.sgs;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import edu.utah.camplab.jx.SegmentBlock;

public interface SGSRunInterface {
  //cli defaults
  public static final int SGS_MAX_FILE_WAIT = 5000; //millis
  public static final int ARGS_MINSPAN = 20;

  //socket communication strings
  public static final String SGS_SOCKET_CHECK  = "SGS_CHECK";
  public static final String SGS_SOCKET_FINISH = "SGS_FINISH";
  public static final String SGS_SOCKET_NOGO   = "SGS_FAIL";
  public static final String SGS_SOCKET_OK     = "SGS_OK";

  // Command line option names
  // -AbstractOption
  public static final String	SGS_OPT_TRIM	    = "probands";
  public static final String	SGS_OPT_MAXTIME	    = "maxtime";
  public static final String	SGS_OPT_DEBUG	    = "debug";
  public static final String    SGS_OPT_ENV         = "environment";
  // - AbstractRun
  public static final String	SGS_OPT_OPERATION   = "operation";
  public static final String	SGS_OPT_REPORTDIR   = "report";
  public static final String	SGS_OPT_SIMSCOUNT   = "simscount";
  public static final String	SGS_OPT_SKIPLENGTH  = "skipLength";
  public static final String	SGS_OPT_ACCUMULATOR = "accumulator";
  public static final String	SGS_OPT_DBNAME	    = "dbname";
  // - MA
  public static final String	SGS_OPT_MEIOSES	    = "meioses";
  public static final String	SGS_OPT_PROBANDSETS = "probandsets";
  public static final String	SGS_OPT_CUTOFFMULTI = "cutoffMulti";
  public static final String	SGS_OPT_MARKERSET   = "markerset";
  public static final String	SGS_OPT_PEDIGREE    = "pedigree";
  // - Chase
  public static final String	SGS_OPT_SEGMENT	    = "segment";
  public static final String	SGS_OPT_CONFIDENCE  = "confidence";
  public static final String	SGS_OPT_THRESHOLD   = "threshold";
  public static final String	SGS_OPT_SIMPVAL	    = "simpval";
  public static final String	SGS_OPT_SIMFILE	    = "simfile";
  
  // Report tags
  public static final String SGSARG_TAG  = "SGS_ARG::";
  public static final String SGSNOTE_TAG = "SGS_NOTE::";
    
  public void run() throws Exception;

  public void                 setSimulationCount(long n); 
  public long                 getSimulationCount(); 

  public void                 setMinimumSegmentSize(int minsize); 
  public int                  getMinimumSegmentSize();

  public void                 setJsonDir(File f) throws IOException;
  public File                 getJsonDir();

  public void                 setTag(UUID tag);
  public UUID                 getTag();

  public Map<String, Object>  getOptions();
  public void                 setOptions(Map<String,Object> opts);

  public void                 setThreshold(float t);
  public Float                getThreshold();
  public void                 setConfidence(int t);
  public int                  getConfidence();

  public void                 setProjectName(String projectName);
  public String               getProjectName();
  public void                 setDbName(String n);    // AKA investigator
  public String               getDbName();

  public String               getAccumulationHost() ;
  public void                 setAccumulationHost(String s);
  public int                  getAccumulationPort();
  public void                 setAccumulationPort(int p);
  public SegmentBlock         buildSegmentBlock();
    
  public void                 init(String[] args) throws Exception;
  public void                 formatHelp();
  public void                 printHelp() throws IOException;
    
}
