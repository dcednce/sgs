package edu.utah.camplab.sgs;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpsgcs.pedapps.GeneDropper;

public class SimulationExecutor extends RecursiveTask<ArrayList<DropEvaluation>> {

    private static final long serialVersionUID = -3985476282403344285L;

    final static Logger logger = LoggerFactory.getLogger(SimulationExecutor.class);

    private int simsToDo = 0;
    private int coreCount = 0;
    private int particianSize = 1;
    private int minimumSegment = 0;

    public SimulationExecutor(int nsims, int cores, int partSize, int minseg) {
        simsToDo = nsims;  // The only one which varies.
        coreCount = cores;
        particianSize = partSize;
        minimumSegment = minseg;
    }

    @Override
    public ArrayList<DropEvaluation> compute() {
        UUID tag = UUID.randomUUID();
        ArrayList<DropEvaluation> resultSet = new ArrayList<>();
        if ( simsToDo <= particianSize ) {
            LITuple availableLI = null;
            //TODO: set an upper bound
            while (availableLI == null) {
                availableLI = LITuple.getAvailableLI();
                if (availableLI == null) {
                    try {
                        logger.info("{}: Waiting for LI", tag);
                        Thread.sleep(60000); //Give it a minute or ten
                    }
                    catch (InterruptedException ie) {
                        ie.printStackTrace();
                        System.exit(2);
                    }
                }
            }
            long blockStart = System.currentTimeMillis();
            for (int currentSim = 0; currentSim < simsToDo; currentSim++) {
                GeneDropper dropper = availableLI.getDropper();
                dropper.geneDrop();
                DropEvaluation dv = new DropEvaluation(availableLI.getLinkageInterface(), minimumSegment);
                resultSet.add(dv);
            }
            LITuple.setAvailableLI(availableLI);
            logger.debug("{} Block done: did {} simulations in {} s", tag, simsToDo, (System.currentTimeMillis() - blockStart)/1000.0D);
            return resultSet;
        }
        else {
            int half = simsToDo/2;
            SimulationExecutor nextExec = new SimulationExecutor(half, coreCount, particianSize, minimumSegment);
            SimulationExecutor futureExec = new SimulationExecutor(simsToDo - half, coreCount, particianSize, minimumSegment);
            nextExec.fork();
            ArrayList<DropEvaluation> futureBlock = futureExec.compute();
            ArrayList<DropEvaluation> nextBlock = nextExec.join();
            resultSet.addAll(futureBlock);
            resultSet.addAll(nextBlock);
            return resultSet;
        }
    }

    public ArrayList<DropEvaluation> generateDropSet() {
        ForkJoinPool sgsPool = new ForkJoinPool(coreCount);
        logger.info("Simulator pool size: {}", sgsPool.getPoolSize());
        return ForkJoinPool.commonPool().invoke(new SimulationExecutor(simsToDo, coreCount, particianSize, minimumSegment));
    }
}
