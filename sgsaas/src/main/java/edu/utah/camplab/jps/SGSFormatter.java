package edu.utah.camplab.jps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

import jpsgcs.linkage.LinkageFormatter;
import org.jooq.DSLContext;

import edu.utah.camplab.tools.S3FileInputStream;

/*
 *  We need to do <emphasis>special</emphasis> things
 */
public class SGSFormatter extends LinkageFormatter {
  private DSLContext dbContext;
  private UUID contextId;

  public SGSFormatter(String url) throws IOException{
    super(S3FileInputStream.getS3Reader(url), url);
  }

  public SGSFormatter(String bucket, String object) throws IOException {
    super(S3FileInputStream.getS3Reader(bucket + "/" + object), object);
    //markersetId = UUID.fromString(object.substring(object.lastIndexOf('/')+1));
  }

  public SGSFormatter(InputStream istream, UUID m) throws IOException {
    super(new BufferedReader(new BufferedReader(new InputStreamReader(istream))), "webcall");
    contextId = m;
  }

  public SGSFormatter(BufferedReader reader, String title) throws IOException{
    super(reader, title);
    contextId = null;
  }

  public void setDbContext(DSLContext ctx) {
    dbContext = ctx;
  }

  public DSLContext getDbContext() {
    return dbContext;
  }

  public UUID getContextId() {
    return contextId;
  }

  public void setContextId(UUID dbid) {
    contextId = dbid;
  }
}
