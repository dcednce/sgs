package edu.utah.camplab.jps;

import edu.utah.camplab.db.generated.base.tables.Markerset;
import edu.utah.camplab.db.generated.base.tables.MarkersetMember;
import edu.utah.camplab.db.generated.base.tables.records.MarkerRecord;
import edu.utah.camplab.db.generated.base.tables.records.MarkersetMemberRecord;

import edu.utah.camplab.db.generated.base.tables.records.MarkersetRecord;
import jpsgcs.genio.ParameterData;
import jpsgcs.linkage.LinkageLocus;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.NumberedAlleleLocus;

import java.io.IOException;
import java.util.ArrayList;

import static edu.utah.camplab.db.generated.base.Tables.MARKERSET_MEMBER;
import static edu.utah.camplab.db.generated.base.tables.Marker.MARKER;

public class SGSParameterData extends LinkageParameterData implements ParameterData {
  public class MarkerTuple {
    public MarkerRecord mark;
    public MarkersetMemberRecord memb;
    public MarkerTuple(MarkerRecord k, MarkersetMemberRecord b) {
      mark = k;
      memb = b;
    }
  }
  private MarkersetRecord markerset;
  private ArrayList<MarkerTuple> markerTuples = new ArrayList<>();
  private boolean isSexLinked = false;
  double[] malethetas;

  public String locusName(int i) {
    return markerTuples.get(i).mark.getName();
  }	

  public int nLoci() {
    return markerTuples.size();
  }

  public int nAlleles(int i) {
    return markerTuples.get(i).mark.getAlleles().length;
  }

  public double[] alleleFreqs(int i) {
    return new double[] {0.5d, 0.5d};
  }

  public double getFemaleRecomFrac(int i, int j) {
    return getMaleRecomFrac(i,j);
  }
    
  public ArrayList<MarkerTuple> getMarkerTuples() {
    return markerTuples;
  }

  /**
     Returns the probability of a recombination between loci i and j in
     a single meiosis in males.
  */
  public double getMaleRecomFrac(int i, int j) {
    double collectedCM = 0.0d;
    for (int k = i; k < j; k++) {
      collectedCM += LinkageParameterData.thetaTocM(markerTuples.get(i).memb.getTheta());
    }
    return LinkageParameterData.cMToTheta(collectedCM);
  }

  /**
     Returns 0 or 1 if the regions is not or is, respectively, sex linked.
  */
  public boolean sexLinked() {
    return isSexLinked;
  }

  public void setIsSexLinked(int linked) {
    isSexLinked = linked != 0;
    if (isSexLinked && linked != 1) {
      throw new UnsupportedOperationException("Sex linked code must be 0 or 1");
    }
  }
  public SGSParameterData(Markerset loci) {
    ;
  }

  public MarkersetRecord getMarkerset() {
    return markerset;
  }

  public void setMarkerset(MarkersetRecord record) {
    markerset = record;
  }

  /**
   *
   * @param formatter - InputFormatter based S3 streamsetmar
   * @throws IOException
   * Does not make a legitimate LinkateParameterData.  Writes marker info to database.
   */
  public SGSParameterData(SGSFormatter formatter) throws IOException {
    boolean crashes = true;
    formatter.readLine();
    int nloci = formatter.readInt("ignore",0,false,crashes);
    int risklocus = formatter.readInt("ignore",0,false, crashes);
    setIsSexLinked(formatter.readInt("sex linkage code",0,false, crashes));
    int programcode = formatter.readInt("ignore",0,false, crashes);
    String line1comment = formatter.restOfLine();

    formatter.readLine();
    int mutlocus = formatter.readInt("ignore: mutation locus code",0,false, crashes);
    double mutmale = formatter.readDouble("ignore: male mutation rate",0,false, crashes);
    double mutfemale = formatter.readDouble("ignore: female mutation rate",0,false, crashes);
    int disequilibrium = formatter.readInt("ignore: disequilibrium code",0,false, crashes);
    String line2comment = formatter.restOfLine();

    formatter.readLine();
    int[] order = new int[nloci];
    boolean ok = true;
    for (int i=0; ok && i<order.length; i++) {
      if (formatter.newToken() && formatter.nextIsInt()) {
	order[i] = formatter.getInt();
      }
      else {
	ok = false;
      }
    }
    if (!ok) {
      formatter.warn("Can't read physical order of loci.\n\tAssumed to be in same order as in file.");
      for (int i=0; i<order.length; i++) {
	order[i] = i+1;
      }
    }
    String line3comment = formatter.restOfLine();
    ArrayList<MarkerRecord> markerList = new ArrayList<>();
    ArrayList<MarkersetMemberRecord> memberList = new ArrayList<>();
    loci = new LinkageLocus[nloci];
    for (int i = 0; i< nloci; i++) {
      formatter.readLine();
      int type = formatter.readInt("ignore: type", 3, false, crashes);
      int numAlleles = formatter.readInt("ignore: alleles", 2, false, crashes);
      String lineComment  = formatter.restOfLine();
      String[] lcParts = lineComment.split("\\|");
      MarkerRecord marker = formatter.getDbContext().newRecord(MARKER);
      marker.setName(lcParts[0].trim());
      marker.setBasepos38(Integer.parseInt(lcParts[1]));
      marker.setChrom(Integer.parseInt(lcParts[2]));
      //marker.setId(UUID.randomUUID());
      MarkersetMemberRecord member = formatter.getDbContext().newRecord(MARKERSET_MEMBER);
      //member.setMemberId(marker.getId());
      member.setOrdinal(i);
      //member.setTheta((malethetas[i]));
      MarkerTuple mt = new MarkerTuple(marker, member);
      markerTuples.add(mt);
      formatter.readLine();  // swallow the frequencies
      loci[i] = new NumberedAlleleLocus(2);
    }
    formatter.readLine();
    int sexdifference = formatter.readInt("ignore: sex recombination difference code",0,false, crashes);
    int interference = formatter.readInt("ignore: interference code",0,false, crashes);
    String line4comment = formatter.restOfLine();
		
    double mintheta = 1.0d/100000000;
    formatter.readLine();
    //malethetas = new double[nloci-1];
    ok = true;
    for (int j = 0; j<nloci; j++) {
      if (formatter.newToken() && formatter.nextIsDouble()) {
	double theta = formatter.getDouble();
	markerTuples.get(j).memb.setTheta(theta);
	if (theta > 0.5) {
	  ok = false;
	}
	if (markerTuples.get(j).memb.getTheta() < 0) {
	  formatter.crash("Negative recombination fraction specified "+malethetas[j]+".");
	}
	else if (markerTuples.get(j).memb.getTheta() == 0) {
	  formatter.warn("Zero recombination fraction specified "+malethetas[j]+"."+
		 "\n\tThis may be a problem for some programs, particularly the Markov chain Monte Carlo samplers (McSomething programs)."+
		 "\n\tSetting to "+mintheta);
          markerTuples.get(j).memb.setTheta(mintheta);
	}
      }
    }
    String line5comment = formatter.restOfLine();

    if (!ok) {
      formatter.warn("Converting inter locus distances from centi Morgans to recombination fractions using Kosambi mapping function");
      for (MarkerTuple mt : markerTuples) {// int j=0; j<malethetas.length; j++)
        MarkersetMemberRecord member = mt.memb;
        member.setTheta(cMToTheta(member.getTheta()));
      }
    }

    formatter.readLine();
    int variablelocus = formatter.readInt("ignore: variable locus code",0,false, crashes);
    double increment = formatter.readDouble("ignore: increment amount",0,false, crashes);
    double stoppingvalue = formatter.readDouble("ignore: stoppint value",0,false, crashes);
    String line6comment = formatter.restOfLine();
  }
}
