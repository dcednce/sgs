package edu.utah.camplab.server;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.utah.camplab.jx.AbstractPayload;

public class PayloadThread extends Thread implements Thread.UncaughtExceptionHandler {
    final static Logger logger = LoggerFactory.getLogger(PayloadThread.class);

    private ByteBuffer      rawBuffer;
    private AbstractPayload sgsPayload;
    private String          urlTemplate;
    private String          emergencyLanding = "";
    private ObjectMapper    jsonMapper = null;

    public PayloadThread(ByteBuffer load, String landingSite, String dbAddress) {
        rawBuffer        = load;
        emergencyLanding = landingSite == null ? "" : landingSite;
        urlTemplate      = dbAddress;
        jsonMapper       = new ObjectMapper();
        jsonMapper.setSerializationInclusion(Include.NON_NULL);
    }

    @Override
    public void run() {
        try {
            deserialize();
            persistPayload();
        }
        catch (Exception e ) {
            logger.error("Server side failure: {}", e.getMessage());
            if (sgsPayload == null) {
                logger.error ("No payload available!");
            }
            logger.error("EMERGENCY LANDING: writing run {} as json to {}", sgsPayload.getRunTag(), emergencyLanding);
            PayloadThread.writeAsString(sgsPayload, emergencyLanding);
        }
    }

    private void deserialize() throws IOException {
        logger.debug("Transforming payload of size {}", rawBuffer.position());
        sgsPayload = null;
        int index = 0;
        long ts = System.currentTimeMillis();
        try {
            sgsPayload = jsonMapper.readValue(rawBuffer.array(), AbstractPayload.class);
            String dbname = sgsPayload.getDbName();
            logger.debug("tranform took {}", System.currentTimeMillis() - ts);
            if (dbname != null) {
                sgsPayload.setDbUrl(String.format(urlTemplate, dbname));
            } else {
                PayloadThread.writeAsString(sgsPayload, emergencyLanding);
            }
        }

        catch (IOException ioe) {
            File landingAt = new File (emergencyLanding);
            if (! landingAt.exists()) {
                landingAt.mkdir();
            }
            File ofile = File.createTempFile("abjectFailure", ".txt", landingAt );
            logger.error("Writing to crashpad: {}", ofile.getCanonicalPath());
            String payloadText = new String(rawBuffer.array()); 
            FileWriter fow = new FileWriter(ofile);
            fow.write(payloadText);
            fow.flush();
            throw new IOException(ioe.getMessage());
        }
    }
    
    private void persistPayload() throws Exception {
        logger.info("Saving payload: {}", sgsPayload.getRunTag());
        //sgsPayload.write(); 
    }

    public static void writeAsString(AbstractPayload payload, String outdirName) {
        try {
            File ofile = new File(outdirName, payload.getRunTag().toString()+ ".json.gz");
            logger.error("Writing to file: {}", ofile.getCanonicalPath());
            FileOutputStream fos = new FileOutputStream(ofile);
            GZIPOutputStream zos = new GZIPOutputStream(fos);
            payload.asJson(zos);
        }
        catch (Exception e) {
            logger.error ("Cannot serialize payload {} to json", payload.getRunTag(), e);
            throw new RuntimeException("Hoping for text file");
        }
    }
        
    @Override
    public void uncaughtException(Thread accThread, Throwable t) {
        logger.error("Thread {} surprised by {}{}", accThread.getName(), t.getMessage(), t);
        throw new RuntimeException (t);
    }
}
