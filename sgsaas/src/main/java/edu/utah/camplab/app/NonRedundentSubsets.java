package edu.utah.camplab.app;

import com.jonoler.longpowerset.LongPowerSet;
import com.jonoler.longpowerset.LongSet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageId;

import edu.utah.camplab.sgs.AlleleSharing;
import edu.utah.camplab.sgs.SharingRun;
import edu.utah.camplab.tools.LinkageIdSet;

public class NonRedundentSubsets {
    File pedFile;
    File parFile;
    File reportFile;
    LinkageIdSet fullProbandSet = null;
    LinkageInterface linkageInterface = null;
    HashMap<SharingRun, List<LinkageIdSet>> fullStretchesMap = new HashMap<>();
    Writer writer;

    public NonRedundentSubsets(File ped, File par) {
        pedFile = ped;
        parFile = par;
    }

    public static void main(String[] args) {
        OptionParser op = new OptionParser();

        OptionSpec<File> pedfileSpec = op.accepts("ped").withRequiredArg().ofType(File.class).describedAs(".ped file name").required();
        OptionSpec<File> parfileSpec = op.accepts("par").withRequiredArg().ofType(File.class).describedAs(".par or .ld file name").required();
        OptionSpec<File> reportSpec = op.accepts("report").withRequiredArg().ofType(File.class).describedAs(".par or .ld file name").required();
        
        try {
            OptionSet opSet  = op.parse(args);
            NonRedundentSubsets nrs = new NonRedundentSubsets(pedfileSpec.value(opSet), parfileSpec.value(opSet));
            nrs.initLinkage();
            nrs.setReportFile(reportSpec.value(opSet));
            nrs.organize();
            nrs.report();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initLinkage() {
        try {
            linkageInterface = new LinkageInterface(new LinkageDataSet(parFile.getAbsolutePath(), pedFile.getAbsolutePath()));
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    public void organize() {
        List<LinkageId<?>> idlist = linkageInterface.getProbandIdSet();
        Set<LinkageId<?>> fullSet = listToSet(idlist);

        final int maxSetSize = idlist.size();
        LinkagePedigreeData lpd = linkageInterface.raw().getPedigreeData();
        fullProbandSet = new LinkageIdSet(idlist);
        collectStretches(idlist);
        
        LongSet<Set<LinkageId<?>>> powerSet = LongPowerSet.create(fullSet);
        for (Set<LinkageId<?>> pset : powerSet) {
            if (pset.size() < 2 || pset.size() == maxSetSize ) {
                continue;
            }
            idlist.clear();
            idlist.addAll(pset);
            lpd.resetProbands(idlist);
            collectStretches(idlist);
        }
    }

    public void report() {
        Writer writer = getWriter();
        try {
            writer.append("Chromosome: " + pullChromosome());
            writer.write("\nFull proband set: " + fullProbandSet.asCSV());
            ArrayList<SharingRun> tlist = new ArrayList<SharingRun>();
            tlist.addAll(fullStretchesMap.keySet());
            Collections.sort(tlist);
            for (SharingRun srun : tlist) {
                writer.append(String.format("\nRun start %d: End %d: length %d",
                                            srun.getFirstmarker(), srun.getLastmarker(), srun.runLength()));
                for (LinkageIdSet lis : fullStretchesMap.get(srun)) {
                    writer.append("\n\t" + lis.asCSV());
                }
            }
            writer.flush();
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File rf) {
        reportFile = rf;
    }

    private String pullChromosome() {
        return "6";
    }

    private Set<LinkageId<?>> listToSet(List<LinkageId<?>> list) {
        HashSet<LinkageId<?>> idset = new HashSet<>();
        idset.addAll(list);
        return idset;
    }

    private void collectStretches(List<LinkageId<?>> subset) {
        ArrayList<SharingRun> stretchList = new ArrayList<>();
        AlleleSharing.hetSharing(linkageInterface, stretchList);
        LinkageIdSet lids = new LinkageIdSet(subset);
        processStetchList(stretchList, lids);
    }

    // check all stretches defined by this id set; look for new
    // stretches or new identifiers or known stretch
    private void processStetchList(List<SharingRun> stretches, LinkageIdSet incoming) {
        for (SharingRun srun : stretches) {
            List<LinkageIdSet> knownSets = fullStretchesMap.get(srun);
            if (knownSets == null) {
                // stretch heretofore unseen
                knownSets = new ArrayList<LinkageIdSet>();
                knownSets.add(incoming);
                fullStretchesMap.put(srun, knownSets);
            }
            else {
                // maybe add this stretch
                addNovelSet(knownSets, incoming, srun);
            }
        }
    }

    private void addNovelSet(List<LinkageIdSet> knowns, LinkageIdSet wannabe, SharingRun seg) {
        List<LinkageIdSet> superceded = new ArrayList<>();
        for (LinkageIdSet known: knowns) {
            if (known.subsumes(wannabe)) {
                return;
            }
            if (wannabe.subsumes(known)) {
                superceded.add(known);
            }
        }
        knowns.removeAll(superceded);
        knowns.add(wannabe);
    }
    
    private Writer getWriter() {
        if (writer == null) {
            try {
                writer = new BufferedWriter(new FileWriter(getReportFile()));
            }
            catch (IOException ioe) {
                System.err.println("Couldn't establish report writer");
                ioe.printStackTrace();
            }
        }
        return writer;
    }

}
