package edu.utah.camplab.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jonoler.longpowerset.LongPowerSet;
import com.jonoler.longpowerset.LongSet;

import edu.utah.camplab.sgs.AbstractSGSRun;
import edu.utah.camplab.tools.LinkageIdSet;
import edu.utah.camplab.tools.MeiosesCounter;
import edu.utah.camplab.tools.PedTrimmer;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import jpsgcs.graph.DAG;
import jpsgcs.linkage.LinkageId;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkagePedigreeData;

public class CountMeioses {

  public static Logger logger = LoggerFactory.getLogger(CountMeioses.class);

  private MeiosesCounter meiosesCounter = null;
  private File           reportFile     = null;
  private File           pedfile        = null;
  private File           parfile        = null;
  private Writer         writer         = null;
    
  public CountMeioses(File fullPed) {
    pedfile = fullPed;
    meiosesCounter = new MeiosesCounter(pedfile, null);
  }
    

  public CountMeioses(LinkageInterface LI, File ped, File par) {
    pedfile = ped;
    meiosesCounter = new MeiosesCounter(LI);
  }
    
  public static void main(String[] args) {
    OptionParser op = new OptionParser();
    op.formatHelpWith(new BuiltinHelpFormatter(120,4));

    OptionSpec<File> pedfileSpec = op.accepts("ped").withRequiredArg().ofType(File.class).describedAs("ped file name").required();
    OptionSpec<File> reportSpec = op.accepts("report").withRequiredArg().ofType(File.class).describedAs("report file name");
    OptionSpec<File> trimSpec = op.accepts("trim").withRequiredArg().ofType(File.class).describedAs("proband trim list");
    OptionSpec<Void> subsetSpec = op.accepts("subsets", "count meioses per proband subset: there are 2**N subsets");
        
    ArrayList<String> helps = new ArrayList<String>();
    helps.add("help");
    helps.add("h");
    OptionSpec<Void> helpSpec = op.acceptsAll(helps);
    File pedFile = null;
    File repFile = null;
        
    OptionSet opSet = null;
    try {
      opSet = op.parse(args);
      if (opSet.has(helpSpec)) {
	op.printHelpOn(System.out);
	System.exit(2);
      }    
      pedFile = pedfileSpec.value(opSet);
    }
    catch (Exception e) {
      System.err.println("Caught in CountMeioses args()");
      e.printStackTrace();
      try {
	op.printHelpOn(System.out);
      }
      catch(Exception e2) {
	e2.printStackTrace();
      }
      System.exit(2);
    }

    //Run from here
    CountMeioses counter = new CountMeioses(pedFile);
        
    if (opSet.has(reportSpec)) {
      counter.setReportFile(reportSpec.value(opSet));
    }

    if (opSet.has(trimSpec)) {
      File trimfile = trimSpec.value(opSet);
      Set<String> idSet = PedTrimmer.getProbandIdsFromListFile(trimfile);
      LinkageIdSet probandSubset = new LinkageIdSet(idSet);
      counter.trim(probandSubset);
    }

    if (opSet.has(subsetSpec)) {
      counter.doEachSubset();
    }
    else {
      LinkageIdSet pids = counter.pullProbandIdSet();
      int justTheFacts = counter.getCount(0);
      //TODO? use writer
      System.out.println(String.format("Pedigree %s shows %d meioses for %s", pedFile.getName(), justTheFacts, pids.asCSV()));
    }
  }


  public void trim(LinkageIdSet trimset) {
    meiosesCounter.getPeddata().resetProbands(trimset.asList());
  }
    
  public int getCount(int split) {
    return meiosesCounter.getCount(split);
  }

  public LinkagePedigreeData[] getSplits() {
    return meiosesCounter.getPeddataSplits(); 
  }

  public LinkagePedigreeData getSplit(int ix) {
    return meiosesCounter.getSplit(ix);
  }

  public void tallyMeioses() {

    for (int k = 0; k < getSplits().length; k++) {
      LinkageIndividual[] people = getSplit(k).getIndividuals();
      DAG<LinkageIndividual, Object> peopledag = new DAG<LinkageIndividual, Object>(); // DAG with
      // everybody
      ArrayList<LinkageIndividual>probs = new ArrayList<>();

      for (int i = 0; i < people.length; i++) {
	peopledag.connect(meiosesCounter.getPeddata().pa(people[i]), people[i]);
	peopledag.connect(meiosesCounter.getPeddata().ma(people[i]), people[i]);
	if (people[i].proband == 1) {
	  probs.add(people[i]);
	}
      }
      System.out.println("\n\nPedigree " + getSplit(k).firstPedid().stringGetId() + "\tAll in\n-------------------------------------------------");
      Set<LinkageIndividual> ss = new LinkedHashSet<LinkageIndividual>();
      MeiosesCounter.CountHolder countHolder = meiosesCounter.getCount(k, ss);

      if (countHolder.countMRCA > 2) {
	System.out.println("\tmore than 2 MRCA");
      }
      else if (countHolder.unmarriedMRCA) {
	System.out.println("\t2 unmarried MRCA");
      }

      else {
	System.out.println("[]\tNumber of common ancestors=" + countHolder.ancestors + "\tNumber of meioses=" + countHolder.descendants);
      }
    }
  }

  public LinkageIdSet pullProbandIdSet() {
    return meiosesCounter.pullProbandIdSet();
  }
    
  public File getReportFile() {
    return reportFile;
  }

  public void setReportFile(File rf) {
    reportFile = rf;
  }

  //%s\t%s\t%s\t%s\t%s\t%s\t%s\n
  //, "meioses", "minKC", "maxKC", "male", "female", "unknown", "probands"
  public void doEachSubset() {
    try {
      if (getReportFile() == null) {
	setReportFile( File.createTempFile("count_meioses", ".txt"));
	System.err.println("No file name given so look here: " + getReportFile().getAbsolutePath());
      }
      reportHeader();
      for (LinkagePedigreeData lpd : meiosesCounter.getPeddataSplits()) {
	LinkageIdSet idlist = new LinkageIdSet(lpd);
	LinkageId<?> pedId = meiosesCounter.pullPedId(idlist.asList().get(0), lpd);
	logger.debug("subset reports for pedigree {}", pedId.toString());
	LongSet<Set<LinkageId<?>>> powerSet = LongPowerSet.create(idlist.asSet());
	getWriter().append(String.format("Pedigree: %s\ttotal probands: %s\n", pedId, idlist.size()));
	getWriter().append(String.format("\t%s\t%9s\t%9s\t%s\t%s\t%s\t%s\t%s\n",
					 "meioses", "minKC", "maxKC", "male", "female", "unknown", "size", "probands"));
	for (Set<LinkageId<?>> pset : powerSet) {
	  if (pset.size() > 1) {
	    ArrayList<LinkageId<?>> setList = new ArrayList<>();
	    setList.addAll(pset);
	    lpd.resetProbands(setList);
	    int meiosesCounted = meiosesCounter.getCount(0);
	    double[] minMaxKC = meiosesCounter.getKinshipCoeffRange(pset);
	    int[] genderCounts = meiosesCounter.getGenderCounts(pset, lpd);
	    reportSubset(pset, meiosesCounted, minMaxKC, genderCounts);
	  }
	}
      }
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }

  private void reportHeader() {
    try {
      getWriter().append("# Ped file: " + pedfile.getCanonicalPath() + "\n");
      getWriter().flush();
    }
    catch(IOException ioe) {
      ioe.printStackTrace();
    }
  }

  private void reportSubset(Set<LinkageId<?>> subset,
			    int meioses,
			    double[] kcMinMax,
			    int[] mfuCounts) {
    try {
      getWriter().append(String.format("\t%d\t%5.3e\t%5.3e\t%d\t%d\t%d\t%d",
				       meioses, kcMinMax[0], kcMinMax[1], mfuCounts[1], mfuCounts[2], mfuCounts[0], subset.size()));
      String comma = "\t";
      for (LinkageId<?> lid : subset) {
	getWriter().append(String.format("%s%s", comma, lid));
	comma = ",";
      }
      getWriter().append("\n").flush();
    }
    catch (IOException ioe) {
      System.err.println("Trouble writing report.");
      ioe.printStackTrace();
    }
  }

  private Writer getWriter() {
    if (writer == null) {
      try {
	writer = new BufferedWriter(new FileWriter(getReportFile()));
      }
      catch (IOException ioe) {
	System.err.println("Couldn't establish report writer");
	ioe.printStackTrace();
      }
    }
    return writer;
  }
}
