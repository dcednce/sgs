package joptsimple;

import java.util.List;
import joptsimple.OptionException;


public class SGSOptionException extends OptionException {
    private static final long serialVersionUID = 7685533485601802374L;

    public SGSOptionException(List<String> options) {
        super(options);
    }


    @Override
    Object[] messageArguments() {
        System.err.println("Who's calling, please?");
        return new Object[] {};
    }
}
