package edu.utah.camplab.app;

import java.util.*;
import java.io.*;

import com.jonoler.longpowerset.LongPowerSet;
import com.jonoler.longpowerset.LongSet;

public class ProbandSetMaker {
    private LongSet<Set<String>> powerSet = null;

    public ProbandSetMaker(File infile) throws Exception{
        try(BufferedReader reader = new BufferedReader(new FileReader(infile))) {
	    if (! infile.getName().endsWith(".ped")) {
		powerSet = LongPowerSet.create(pullProbandSet(reader));
	    }
	    else {
		powerSet = LongPowerSet.create(pullPedfileSet(reader));
	    }
	}
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public HashSet<String> pullProbandSet(BufferedReader reader) throws IOException {
        HashSet<String> idList = new HashSet<>();
        String line = reader.readLine();
        while (line != null) {
            String[] parts = line.split("\\s");
            //We take the last  or only value
            int px = parts.length  - 1;
            idList.add(parts[px]);
            line = reader.readLine();
        }
        return idList;
    }

    public HashSet<String> pullPedfileSet(BufferedReader reader) throws IOException{
        HashSet<String> idList = new HashSet<>();
        String redLine = reader.readLine();
        while (redLine != null) {
            String pedParts[] = redLine.split("\\s", 10);
            int probandStatus = Integer.parseInt(pedParts[8]);
            if ( probandStatus == 1 ) {
                idList.add(pedParts[1]);
            }
            else if ( probandStatus > 1 ) {
                throw new RuntimeException("Invalid proband status: " + probandStatus + ".  Looking at correct column?");
            }
            redLine = reader.readLine();
        }
        return idList;
    }

    public ProbandSetMaker(Set<String> idList) {
        powerSet = LongPowerSet.create(idList);
    }

    public static void main(String[] args) throws Exception{
        File inFile = new File(args[0]);
        File outFile = new File(args[1]);
        
        ProbandSetMaker psm = new ProbandSetMaker(inFile); 
        psm.writeSets(outFile);
    }

    public void writeSets(Writer writer) {
        try {
            for (Set<String> l : powerSet) {
                //TODO: make this an option
                if (l.size() < 2) {
                    continue;
                }
                TreeSet<String> tset = new TreeSet<>();
                tset.addAll(l);
                writer.write(asCSV(tset) + "\n");
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public void writeSets(File outFile) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outFile))){
            writeSets(writer);
        }
        catch(IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public LongSet<Set<String>> getIdSets() {
        return powerSet;
    }
    
    private String asCSV(Set<String> sset) {
        String comma = "";
        StringBuffer sbuf = new StringBuffer();
        for (String s : sset) {
            sbuf.append(comma + s);
            comma = ",";
        }
        return sbuf.toString();
    }
}
