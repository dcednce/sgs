package edu.utah.camplab.app;

import java.io.File;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.FileVisitor;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import edu.utah.camplab.sgs.AbstractSGSRun;
import edu.utah.camplab.tools.PvalFileVisitor;
import edu.utah.camplab.tools.SegmentSearchCriterion;

public class PvalVariantLookup {
    List<SegmentSearchCriterion> criteria = new ArrayList<>();
    File variantListing = null;
    Path startingDirectory = null;
    File reportFile = null;
    
    public static void main(String[] args) {
        PvalVariantLookup pvl = new PvalVariantLookup();
        pvl.init(args);
        pvl.run();
    }

    public OptionSet argParse(String[] args) {
        OptionParser op = new OptionParser();
        OptionSpec<File> varfileSpec =
            op.accepts("variants").withRequiredArg().ofType(File.class).describedAs("variant listing file name").required();
        OptionSpec<File> directorySpec =
            op.accepts("directory").withRequiredArg().ofType(File.class).describedAs("directory in which to start looking").required();
        OptionSpec<File> reportSpec =
            op.accepts("report").withRequiredArg().ofType(File.class).describedAs("file for report").required();
        List<String> helps = new ArrayList<String>();
        helps.add("help");
        helps.add("h");
        OptionSpec<Void> helpSpec = op.acceptsAll(helps);

        OptionSet opSet = null;
        try {
            opSet = op.parse(args);
            if (opSet.has(helpSpec)) {
                op.printHelpOn(System.out);
                System.exit(2);
            }
            startingDirectory = directorySpec.value(opSet).toPath();
            variantListing = varfileSpec.value(opSet);
            reportFile = reportSpec.value(opSet);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                op.printHelpOn(System.out);
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            System.exit(3);
        }
        return opSet;
    }
    
    public void init(String[] args) {
        try {
            argParse(args);
            buildSetLocationList();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(reportFile));
            FileVisitor<Path> visitor =  new PvalFileVisitor(criteria);
            Files.walkFileTree(startingDirectory, visitor);
            report(writer);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void report(BufferedWriter writer) throws IOException {
        preamble(writer);
        for (SegmentSearchCriterion ssc : criteria) {
            ssc.write(writer);
            if (ssc.bestSegment != null) {
                ssc.bestSegment.write(writer);
            }
            else {
                writer.append("\tNo enclosing segment found, (likely in region of zeros)\n");
            }
            writer.flush();
        }
    }

    private void preamble(BufferedWriter w) throws IOException {
    	for (String jar : AbstractSGSRun.addManifest()) {
    		w.write(jar);	
    	}
        w.write(String.format("# Base directory: %s\n", startingDirectory.toString()));
        w.flush();
    }

    private void buildSetLocationList() throws IOException{
        try(BufferedReader reader = new BufferedReader(new FileReader(variantListing))) {
                String redLine = reader.readLine();
                while (redLine != null) {
                    if (! redLine.startsWith("#")) {
                        String[] grossParts = redLine.split("\\|");
                        int chrom = Integer.valueOf(grossParts[0]);
                        long basepos = Long.valueOf(grossParts[1]);
                        List<String> mustHave = Arrays.asList(grossParts[2].split(","));
                        List<String> cantHave = Arrays.asList(grossParts[3].split(","));
                        SegmentSearchCriterion ssc = new SegmentSearchCriterion(chrom, basepos, mustHave, cantHave);
                        criteria.add(ssc);
                    }
                    redLine = reader.readLine();
                }
            }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
